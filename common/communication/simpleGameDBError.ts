export class SimpleGameDBError extends Error {

    constructor(m: string) {
        super(m);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, SimpleGameDBError.prototype);
    }
}
