
export enum ImageProcessingStatus {
    S_OK,
    E_INVALID_DIFFERENCES_COUNT,
    E_INVALID_IMAGE_SIZE,
    E_INVALID_IMAGE_TYPE,
    E_NOT_FOUND,
}
export enum SceneProcessingStatus{
    S_OK,
    E_NOT_DIFFERENCE
}
export enum UserLoginStatus {
    S_OK,
    E_USER_ALREADY_CONNECTED,
    E_INVALID_USER_NAME,
    E_ERROR,
    NOT_LOGGED_IN,
    NAME
}

export enum SceneGenerationStatus {
    S_OK,
    E_INVALID_OBJECT_COUNT,
}
