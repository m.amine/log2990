export class FreeGameDBError extends Error {

    constructor(m: string) {
        super(m);

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, FreeGameDBError.prototype);
    }
}
