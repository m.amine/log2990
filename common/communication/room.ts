export class Room {
    public users: string[];
    public id: string;
    public name: string;

    public constructor(name: string, idRoom: string) {
        this.users = new Array<string>();
        this.name = name;
        this.id = idRoom;
    }

    public addUser(user: string): void {
        this.users.push(user);
    }

    public getFirstUser(): string {
        return this.users[0];
    }

    public getSecondUser(): string {
        return this.users[this.users.length - 1];
    }

    public count(): number {
        return this.users.length;
    }
}
