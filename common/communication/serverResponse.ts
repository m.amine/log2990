
export type ServerResponse<Status, Response> = {
    status: Status,
    response: Response | null,
};