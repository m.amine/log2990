import { IPoint3D } from "./iPoint3D";

export interface IBound {
    dimension: IPoint3D;
    position: IPoint3D;
}