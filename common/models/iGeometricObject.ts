import { IPoint3D } from "./iPoint3D";

export enum GeometricObjectType {
    BOX,
    SPHERE,
    CYLINDER,
    CONE,
    PYRAMID,
    PLANE,
}

export interface IGeometricObject {
    id: string;
    hexColor: number;
    position: IPoint3D;
    rotation: IPoint3D;
    castShadow: boolean;
    type: GeometricObjectType;
    w: number;
    h: number;
    d: number;
    r: number;
}