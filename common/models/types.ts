import { Pixel } from "./pixel";
import { Point } from "./point";

export type Pair<T> = {
    first: T,
    second: T,
};

export type PixelMap = {position: Point, value: Pixel}[];