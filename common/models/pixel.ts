export class Pixel {
    public r: number;
    public g: number;
    public b: number;

    public constructor (r: number, g: number, b: number) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public equals(pix: Pixel): boolean {
        return (this.r === pix.r && this.g === pix.g && this.b === pix.b);
    }
}
