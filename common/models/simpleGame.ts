import { Record } from "./record";

export interface SimpleGame {
    gameId: String;
    name: String;
    originalImage: String;
    modifiedImage: String;
    differencesImage: String;
    soloTime: Record[];
    vsTime: Record[];
    action: String;
}
