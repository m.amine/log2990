import { Point } from "./point";
import { IGeometricObject } from "./iGeometricObject";
import { IBound } from "./iBound";

export enum SceneType {
    CLASSIC,
    THEMATIC,
}

export enum SceneModification {
    ADD,
    REMOVE,
    CHANGE,
}

export enum ObjectColor {
    RED = 0xFF0000,
    GREEN = 0x00FF00,
    BLUE = 0x0000FF,
    YELLOW = 0xFFFF00,
    CYAN = 0x00FFFF,
    WHITE = 0xFFFFFF,
}

export interface IScene {
    backgroundColor: number;
    decoration: IGeometricObject[];
    objects: IGeometricObject[];
    type: SceneType;
    boundingBoxDimension: IBound;
}

export interface convertedPoint {
    convertedPoint: Point,
    scene: number
}

export enum SceneNames{
    original,
    modified
}