export enum KeyBoardInputs {
    FORWARD = "w",
    LEFT = "a",
    RIGHT = "d",
    BACKWARDS = "s",
    CHEAT = "t"
}

export enum MouseInputs {
    LEFTCLICK,
    SCROLLWHEELCLICK,
    RIGHTCLICK
}

export class Vector3D {
    public x: number;
    public y: number;
    public z: number;

    public constructor(x: number, y: number, z: number) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public normalize(): void {
        const norm = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
        this.x /= norm;
        this.y /= norm;
        this.z /= norm;
    }

    public cross(vector: Vector3D): Vector3D {
        const x: number = this.y * vector.z - this.z * vector.y;
        const y: number = this.z * vector.x - this.x * vector.z;
        const z: number = this.x * vector.y - this.y * vector.x;

        return new Vector3D(x, y, z);
    }
}

export class Point2D {
    public x: number;
    public y: number;

    public constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }
}