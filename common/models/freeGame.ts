import { Record } from "./record";
import { IScene } from "./iScene";

export interface FreeGame {
    gameId: String;
    name: String;
    thumbnail: String;
    originalScene: IScene;
    modifiedScene: IScene;
    differencesId: string[];
    soloTime: Record[];
    vsTime: Record[];
    action: String;
}