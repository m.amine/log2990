export interface gameController {
    simpleViewSoloGame: boolean;
    simpleViewMultiGame: boolean;
    freeViewSoloGame: boolean;
    freeViewMultiGame: boolean;
    waitingView: boolean;
    cancelSimpleViewMultiGame: boolean;
    cancelFreeViewMultiGame: boolean;
  }