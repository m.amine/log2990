export class Chronometer {

    private _minutes: number = 0;
    private _secondes: number = 0;
    private _totalSecondes: number = 0;
    private _timer : number = 0;

    constructor(minutes:number, seconds:number) {
        this._totalSecondes = 0;
        if ( (seconds < 0) || (minutes< 0) ) {
            minutes= 0 ;
            seconds = 0 ;
          }
        
    }
    public  get minutes(): number { return this._minutes; }
    public get secondes(): number { return this._secondes; }
    public get totalSecondes(): number { return this._totalSecondes;}


    public start(): void  {
        this._timer = window.setInterval(() => {
        this._minutes = Math.floor(++this._totalSecondes / 60);
        this._secondes = this._totalSecondes - this._minutes * 60;
      }, 1000);

    }

    public stop():void  {
      clearInterval(this._timer);
    }

    public reset(): void {
      this._totalSecondes = this._minutes = this._secondes = 0;
    }

    public display(): string{
    return ((this.minutes < 10 ? "0"+this.minutes: this.minutes)+
        ":"+ (this.secondes < 10 ? "0"+this.secondes: this.secondes));
   
    }
  }