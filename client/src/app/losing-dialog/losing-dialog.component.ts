import { Component, EventEmitter, Output} from "@angular/core";

@Component({
  selector: "app-losing-dialog",
  templateUrl: "./losing-dialog.component.html",
  styleUrls: ["./losing-dialog.component.css"],
})
export class LosingDialogComponent {
  @Output() public finish: EventEmitter<void>;
  @Output() public restart: EventEmitter<void>;
  public constructor() {
    this.finish = new EventEmitter<void>();
    this.restart = new EventEmitter<void>();
  }
  public restartGame(): void {
    this.restart.emit();
  }

  public leave(): void {
    this.finish.emit();
  }

}
