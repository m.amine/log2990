import { Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
  selector: "app-waiting-view",
  templateUrl: "./waiting-view.component.html",
  styleUrls: ["./waiting-view.component.css"],
})
export class WaitingViewComponent /* implements OnInit*/ {
  @Output() public cancelSimpleViewMultiGame: EventEmitter<void>;
  @Output() public cancelFreeViewMultiGame: EventEmitter<void>;
  @Input() public freeViewMultiGame: boolean;
  @Input() public simpleViewMultiGame: boolean;

  public constructor() {
    this.cancelSimpleViewMultiGame = new EventEmitter<void>();
    this.cancelFreeViewMultiGame = new EventEmitter<void>();
   }

  public restartGame(): void {
     if (this.simpleViewMultiGame) {
      this.cancelSimpleViewMultiGame.emit();
     }
     if (this.freeViewMultiGame) {
      this.cancelFreeViewMultiGame.emit();

    }
  }
}
