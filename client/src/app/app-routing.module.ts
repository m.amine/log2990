import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FreeGameViewMultiPlayerComponent } from "./free-game-view-multi-player/free-game-view-multi-player.component";
import { GamesCardListComponent } from "./games-card-list/games-card-list.component";
import { SimpleGameViewMultiPlayerComponent } from "./simple-game-view-multi-player/simple-game-view-multi-player.component";
import { WaitingViewComponent } from "./waiting-view/waiting-view.component";

export const routes: Routes = [
  {path: "gameCard", component: GamesCardListComponent },
  { path: "simpleGameMultiple", component: SimpleGameViewMultiPlayerComponent},
  {path: "freeGameMultiple", component: FreeGameViewMultiPlayerComponent},
  {path: "waitingView", component: WaitingViewComponent},
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})

export class AppRoutingModule { }
