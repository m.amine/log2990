import { Component, EventEmitter, Output} from "@angular/core";

@Component({
  selector: "app-winning-dialog",
  templateUrl: "./winning-dialog.component.html",
  styleUrls: ["./winning-dialog.component.css"],
})
export class WinningDialogComponent {
  @Output() public finish: EventEmitter<void>;

  public constructor() {
    this.finish = new EventEmitter<void>();
  }

  public leave(): void {
    this.finish.emit();
  }

}
