import { Component } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { FreeViewGameFormComponent} from "./free-view-game-form/free-view-game-form.component";
import { SimpleViewGameFormComponent } from "./simple-view-game-form/simple-view-game-form.component";
@Component({
  selector: "app-admin-view",
  templateUrl: "./admin-view.component.html",
  styleUrls: ["./admin-view.component.css"],
})
export class AdminViewComponent {

  public constructor(public dialog: MatDialog) { }

  public openSimpleViewDialog(): void {
    this.dialog.open(SimpleViewGameFormComponent);
  }
  public openFreeViewDialog(): void {
    this.dialog.open(FreeViewGameFormComponent);
  }

}
