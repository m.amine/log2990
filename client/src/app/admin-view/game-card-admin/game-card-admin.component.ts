import { Component, OnInit } from "@angular/core";
import { FreeGameService } from "src/app/services/freeGame.service";
import { FreeGame } from "../../../../../common/models/freeGame";
import { SimpleGame } from "../../../../../common/models/simpleGame";
import { SimpleGameService } from "../../services/simpleGame.service";
import { SocketService } from "../../services/socket.service";

@Component({
  selector: "app-game-card-admin",
  templateUrl: "./game-card-admin.component.html",
  styleUrls: ["./game-card-admin.component.css"],
})

export class GameCardAdminComponent implements OnInit {
  public constructor(
    private simpleGameService: SimpleGameService,
    private freeGameService: FreeGameService,
    private socketService: SocketService) { }

  public readonly title: String = "Liste des jeux";

  public simpleGames: SimpleGame[] = [];
  public freeGames: FreeGame[] = [];

  public deleteSimpleGame(id: string): void {
    if (confirm("This Game is about to be deleted.")) {
      this.socketService.emit<string>("deleteGame", id);
      this.simpleGameService.deleteGame(id).then(() => window.location.reload());
    }
  }

  public resetSimpleGame(id: string): void {
    if (confirm("This Game is about to be reset.")) {
      this.simpleGameService.resetGame(id).then(() => window.location.reload());
    }
  }

  public deleteFreeGame(id: string): void {
    if (confirm("This Game is about to be deleted.")) {
      this.socketService.emit<string>("deleteGame", id);
      this.freeGameService.deleteGame(id).then(() => window.location.reload());
    }
  }

  public resetFreeGame(id: string): void {
    if (confirm("This Game is about to be reset.")) {
      this.freeGameService.resetGame(id).then(() => window.location.reload());
    }
  }

  public ngOnInit(): void {
    this.simpleGameService.getAll().subscribe((newSimpleViewGames: SimpleGame[]) => this.simpleGames = newSimpleViewGames);
    this.freeGameService.getAll().subscribe((newFreeViewgames: FreeGame[]) => this.freeGames = newFreeViewgames);
  }

}
