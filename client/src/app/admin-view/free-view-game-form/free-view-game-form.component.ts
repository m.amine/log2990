import { Component } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators, ValidatorFn } from "@angular/forms";
import {MatDialogRef} from "@angular/material";
import { FreeViewFormService} from "../../services/free-view-form.service";
export interface ObjectType {
  value: string;
  view: string;
}
export interface Checkbox {
  id: number;
  name: string;
}
const MAX_LENGTH: number = 12;
const MIN_LENGTH: number = 3;
const MIN_OBJECT_QUANTITY: number = 10;
const MAX_OBJECT_QUANTITY: number = 200;

@Component({
  selector: "app-free-view-game-form",
  templateUrl: "./free-view-game-form.component.html",
  styleUrls: ["./free-view-game-form.component.css"],
})

export class FreeViewGameFormComponent {
  public freeViewGameForm: FormGroup;
  public readonly types: ObjectType [];
  public readonly checkBoxes: Checkbox[];
  public isSendingForm: boolean;
  public constructor( public fb: FormBuilder, private freeViewService: FreeViewFormService,
                      public dialogRef: MatDialogRef<FreeViewGameFormComponent>) {
    this.isSendingForm = false;
    this.checkBoxes = [
      {id: 0, name: "Ajout" },
      {id: 1, name: "Suppression" },
      {id: 2, name: "Changement de couleur" },
    ];
    this.types = [
      {value: "0", view: "Formes Geometriques"},
      {value: "1", view: "Residence"},
    ];
    const controls: FormControl[] = this.checkBoxes.map(() => new FormControl(false));
    this.freeViewGameForm = this.fb.group(
      {
        gameName: new FormControl
        ("", Validators.compose([Validators.maxLength(MAX_LENGTH), Validators.minLength(MIN_LENGTH), Validators.required])),
        objectQuantity: new FormControl ("", [Validators.max(MAX_OBJECT_QUANTITY),
                                              Validators.min(MIN_OBJECT_QUANTITY), Validators.required]),
        objectType: new FormControl("", Validators.required),
        checkBoxes: new FormArray(controls, this.atLeastOneSelected()),
    });

  }
  public submitFreeViewGameForm(): void {
      this.isSendingForm = true;
      this.freeViewService.submitFreeViewForm(this.freeViewGameForm.value)
      .then(() => {
        this.dialogRef.close();
        window.location.reload();
      });
  }
  public atLeastOneSelected(): ValidatorFn {
    return (formArray: FormArray) => {
    const totalSelected: number = formArray.controls.
    map((control) => control.value).reduce((prev, next) => next ? prev + next : prev, 0);

    return (totalSelected >= 1 ? null : { required: true });
    };
  }
}
