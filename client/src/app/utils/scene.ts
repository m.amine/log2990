import { IBound } from "../../../../common/models/iBound";
import { IGeometricObject } from "../../../../common/models/iGeometricObject";
import { IScene, SceneType } from "../../../../common/models/iScene";

export class Scene implements IScene {
    public backgroundColor: number;
    public decoration: IGeometricObject[];
    public objects: IGeometricObject[];
    public type: SceneType;
    public boundingBoxDimension: IBound;

    public constructor() {
        this.objects = [];
        this.decoration = [];
    }
}
