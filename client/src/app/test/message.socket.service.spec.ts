import { TestHelper } from "../../test.helper";
import { MessageSocketService } from "../services/message.socket.service";

// tslint:disable-next-line:no-any
let socketSpy: any;
let service: MessageSocketService;

describe("MessageSocketService", () => {

    beforeEach(() => {
        socketSpy = jasmine.createSpyObj("SocketIOClient.Socket", ["on", "emit"]);
        service = new MessageSocketService();
        service.initSocket(socketSpy);
    });

    it("Should listen on the Login User", () => {

        const nameOfPlayer: string = "mimi";
        socketSpy.on.and.returnValue(TestHelper.asyncData(nameOfPlayer));
        // check the content of the mocked call
        service.onNewUserConnected();
        // check if only one call was made
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should listen on the Logout User", () => {
        const nameOfPlayer: string = "mimi";
        socketSpy.on.and.returnValue(TestHelper.asyncData(nameOfPlayer));
        service.onUserDisconnect();

        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should emit for find difference in solo game", () => {
        const nameOfPlayer: string = "mimi";
        service.emitErrorIdentificationSoloGame(nameOfPlayer);
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });

    it("Should listen on find difference in solo game", () => {
        service.onFindDifferenceSoloGame();
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should emit for find error identification in solo game", () => {
        const nameOfPlayer: string = "mimi";
        service.emitErrorIdentificationSoloGame(nameOfPlayer);
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });

    it("Should listen on find error identification in solo game", () => {
        service.onErrorIdentificationSoloGame();
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should emit for find difference in multiPlayer game", () => {
        const nameOfPlayer: string = "mimi";
        const gameId: string = "111";

        socketSpy.emit.and.returnValue(TestHelper.asyncData(nameOfPlayer));
        service.emitFindDifferenceMultiPlayerGame(nameOfPlayer, gameId);
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });
    it("Should listen on find difference in multiPlayer game", () => {
        const nameOfPlayer: string = "mimi";
        socketSpy.on.and.returnValue(TestHelper.asyncData(nameOfPlayer));
        service.onFindDifferenceMultiPlayerGame();
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });
    it("Should emit for find error identification  in multiPlayer game", () => {
        const nameOfPlayer: string = "mimi";
        const gameId: string = "121";

        socketSpy.emit.and.returnValue(TestHelper.asyncData(nameOfPlayer));
        service.emitErrorIdentificationMultiPlayerGame(nameOfPlayer, gameId);
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });
    it("Should listen on find error identification  in multiPlayer game ", () => {

        const nameOfPlayer: string = "mimi";
        socketSpy.on.and.returnValue(TestHelper.asyncData(nameOfPlayer));
        service.onErrorIdentificationMultiPlayerGame();
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should emit the Best Scores ", () => {

        const nameOfPlayer: string = "mimi";
        const position: number = 3;
        const nameOfGame: string = "game";
        const playerPosition: string = "position";

        service.EmitBestScoreFind(nameOfPlayer, position, nameOfGame, playerPosition);
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });
    it("Should listen on Best Scores", () => {
        service.onBestScore();
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });
});
