/* tslint:disable */
import { Euler } from "three/three-core";
import { KeyBoardInputs, Point2D } from "../../../../common/models/camera";
import { CameraMovementService } from "../services/cameraMovement.service";
import * as THREE from "three";

let cameraMovement: CameraMovementService;

describe("CameraMouvementService", () => {
    beforeEach(() => {
       cameraMovement = new CameraMovementService();
    });

    it(" should decrement of cameraMovement.step in Z axis", () => {
        const initialZPosition: number = cameraMovement.camera.position.z;
        cameraMovement.moveCamera(KeyBoardInputs.FORWARD);
        const finalZPosition: number = cameraMovement.camera.position.z;
        expect(finalZPosition - initialZPosition).toEqual(- cameraMovement.step);
    });

    it(" should increment of cameraMovement.step in Z axis ", () => {
        const initialZPosition: number = cameraMovement.camera.position.z;
        cameraMovement.moveCamera(KeyBoardInputs.BACKWARDS);
        const finalZPosition: number = cameraMovement.camera.position.z;
        expect(finalZPosition - initialZPosition).toEqual(cameraMovement.step);
    });

    it(" should decrement of cameraMovement.step in X axis", () => {
        const initialXPosition: number = cameraMovement.camera.position.x;
        cameraMovement.moveCamera(KeyBoardInputs.LEFT);
        const finalXPosition: number = cameraMovement.camera.position.x;
        expect(finalXPosition - initialXPosition).toEqual(- cameraMovement.step);
    });

    it(" should increment of cameraMovement.step in X axis", () => {
        const initialXPosition: number = cameraMovement.camera.position.x;
        cameraMovement.moveCamera(KeyBoardInputs.RIGHT);
        const finalXPosition: number = cameraMovement.camera.position.x;
        expect(finalXPosition - initialXPosition).toEqual(cameraMovement.step);
    });

    it("should not change camera rotation ", () => {
        const initialRotation: Euler = cameraMovement.camera.rotation;
        cameraMovement.rotateCamera(new Point2D(0, 0));
        const finalRotation: Euler = cameraMovement.camera.rotation;
        expect(initialRotation).toEqual(finalRotation);
    });

    it("should rotate in X axis only", () => {
        const initialRotation: Euler = cameraMovement.camera.rotation;
        const iniy: number = initialRotation.y;
        const inix: number = initialRotation.x;
        cameraMovement.rotateCamera(new Point2D(-1, 0));
        const finalRotation: Euler = cameraMovement.camera.rotation;
        expect(finalRotation.y - iniy).toBeCloseTo(cameraMovement.angle);
        expect(finalRotation.x - inix).toBeCloseTo(0);
    });

    it("should rotate in Y axis only", () => {
        const initialRotation: Euler = cameraMovement.camera.rotation;
        const inix: number = initialRotation.x;
        const iniy: number = initialRotation.y;
        cameraMovement.rotateCamera(new Point2D(0, -1));
        const finalRotation: Euler = cameraMovement.camera.rotation;
        expect(finalRotation.x - inix).toBeCloseTo(cameraMovement.angle); // infinie decimal
        expect(finalRotation.y - iniy).toBeCloseTo(0);
    });
    it("should set camera by puting cameraSetted at true", () => {
        let camera1: THREE.PerspectiveCamera = new THREE.PerspectiveCamera;
        let camera2: THREE.PerspectiveCamera = new THREE.PerspectiveCamera;
        cameraMovement.setCamera(camera1); 
        cameraMovement.setCamera(camera2); 
        expect(camera1).toEqual(cameraMovement.camera); 
        expect(camera2).not.toEqual(cameraMovement.camera); 
    });

});
