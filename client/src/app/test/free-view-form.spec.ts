/* tslint:disable */
import { of } from "rxjs/internal/observable/of";
import { SceneGenerationStatus as SGStatus } from "../../../../common/communication/errors";
import { FreeViewFormService, SGResponse } from "../services/free-view-form.service";
import { IScene } from "../../../../common/models/iScene";

interface FreeViewForm {
    objectQuantity: number;
    checkBoxes: boolean[];
}

describe("FreeViewFormService", () => {
    // tslint:disable-next-line:no-any
    let httpClientSpy: any;
    let service: FreeViewFormService;
    const mockForm: FreeViewForm = {objectQuantity: 10, checkBoxes: [true, false, false]};
    const SCENE1: IScene = {} as IScene;
    const SCENE2: IScene = {} as IScene;
    beforeEach(() => {
      httpClientSpy = jasmine.createSpyObj("HttpClient", ["post"]);
      service = new FreeViewFormService(httpClientSpy);
    });
    it("should  call the requestFreeGameCreation and createGameData if the response status is OK",  async() => {
        // tslint:disable-next-line:max-line-length
        const res: SGResponse = {status: SGStatus.S_OK , response: { scenes: {first: SCENE1, second: SCENE2}, differencesIds: ["id1", "id2" , "id2", "id2", "id2", "id2", "id2","id2"]}};
        const spyCreateGameData: any = spyOn<any>(service, "createGameData");
        const spyRequestFreeGameCreation: any = spyOn<any>(service, "requestFreeGameCreation");
        httpClientSpy.post.and.returnValue(of([res]));
        await service.submitFreeViewForm(mockForm).then(() => {
         expect(spyCreateGameData).toHaveBeenCalled();
         expect(spyRequestFreeGameCreation).toHaveBeenCalled();
        }).catch((answer) => {
            console.log(answer);
        });
     });
    it("should not call the requestFreeGameCreation and createGameData if the response status is an error", async () => {
        // tslint:disable-next-line:max-line-length
        const res: SGResponse = {status: SGStatus.E_INVALID_OBJECT_COUNT, response: { scenes: {first: SCENE1, second: SCENE2}, differencesIds: []}};
        const spyCreateGameData: any = spyOn<any>(service, "createGameData");
        const spyRequestFreeGameCreation: any = spyOn<any>(service, "requestFreeGameCreation");
        httpClientSpy.post.and.returnValue(of([res]));
        await service.submitFreeViewForm(mockForm).then(() => {
         expect(spyCreateGameData).not.toHaveBeenCalled();
         expect(spyRequestFreeGameCreation).not.toHaveBeenCalled();
        }).catch((answer) => {
            console.log(answer);
        });
     });
});
