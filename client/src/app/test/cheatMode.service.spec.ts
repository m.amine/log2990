import * as THREE from "three";
import { KeyBoardInputs } from "../../../../common/models/camera";
import { GeometricObjectType, IGeometricObject } from "../../../../common/models/iGeometricObject";
import { IScene, ObjectColor, SceneType } from "../../../../common/models/iScene";
import { CheatModeService } from "../services/cheatMode.service";

const SCENE_OBJECTS: number = 2;
const originalScene: IScene = {} as IScene;
let cheatMode: CheatModeService;
let differencesId: string[];

describe("CheatModeService", () => {
    beforeEach(() => {
        originalScene.backgroundColor = 0x000000;
        originalScene.objects = [];

        for (let i: number = 0; i < SCENE_OBJECTS; i++) {
            const sceneObject: IGeometricObject = {} as IGeometricObject;
            sceneObject.id = "" + i;
            sceneObject.type = GeometricObjectType.BOX;
            sceneObject.w = 0;
            sceneObject.h = 0;
            sceneObject.d = 0;
            sceneObject.r = 0;
            sceneObject.hexColor = ObjectColor.WHITE;
            sceneObject.position = {x: 0, y: 0, z: 0};
            sceneObject.rotation = {x: 0, y: 0, z: 0};
            sceneObject.castShadow = true;
            originalScene.objects.push(sceneObject);
        }
        originalScene.type = SceneType.CLASSIC;
    });

    it("should have zero red objects in the scene", () => {
        differencesId = [];
        cheatMode = new CheatModeService(originalScene, differencesId, ObjectColor.WHITE);
        let redObjects: number = 0;
        cheatMode.cheatedSceneData.objects.forEach((cheatedObject: IGeometricObject) => {
            if (cheatedObject.hexColor === ObjectColor.RED) {
                redObjects++;
            }
        });
        expect(redObjects).toEqual(differencesId.length);
    });
    it("should have one red objects in the scene", () => {
        differencesId = ["0"];
        cheatMode = new CheatModeService(originalScene, differencesId, ObjectColor.WHITE);
        let redObjects: number = 0;
        cheatMode.cheatedSceneData.objects.forEach((cheatedObject: IGeometricObject) => {
            if (cheatedObject.hexColor === ObjectColor.RED) {
                redObjects++;
            }
        });
        expect(redObjects).toEqual(differencesId.length);
    });
    it("should have two red objects in the scene", () => {
        differencesId = ["0", "1"];
        cheatMode = new CheatModeService(originalScene, differencesId, ObjectColor.WHITE);
        let redObjects: number = 0;
        cheatMode.cheatedSceneData.objects.forEach((cheatedObject: IGeometricObject) => {
            if (cheatedObject.hexColor === ObjectColor.RED) {
                redObjects++;
            }
        });
        expect(redObjects).toEqual(differencesId.length);
    });
    it("should toggle the cheat mode", () => {
        cheatMode = new CheatModeService(originalScene, [], ObjectColor.WHITE);
        const beforeToggle: boolean = cheatMode.toggle;
        cheatMode.toggleCheatMode(KeyBoardInputs.CHEAT);
        const afterToggle: boolean = cheatMode.toggle;
        expect(beforeToggle === afterToggle).toBeFalsy();
    });
    it("should be equal to the threeScene", () => {
        const threeScene: THREE.Scene = new THREE.Scene();
        cheatMode = new CheatModeService(originalScene, differencesId, ObjectColor.WHITE);
        expect(threeScene).toEqual(cheatMode.chooseMode(threeScene));
    });
    it("should be equal to the threeScene when blinking % framerate > framerate / 2", () => {
        const threeScene: THREE.Scene = new THREE.Scene();
        cheatMode = new CheatModeService(originalScene, differencesId, ObjectColor.WHITE);
        cheatMode.toggle = true;
        // tslint:disable-next-line:no-magic-numbers
        for (let i: number = 0; i < cheatMode.frameRate / 2 + 1; i++) {
            cheatMode.chooseMode(threeScene);
        }
        expect(threeScene).toEqual(cheatMode.chooseMode(threeScene));
    });

});
