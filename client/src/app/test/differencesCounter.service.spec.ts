/* tslint:disable */
import { DifferencesCounterService } from "../services/differencesCounter.service";

let counter: DifferencesCounterService;

describe("DifferencesCounterService", () => {
    beforeEach(() => {
       counter = new DifferencesCounterService();

    });

    it(" should incremente the counter ", () => {
       counter.incrementCounter();
       counter.incrementCounter();
       counter.incrementCounter();

       expect(counter.displayTotalCounter()).toEqual(3);
  });

    it(" should reset the counter ", () => {
       counter.incrementCounter();
       counter.incrementCounter();
       counter.incrementCounter();

       counter.resetCounter();
       expect(counter.displayTotalCounter()).toEqual(0);

      });

});
