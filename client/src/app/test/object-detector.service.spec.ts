import { HttpClient } from "@angular/common/http";
import { Scene, WebGLRenderer } from "three";
import { GeometricObjectType, IGeometricObject } from "../../../../common/models/iGeometricObject";
import { IScene as ObjScene, ObjectColor } from "../../../../common/models/iScene";
import { CameraMovementService } from "../services/cameraMovement.service";
import { ObjectDetectorService } from "../services/object-detector.service";
import { SceneParserService } from "../services/sceneParser.service";
import { VerifyDifferenceService } from "../services/verify-difference.service";

interface MousePosition {
    x: number;
    y: number;
}

// tslint:disable-next-line:no-any
describe("ObjectDetectorService", () => {
    // tslint:disable-next-line:no-any
    let raycasterSpy: any;
    let lenghtIntersection: number;
    let service: ObjectDetectorService;
    let httpClientSpy: HttpClient;
    let meshMap: [string, number][];
    let diffrencesId: string[];
    let camera: CameraMovementService;
    let mouse: MousePosition;
    let scene: Scene;
    let renderer: WebGLRenderer;
    let verificationService: VerifyDifferenceService;
    // tslint:disable-next-line:prefer-const
    let objScene: ObjScene;
    let parserService: SceneParserService;

    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj("HttpClient", ["post"]);
        verificationService = new VerifyDifferenceService(httpClientSpy);
        // tslint:disable-next-line:no-magic-numbers
        meshMap = [["id1", 1], ["id2", 2]];
        diffrencesId = ["id1", "id2", "id3"];
        camera = new CameraMovementService();
        mouse = {x: 200, y: 200};
        scene = new Scene();
        objScene = {} as ObjScene;
        objScene.objects = [];
        renderer = new WebGLRenderer();
        raycasterSpy = jasmine.createSpyObj("ObjectDetectorService", ["raycasterMesh"]);

        service = new ObjectDetectorService(httpClientSpy, meshMap, diffrencesId);
    });

// tslint:disable-next-line:no-any
    it("it should not call the isDifferent function ",  async() => {
        lenghtIntersection = 0;
        raycasterSpy.raycasterMesh.and.returnValue(lenghtIntersection);
        service.detect(camera.camera, mouse, scene, renderer);
        spyOn(verificationService , "isDifference").and.callThrough();
        expect(verificationService.isDifference).not.toHaveBeenCalled();
    });
    it("it should call the isDifferent function ",  async() => {
        // tslint:disable-next-line:no-magic-numbers
        lenghtIntersection = 2;
        raycasterSpy.raycasterMesh.and.returnValue(lenghtIntersection);
        service.detect(camera.camera, mouse, scene, renderer);
        spyOn(verificationService , "isDifference").and.callThrough();
        expect(verificationService.isDifference).not.toHaveBeenCalled();
    });
    it("the raycaster method should not intersect with a scene object (return 0)",  async() => {
        camera.camera.position.x = 0;
        camera.camera.position.y = 0;
        camera.camera.position.z = 0;
        const sceneObject: IGeometricObject = {} as  IGeometricObject;
        sceneObject.id = "0";
        // tslint:disable-next-line:no-magic-numbers
        sceneObject.position = {x: 0, y: 0, z: -10};
        sceneObject.rotation = {x: 0, y: 0, z: 0};
        // tslint:disable-next-line:no-magic-numbers
        sceneObject.w = 10; sceneObject.h = 10; sceneObject.d = 10; sceneObject.r = 10;
        sceneObject.type = GeometricObjectType.BOX;
        sceneObject.hexColor = ObjectColor.BLUE;
        sceneObject.castShadow = false;
        objScene.objects.push(sceneObject);
        parserService = new SceneParserService(objScene);
        // tslint:disable-next-line:no-magic-numbers
        renderer.setSize(512, 384);
        const length: number = service.raycasterMesh(camera.camera , {x: 0 , y: 0} , parserService.getParsedScene()[0], renderer);
        expect(length).toEqual(0);
    });

});
