import { MeshMap } from "../services/sceneParser.service";
import { VerifyDifferenceService } from "../services/verify-difference.service";

describe("VerifyDifferenceService", () => {
     // tslint:disable-next-line:no-any
     let httpClientSpy: any;
     let verificationService: VerifyDifferenceService;
     beforeEach(() => {
       httpClientSpy = jasmine.createSpyObj("HttpClient", ["post"]);
       verificationService = new VerifyDifferenceService(httpClientSpy);
     });
     it("should  call the initDifferenceRequest",  async() => {
         const meshMap: MeshMap[] = [["id1", 1]];
         const difference: string[] = ["id1", "id2"];
         // tslint:disable-next-line:no-any
         const spyInitDiffRequest: any = spyOn<any>(verificationService, "initDifferenceRequest");
         verificationService.isDifference( 1, meshMap , difference);
         expect(spyInitDiffRequest).toHaveBeenCalled();
        });
});
