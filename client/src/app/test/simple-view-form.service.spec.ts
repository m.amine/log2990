
import { ImageProcessingStatus as IPStatus } from "../../../../common/communication/errors";
import { TestHelper } from "../../test.helper";
import { SimpleViewFormService } from "../services/simple-view-form.service";

// tslint:disable-next-line:no-any Used to mock the http call
let httpClientSpy: any;
let service: SimpleViewFormService;
// tslint:disable-next-line:max-line-length
const mockImage1: string = "iVBORw0KGgoAAAANSUhEUgAAAA0AAAAKCAIAAADgjEOTAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAAUSURBVChTY/hPHBhVhx1QV93//wCSoYSKpKtKNAAAAABJRU5ErkJggg==";
// tslint:disable-next-line:max-line-length
const mockImage2: string = "iVBORw0KGgoAAAANSUhEUgAAAA0AAAAKCAIAAADgjEOTAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAAAAUSURBVChTY/hPHBhVhx1QV93//wCSoYSKpKtKNAAAAABJRU5ErkJggg==";
// tslint:disable-next-line:no-any
const res1: any = {status: IPStatus.S_OK, imgs: [mockImage1, mockImage2]};
// tslint:disable-next-line:no-any
const res2: any = {status: IPStatus.E_INVALID_DIFFERENCES_COUNT, imgs: [mockImage1, mockImage2]};
describe("SimpleViewFormService", () => {

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj("HttpClient", ["post"]);
    service = new SimpleViewFormService(httpClientSpy);
  });

  it("should post the correct data with mocked call Ok (HTTPClient called once)", () => {
    httpClientSpy.post.and.returnValue(TestHelper.asyncData(res1));

    service.submitSimpleForm({mockImage1, mockImage2}).then(() => {
      expect(service.response.status).toEqual(IPStatus.S_OK);
    });

    expect(httpClientSpy.post.calls.count()).toBe(1, "one call");
  });

  it("should post the correct data with mocked call error (HTTPClient called once)", () => {
    httpClientSpy.post.and.returnValue(TestHelper.asyncData(res2));
    service.submitSimpleForm({mockImage1, mockImage2}).then(() => {
      expect(service.response.status).toEqual(IPStatus.E_INVALID_DIFFERENCES_COUNT);
    });

    expect(httpClientSpy.post.calls.count()).toBe(1, "one call");
  });

  it ("should handle the reponse correctly" , () => {
    const firstServiceStatus: number = 1;
    const secondServiceStatus: number = 2;
    const thirdServiceStatus: number = 3;

    service.response = res1;
    service.response.status = firstServiceStatus;
    service.responseHandler();
    expect(service.imageStatus[firstServiceStatus]).toBeTruthy();
    service.response.status = secondServiceStatus;
    service.responseHandler();
    expect(service.imageStatus[secondServiceStatus]).toBeTruthy();
    service.response.status = thirdServiceStatus;
    service.responseHandler();
    expect(service.imageStatus[thirdServiceStatus]).toBeTruthy();
  });

});
