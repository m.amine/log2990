/* tslint:disable */
import { UserLoginStatus } from "../../../../common/communication/errors";
import { LoginService } from "../services/Login.service";

let service: LoginService;
// tslint:disable-next-line:no-any
let httpClientSpy: any;
describe("LoginService", () => {
    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj("HttpClient", ["get"]);
        service = new LoginService(httpClientSpy);
    });

    it(" Test authentificate user name ", () => {
        service.login("Nicolas").then(async () => {
            await expect(service.loginStatus).toBe(UserLoginStatus.S_OK);
        }).catch(null);
    });

    it(" Test invalid username when some user already connect with the same username ", () => {
        service.login("Nicolas").then(async () => {
            await expect(service.loginStatus).toBe(UserLoginStatus.E_USER_ALREADY_CONNECTED);
        }).catch(null);
    });

    it(" Test invalid username (non-alphanumeric characters) ", () => {
        service.login("Nicolas1234").then(async () => {
            await expect(service.loginStatus).toBe(UserLoginStatus.E_INVALID_USER_NAME);
        }).catch(null);
    });
});
