import * as THREE from "three";
import { KeyBoardInputs } from "../../../../common/models/camera";
import { CameraMovementService } from "../services/cameraMovement.service";

const xPosition: number = 0;
const yPosition: number = 100;
const zPosition: number = 0;
let scene: THREE.Scene;
let mesh: THREE.Mesh;
const meshRadius: number = 10;
let cameraMovement: CameraMovementService;

describe("CameraCollisionDetectionService", () => {
    beforeEach(() => {
        cameraMovement = new CameraMovementService();
        scene = new THREE.Scene();
        mesh = new THREE.Mesh(new THREE.SphereGeometry(meshRadius), new THREE.MeshBasicMaterial());
        THREE.GeometryUtils.center(mesh.geometry);
        mesh.position.set(xPosition, yPosition, zPosition);
        scene.add(mesh);
    });
    it(" should not add a scene to the virtualScene", () => {
        expect(cameraMovement.cameraCollisionDetection.virtualScene.children.length).toEqual(0);
    });

    it(" should add a scene to the virtualScene", () => {
        cameraMovement.cameraCollisionDetection.addSceneToDetect(scene);
        expect(cameraMovement.cameraCollisionDetection.virtualScene.children.length).toEqual(scene.children.length);
    });

    it(" should add two scenes to the virtualScene", () => {
        cameraMovement.cameraCollisionDetection.addSceneToDetect(scene);
        cameraMovement.cameraCollisionDetection.addSceneToDetect(scene);
        // tslint:disable-next-line:no-magic-numbers
        expect(cameraMovement.cameraCollisionDetection.virtualScene.children.length).toEqual(scene.children.length * 2);
    });

    it(" should not detect a collision with a mesh behind the camera", () => {
        mesh.position.set(xPosition, yPosition, zPosition + meshRadius + 1 + cameraMovement.cameraCollisionDetection.cameraRadius);
        cameraMovement.cameraCollisionDetection.addSceneToDetect(scene);
        const oldPosition: THREE.Vector3 = cameraMovement.camera.position.clone();
        cameraMovement.moveCamera(KeyBoardInputs.FORWARD);
        expect(oldPosition.x).toEqual(cameraMovement.camera.position.x);
        expect(oldPosition.y).toEqual(cameraMovement.camera.position.y);
        expect(oldPosition.z).toEqual(cameraMovement.camera.position.z + cameraMovement.step);
    });

    it(" should not detect a collision with a mesh too far from the camera", () => {
        mesh.position.set(xPosition, yPosition, zPosition - meshRadius - 1 - cameraMovement.cameraCollisionDetection.cameraRadius);
        cameraMovement.cameraCollisionDetection.addSceneToDetect(scene);
        const oldPosition: THREE.Vector3 = cameraMovement.camera.position.clone();
        cameraMovement.moveCamera(KeyBoardInputs.FORWARD);
        expect(oldPosition.x).toEqual(cameraMovement.camera.position.x);
        expect(oldPosition.y).toEqual(cameraMovement.camera.position.y);
        expect(oldPosition.z).toEqual(cameraMovement.camera.position.z + cameraMovement.step);
    });

    it(" should not detect a collision to the left with a mesh in front of the camera", () => {
        mesh.position.set(xPosition, yPosition, zPosition + meshRadius - 1);
        cameraMovement.cameraCollisionDetection.addSceneToDetect(scene);
        const oldPosition: THREE.Vector3 = cameraMovement.camera.position.clone();
        cameraMovement.moveCamera(KeyBoardInputs.LEFT);
        expect(oldPosition.x).toEqual(cameraMovement.camera.position.x + cameraMovement.step);
        expect(oldPosition.y).toEqual(cameraMovement.camera.position.y);
        expect(oldPosition.z).toEqual(cameraMovement.camera.position.z);
    });

    it(" should not detect a collision to the left with a mesh in front of the camera", () => {
        mesh.position.set(xPosition, yPosition, zPosition + meshRadius - 1);
        cameraMovement.cameraCollisionDetection.addSceneToDetect(scene);
        const oldPosition: THREE.Vector3 = cameraMovement.camera.position.clone();
        cameraMovement.moveCamera(KeyBoardInputs.RIGHT);
        expect(oldPosition.x).toEqual(cameraMovement.camera.position.x - cameraMovement.step);
        expect(oldPosition.y).toEqual(cameraMovement.camera.position.y);
        expect(oldPosition.z).toEqual(cameraMovement.camera.position.z);
    });

    it(" should not detect a collision behind with a mesh in front of the camera", () => {
        mesh.position.set(xPosition, yPosition, zPosition + meshRadius - 1);
        cameraMovement.cameraCollisionDetection.addSceneToDetect(scene);
        const oldPosition: THREE.Vector3 = cameraMovement.camera.position.clone();
        cameraMovement.moveCamera(KeyBoardInputs.BACKWARDS);
        expect(oldPosition.x).toEqual(cameraMovement.camera.position.x);
        expect(oldPosition.y).toEqual(cameraMovement.camera.position.y);
        expect(oldPosition.z).toEqual(cameraMovement.camera.position.z - cameraMovement.step);
    });
});
