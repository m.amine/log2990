import { TestHelper } from "../../test.helper";
import { AdminService } from "../services/admin.service";
// tslint:disable-next-line:no-any Used to mock the http call
let httpClientSpy: any;
let serviceG: AdminService;

describe("AdminService", () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj("HttpClient", ["post"]);
    serviceG = new AdminService(httpClientSpy);
  });

  it(" should create the game (HttpClient called once) ", () => {

    const verify: boolean = true;
    const body: string = "";

    httpClientSpy.post.and.returnValue(TestHelper.asyncData(verify));

    // check the content of the mocked call
    serviceG.create(body).subscribe((result: boolean) => {
        expect(result).toBe(true);
    },
     );

    // check if only one call was made
    expect(httpClientSpy.post.calls.count()).toBe(1, "one call");
  });
});
