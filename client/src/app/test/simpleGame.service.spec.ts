import { SimpleGame } from "../../../../common/models/simpleGame";
import { TestHelper } from "../../test.helper";
import { SimpleGameService } from "../services/simpleGame.service";

// tslint:disable-next-line:no-any Used to mock the http call
let httpClientSpy: any;
let service: SimpleGameService;
describe("SimpleGameService", () => {

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj("HttpClient", ["get", "patch", "delete", "put"]);
    service = new SimpleGameService(httpClientSpy);
  });

  it(" should get all the game (HttpClient called once) ", () => {

    const games: SimpleGame[] = [];

    httpClientSpy.get.and.returnValue(TestHelper.asyncData(games));

    // check the content of the mocked call
    service.getAll().subscribe(
      (result: SimpleGame[]) => {
        expect(result.length).toBeGreaterThanOrEqual(0);
      },
    );

    // check if only one call was made
    expect(httpClientSpy.get.calls.count()).toBe(1, "one call");
  });

  it(" should  reset game  (HttpClient called once) ", () => {

    const id: string = "";
    httpClientSpy.patch.and.returnValue(TestHelper.asyncData(""));

    // check the content of the mocked call
    service.resetGame(id);

    // check if only one call was made
    expect(httpClientSpy.patch.calls.count()).toBe(1, "one call");
  });

  it(" should delete game  (HttpClient called once) ", () => {

    const id: string = "";
    httpClientSpy.delete.and.returnValue(TestHelper.asyncData(""));

    // check the content of the mocked call
    service.deleteGame(id);

    // check if only one call was made
    expect(httpClientSpy.delete.calls.count()).toBe(1, "one call");
  });

  it(" should get position of solo Game Record (HttpClient called once)", () => {
    const id: string = "";
    const name: string = "hanane";
    const score: number = 4;

    httpClientSpy.put.and.returnValue(TestHelper.asyncData(""));
    service.getPositionOfSoloGameRecord(id, name, score);

    expect(httpClientSpy.put.calls.count()).toBe(1, "one call");
  });

  it(" should update solo Game Record (HttpClient called once)", () => {

    const id: string = "";
    const name: string = "space";
    const score: number = 5;

    httpClientSpy.put.and.returnValue(TestHelper.asyncData(""));
    service.updateSoloGameRecord(id, name, score);

    expect(httpClientSpy.put.calls.count()).toBe(1, "one call");
  });

  it(" should get position of Multi Game Record (HttpClient called once)", () => {
    const id: string = "";
    const name: string = "4season";
    const score: number = 14;

    httpClientSpy.put.and.returnValue(TestHelper.asyncData(""));
    service.getPositionOfMultiGameRecord(id, name, score);

    expect(httpClientSpy.put.calls.count()).toBe(1, "one call");
  });

  it(" should update Multi Game Record (HttpClient called once)", () => {

    const id: string = "";
    const name: string = "music";
    const score: number = 8;

    httpClientSpy.put.and.returnValue(TestHelper.asyncData(""));
    service.updateMultiGameRecord(id, name, score);

    expect(httpClientSpy.put.calls.count()).toBe(1, "one call");
  });

});
