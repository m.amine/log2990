// import * as THREE from "three";
import { IGeometricObject } from "../../../../common/models/iGeometricObject";
import { IScene, SceneType } from "../../../../common/models/iScene";
// import { SceneParserService } from "../services/sceneParser.service";

const nbObjects: number = 5;
const size: number = 10;
const sceneClassic: IScene = {} as IScene;
// tslint:disable-next-line:no-magic-numbers
jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;

describe("sceneParserServiceGeometric", () => {
    beforeAll(() => {
        sceneClassic.type = SceneType.CLASSIC;
        sceneClassic.objects = [];
        sceneClassic.decoration = [];

        for (let i: number = 0; i < nbObjects; i++) {
            const sceneObject: IGeometricObject = {} as IGeometricObject;
            sceneObject.type = i;
            sceneObject.w = size;
            sceneObject.h = size;
            sceneObject.d = size;
            sceneObject.r = size;
            sceneObject.hexColor = 0;
            sceneObject.position = {x: 0, y: 0, z: 0};
            sceneObject.rotation = {x: 0, y: 0, z: 0};
            sceneClassic.objects.push(sceneObject);
        }

    });

    // it(" Test getting parsed scene", (done) => {
    //     const service: SceneParserService = new SceneParserService(sceneClassic);
    //     service.parse();

    //     const tsc: THREE.Scene = service.getParsedScene()[0];
    //     let meshCounter: number = 0;
    //     tsc.traverse((node: THREE.Object3D) => {
    //         if (node instanceof THREE.Mesh) {
    //             if (node.isMesh) {
    //                 meshCounter++;
    //             }
    //         }
    //     });

    //     expect(meshCounter).toBeGreaterThanOrEqual(nbObjects);
    //     done();
    // });
});
