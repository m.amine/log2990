/* tslint:disable */
import { ChronometerService } from "../services/chronometer.service";

let service: ChronometerService;


describe("Chronometer Service ", () => {
  const intervalTime: number = 10000;
  beforeEach(() => {
    service = new ChronometerService();
  })

  it(" verify if the chronometer start and return a good value ", () => {
    service.startChronometer()
    setTimeout((): void => { expect(service.displayChronometer()).toEqual("00:10") }, intervalTime);

  });

  it(" verify if the chronometer can stop and return a good value  ", () => {
    service.startChronometer()
    setTimeout((): void => {
      service.stopChronometer();
      expect(service.displayChronometer()).toEqual("00:10")
    },
    intervalTime);

  });

  it(" verify if the chronometer is in a MM:ss format ", () => {
    service.displayChronometer();
    expect(typeof service === "string");
    expect(service.displayChronometer()).toEqual("00:00");

  });
});
