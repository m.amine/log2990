/* tslint:disable */
import { FreeGame } from "../../../../common/models/freeGame";
import { TestHelper } from "../../test.helper";
import { FreeGameService } from "../services/freeGame.service";

// tslint:disable-next-line:no-any Used to mock the http call
let httpClientSpy: any;
let serviceG: FreeGameService;
describe("FreeGameService", () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj("HttpClient", ["get", "put", "patch", "delete"]);
    serviceG = new FreeGameService(httpClientSpy);
  });

  it(" should get all the game (HttpClient called once) ", async () => {

    const games: FreeGame[] = [];

    httpClientSpy.get.and.returnValue(TestHelper.asyncData(games));

    // check the content of the mocked call
    serviceG.getAll().subscribe(
      async (result: FreeGame[]) => {
       await expect(result.length).toBeGreaterThanOrEqual(0);
      },
    );

    // check if only one call was made
    await expect(httpClientSpy.get.calls.count()).toBe(1, "one call");
  });

  it(" should  reset game  (HttpClient called once) ", async () => {

    const id: string = "";
    httpClientSpy.patch.and.returnValue(TestHelper.asyncData(""));

    // check the content of the mocked call
    serviceG.resetGame(id).then().catch();

    // check if only one call was made
    await expect(httpClientSpy.patch.calls.count()).toBe(1, "one call");
  });

  it(" should delete game  (HttpClient called once) ", async () => {

    const id: string = "";
    httpClientSpy.delete.and.returnValue(TestHelper.asyncData(""));

    // check the content of the mocked call
    serviceG.deleteGame(id).then().catch();

    // check if only one call was made
    await expect(httpClientSpy.delete.calls.count()).toBe(1, "one call");
  });

  it(" should get position of solo Game Record (HttpClient called once)", async () => {

    const id: string = "";
    const name: string = "hanane";
    const score: number = 4;

    httpClientSpy.put.and.returnValue(TestHelper.asyncData(""));
    serviceG.getPositionOfSoloGameRecord(id, name, score);

    await expect(httpClientSpy.put.calls.count()).toBe(1, "one call");
  });

  it(" should update solo Game Record (HttpClient called once)", async () => {

    const id: string = "";
    const name: string = "space";
    const score: number = 5;

    httpClientSpy.put.and.returnValue(TestHelper.asyncData(""));
    serviceG.updateSoloGameRecord(id, name, score);

    await expect(httpClientSpy.put.calls.count()).toBe(1, "one call");
  });

  it(" should get position of multi Game Record (HttpClient called once)", async () => {

    const id: string = "";
    const name: string = "4season";
    const score: number = 9;

    httpClientSpy.put.and.returnValue(TestHelper.asyncData(""));
    serviceG.getPositionOfMultiGameRecord(id, name, score);

    await expect(httpClientSpy.put.calls.count()).toBe(1, "one call");
  });

  it(" should update multi Game Record (HttpClient called once)", async () => {

    const id: string = "";
    const name: string = "music";
    const score: number = 15;

    httpClientSpy.put.and.returnValue(TestHelper.asyncData(""));
    serviceG.updateMultiGameRecord(id, name, score);

    await expect(httpClientSpy.put.calls.count()).toBe(1, "one call");
  });
});
