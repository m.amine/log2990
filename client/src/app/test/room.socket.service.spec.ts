// tslint:disable
import { Room } from "../../../../common/communication/room";
import { FreeGame } from "../../../../common/models/freeGame";
import { Point } from "../../../../common/models/point";
import { SimpleGame } from "../../../../common/models/simpleGame";
import { TestHelper } from "../../test.helper";
import {RoomSocketService } from "../services/room.socket.service";

// tslint:disable-next-line:no-any
let socketSpy: any;
let service: RoomSocketService;

describe("Room Socket Service", () => {

    beforeEach(() => {
        socketSpy = jasmine.createSpyObj("SocketIOClient.Socket", ["on", "emit"]);
        service = new RoomSocketService();
        service.initSocket(socketSpy);
    });

    it("Should get simpleGame action ", () => {

        const game: SimpleGame[] = new Array<SimpleGame>();
        socketSpy.on.and.returnValue(TestHelper.asyncData(game));
        // check the content of the mocked call
        service.onGetSimpleGamesAction().subscribe((games) => undefined, fail );
        // check if only one call was made
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should get freeGame action ", () => {

        const game: FreeGame[] = new Array<FreeGame>();
        socketSpy.on.and.returnValue(TestHelper.asyncData(game));
        // check the content of the mocked call
        service.onGetFreeGamesAction().subscribe((games) => undefined, fail );
        // check if only one call was made
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("should receive the newly updated game scenes ", () => {

        const game: FreeGame[] = new Array<FreeGame>();
        socketSpy.on.and.returnValue(TestHelper.asyncData(game));
        // check the content of the mocked call
        service.onUpdateScenes().subscribe((games) => undefined, fail );
        // check if only one call was made
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("should recieve the valid difference location ", () => {

        const point: [Point, Point] = [new Point(50, 50), new Point(50, 50)];
        socketSpy.on.and.returnValue(TestHelper.asyncData(point));
        // check the content of the mocked call
        service.onUpdateImage().subscribe((games) => undefined, fail );
        // check if only one call was made
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should start simpleGame  ", () => {

        const game: SimpleGame[] = new Array<SimpleGame>();
        socketSpy.on.and.returnValue(TestHelper.asyncData(game));
        // check the content of the mocked call
        service.onStartSimpleGame().subscribe(() => undefined, fail );
        // check if only one call was made
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should start freeGame  ", () => {

        const game: FreeGame[] = new Array<FreeGame>();
        socketSpy.on.and.returnValue(TestHelper.asyncData(game));
        // check the content of the mocked call
        service.onStartFreeGame().subscribe(() => undefined, fail );
        // check if only one call was made
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should  increment the counter for the first player ", () => {

        const game: FreeGame[] = new Array<FreeGame>();
        socketSpy.on.and.returnValue(TestHelper.asyncData(game));
        // check the content of the mocked call
        service.onFirstCounterIncrement().subscribe(() => undefined, fail );
        // check if only one call was made
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should  increment the counter for the second player ", () => {

        const game: FreeGame[] = new Array<FreeGame>();
        socketSpy.on.and.returnValue(TestHelper.asyncData(game));
        // check the content of the mocked call
        service.onSecondCounterIncrement().subscribe(() => undefined, fail );
        // check if only one call was made
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should get simpleGame action ", () => {

        const game: SimpleGame[] = new Array<SimpleGame>();
        socketSpy.emit.and.returnValue(TestHelper.asyncData(game));
        // check the content of the mocked call
        service.getSimpleGamesAction(game);
        // check if only one call was made
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });

    it("Should get freeGame action ", () => {

        const game: FreeGame[] = new Array<FreeGame>();
        socketSpy.emit.and.returnValue(TestHelper.asyncData(game));
        // check the content of the mocked call
        service.getFreeGamesAction(game);
        // check if only one call was made
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });

    it("Should emit with Room information for join in Simple Room ", () => {

        const room: Room = new Room("hanane", "boussari");
        socketSpy.emit.and.returnValue(TestHelper.asyncData(room));
        // check the content of the mocked call
        service.emitJoinForSimpleRoom(room);
        // check if only one call was made
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });

    it("Should emit with Room information for join in Free Room ", () => {

        const room: Room = new Room("hanane", "boussari");
        socketSpy.emit.and.returnValue(TestHelper.asyncData(room));
        // check the content of the mocked call
        service.emitJoinForFreeRoom(room);
        // check if only one call was made
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });

    it("Should emit with room information for create room ", () => {

        const room: Room = new Room("hanane", "boussari");
        socketSpy.emit.and.returnValue(TestHelper.asyncData(room));
        // check the content of the mocked call
        service.emitCreateRoom(room);
        // check if only one call was made
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });

    it("Should emit the game with the newly updated scenes", () => {

        const room: Room = new Room("hanane", "boussari");
        socketSpy.emit.and.returnValue(TestHelper.asyncData(room));
        // check the content of the mocked call
        service.emitCreateRoom(room);
        // check if only one call was made
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });

    it("Should create a room ", () => {

        const nameOfRoom: string = "hanane";
        socketSpy.on.and.returnValue(TestHelper.asyncData(nameOfRoom));
        // check the content of the mocked call
        service.createRoom().subscribe((roomId: string) => {});
        // check if only one call was made
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should join a Room ", () => {
        const nameOfRoom: string = "hanane";
        socketSpy.on.and.returnValue(TestHelper.asyncData(nameOfRoom));
        // check the content of the mocked call
        service.onJoined().subscribe((roomId: string) => {});
        // check if only one call was made
        expect(socketSpy.on.calls.count()).toBe(1, "one call");
    });

    it("Should emit for leave a Room ", () => {

        const room: Room = new Room("hanane", "boussari");
        socketSpy.emit.and.returnValue(TestHelper.asyncData(room));
        // check the content of the mocked call
        service.leaveRoom(room);
        // check if only one call was made
        expect(socketSpy.emit.calls.count()).toBe(1, "one call");
    });

});
