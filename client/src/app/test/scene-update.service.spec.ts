import { TestBed } from "@angular/core/testing";
import { FreeGame } from "../../../../common/models/freeGame";
import { GeometricObjectType, IGeometricObject } from "../../../../common/models/iGeometricObject";
import {IScene, ObjectColor, SceneType } from "../../../../common/models/iScene";
import { SceneUpdateService } from "../services/scene-update.service";

let scene: IScene;
let modifiedScene: IScene;
const sceneUpdateService: SceneUpdateService = new SceneUpdateService();

describe("SceneUpdateService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));
  scene = {} as IScene;
  scene.objects = [];
  modifiedScene = {} as IScene;
  modifiedScene.objects = [];

  // tslint:disable-next-line:no-magic-numbers
  for (let i: number = 0; i < 2; i++) {
    const sceneObject: IGeometricObject = {} as IGeometricObject;
    sceneObject.id = "" + i;
    sceneObject.type = GeometricObjectType.BOX;
    sceneObject.w = 0;
    sceneObject.h = 0;
    sceneObject.d = 0;
    sceneObject.r = 0;
    sceneObject.hexColor = ObjectColor.WHITE;
    sceneObject.position = {x: 0, y: 0, z: 0};
    sceneObject.rotation = {x: 0, y: 0, z: 0};
    sceneObject.castShadow = true;
    scene.objects.push(sceneObject);
    modifiedScene.objects.push({} as IGeometricObject);
  }
  scene.type = SceneType.CLASSIC;

  // it("should splice the clicked object from the modified scene", () => {
  //   const beforeModifiedSceneLength: number = modifiedScene.objects.length;
  //   const game: FreeGame = {
  //     gameId: "0",
  //     name: "0",
  //     thumbnail: "0",
  //     originalScene: scene,
  //     modifiedScene: modifiedScene,
  //     differencesId: [],
  //     soloTime: [],
  //     vsTime: [],
  //     action: "creer",
  //   };
  //   sceneUpdateService.updateModifiedScene(modifiedScene, "2", game);
  //   const afterModifiedSceneLength: number = modifiedScene.objects.length;

  //   expect(beforeModifiedSceneLength - afterModifiedSceneLength).toEqual(1);
  // });
  it("should add the clicked object to the modified scene", () => {
    const beforeModifiedSceneLength: number = modifiedScene.objects.length;
    const game: FreeGame = {
      gameId: "0",
      name: "0",
      thumbnail: "0",
      originalScene: scene,
      modifiedScene: modifiedScene,
      differencesId: ["1"],
      soloTime: [],
      vsTime: [],
      action: "creer",
    };
    sceneUpdateService.updateModifiedScene(modifiedScene, "1", game);
    const afterModifiedSceneLength: number = modifiedScene.objects.length;

    expect(afterModifiedSceneLength - beforeModifiedSceneLength).toEqual(1);
  });
});
