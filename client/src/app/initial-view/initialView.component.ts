import { Component } from "@angular/core";
import { LoginService } from "../services/Login.service";
import { SocketService } from "../services/socket.service";

@Component({
  selector: "app-initial-view",
  templateUrl: "./initialView.component.html",
  styleUrls: ["./initialView.component.css"],
})
export class InitialViewComponent {
  public name: string = "";

  public constructor(public loginService: LoginService,
                     private socketService: SocketService) {}

  public connectUser(): void {
    this.loginService.login(this.name)
      .then(() => {
        this.socketService.emit<String>("login", this.name);
      })
      .catch(); // login failed=> handling will be treated inside loginService
  }
}
