import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { Point } from "../../../../common/models/point";
import { SimpleGame } from "../../../../common/models/simpleGame";

@Component({
  selector: "app-simple-view-image",
  templateUrl: "./simple-view-image.component.html",
  styleUrls: ["./simple-view-image.component.css"],
})
export class SimpleViewImageComponent {
  @ViewChild("myCanvas") public canvas: ElementRef;
  @Input() private imagePath: string;
  @Input() public currentGame: SimpleGame;

  @Output() public clickEvent: EventEmitter<[Point, Point]> = new EventEmitter<[Point, Point]>();

  private context: CanvasRenderingContext2D | null;
  public canvasElement: HTMLCanvasElement;
  private image: HTMLImageElement;

  public loadImageInCanvas(): void {
    this.canvasElement = this.canvas.nativeElement;
    this.context  = this.canvasElement.getContext("2d");

    this.image = new Image();
    this.image.crossOrigin = "anonymous";
    this.image.addEventListener("load", () => {
      if (this.context != null) {
        this.context.drawImage(this.image, 0, 0, this.canvasElement.width, this.canvasElement.height);
      }
    });
    this.image.src = this.imagePath;
    this.image.id = "img";
    this.canvasElement.addEventListener("click", (event: MouseEvent) => this.getElementPosition(event), false);
  }

  public getElementPosition(event: MouseEvent): void {
    const point: Point = new Point(event.layerX, event.layerY);
    this.clickEvent.emit([point, new Point(event.x, event.y)]);
  }
}
