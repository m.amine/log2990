import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material";
import { MouseInputs, Point2D } from "../../../../common/models/camera";
import { FreeGame } from "../../../../common/models/freeGame";
import { ChronometerComponent } from "../chronometer/chronometer.component";
import { FreeViewImageComponent } from "../free-view-image/free-view-image.component";
import { LosingDialogComponent } from "../losing-dialog/losing-dialog.component";
import { CameraMovementService } from "../services/cameraMovement.service";
import { FreeGameService } from "../services/freeGame.service";
import { MessageSocketService } from "../services/message.socket.service";
import { RoomSocketService } from "../services/room.socket.service";
import { SceneUpdateService } from "../services/scene-update.service";
import { WinningDialogComponent } from "../winning-dialog/winning-dialog.component";

const successAudio: string = "../../assets/youSmart.mp3";
@Component({
  selector: "app-free-game-view-multi-player",
  templateUrl: "./free-game-view-multi-player.component.html",
  styleUrls: ["./free-game-view-multi-player.component.css"],
})
export class FreeGameViewMultiPlayerComponent implements  OnInit {
  @ViewChild("background") public background: ElementRef;
  @ViewChild("chrono") public chrono: ChronometerComponent;
  @Output() public finish: EventEmitter<void>;
  @Output() public restart: EventEmitter<FreeGame>;
  @ViewChild("modifiedScene") public modifiedScene: FreeViewImageComponent;
  @ViewChild("originalScene") public originalScene: FreeViewImageComponent;
  @Input() public game: FreeGame;
  @Input() public currentUserName: string;
  @Input() public currentGameId: string;
  public readonly MAX_COUNTER: number = 4;
  public winner: boolean;
  public cameraMovement: CameraMovementService = new CameraMovementService();
  public differencesCountPlayer1: number;
  public differencesCountPlayer2: number;
  public losingDialogRef: MatDialogRef<LosingDialogComponent>;
  public winningDialogRef: MatDialogRef<WinningDialogComponent>;
  public constructor(private freeGameService: FreeGameService,
                     private msgSocketService: MessageSocketService,
                     private sceneUpdater: SceneUpdateService,
                     private roomSocketService: RoomSocketService,
                     public dialog: MatDialog) {
    this.finish = new EventEmitter<void>();
    this.restart = new EventEmitter<FreeGame>();
    this.differencesCountPlayer2 = 0;
    this.winner = false;
    this.differencesCountPlayer1 = 0;
  }

  public ngOnInit(): void {
    this.roomSocketService.emitUpdateScene(this.game);
    this.msgSocketService.onFindDifferenceMultiPlayerGame();
    this.msgSocketService.onErrorIdentificationMultiPlayerGame();
    this.roomSocketService.onSecondCounterIncrement().subscribe((username: string) => {
      this.differencesCountPlayer2++;
      this.verifyWinner(this.differencesCountPlayer2, username);
    });
    this.roomSocketService.onFirstCounterIncrement().subscribe((username: string) => {
      this.differencesCountPlayer1++;
      this.verifyWinner(this.differencesCountPlayer1, username);
    });
    this.roomSocketService.onUpdateScenes().subscribe((game: FreeGame) => {
      this.modifiedScene.refreshScenes(game.modifiedScene);
      this.game = game;
    });
  }
  public updateScenes(modifiedObject: string): void {
    this.sceneUpdater.updateModifiedScene(this.game.modifiedScene, modifiedObject, this.game);
    this.roomSocketService.emitUpdateScene(this.game);
    this.msgSocketService.emitFindDifferenceMultiPlayerGame(this.currentUserName,  this.game.gameId as string);
    this.modifiedScene.initializeScenes();
    this.originalScene.initializeScenes();
    const audio: HTMLAudioElement = new Audio(successAudio);
    audio.play();

  }
  public displayError(error: boolean): void {
   this.msgSocketService.emitErrorIdentificationMultiPlayerGame(this.currentUserName, this.game.gameId as string);
  }
  private getPositionOfScore(id: string, userName: string, score: number): void {
    this.freeGameService.getPositionOfMultiGameRecord(id, userName, score).subscribe((position: { index: number }) => {
      if (position.index !== -1) {
        this.msgSocketService.EmitBestScoreFind(userName, position.index,
                                                this.game.name as string, "un contre un");
      }
    });
  }

  public reset(): void {
    this.winner = false;
    this.differencesCountPlayer1 = 0;
    this.differencesCountPlayer2 = 0;
  }
  public verifyWinner(differenceCount: number, username: string): void {
    if (differenceCount === this.MAX_COUNTER && username === this.currentUserName) {
      this.freeGameService.updateMultiGameRecord(this.currentGameId as string, this.currentUserName,
                                                 this.chrono.getScore()).subscribe((status) => {
                                                  if (status) {
             this.getPositionOfScore(this.currentGameId as string, this.currentUserName,
                                     this.chrono.getScore());
           }
         });
      this.winningDialogRef = this.dialog.open(WinningDialogComponent, {disableClose : true}).componentInstance.finish.subscribe(() => {
      this.dialog.closeAll();
      this.finish.emit();
        });
      this.reset();
    } else if (differenceCount === this.MAX_COUNTER) {
      this.losingDialogRef =  this.dialog.open(LosingDialogComponent, {disableClose : true});
      this.losingDialogRef.componentInstance.finish.subscribe(() => {
      this.finish.emit();
      this.dialog.closeAll();
      });
      this.losingDialogRef.componentInstance.restart.subscribe(() => {
        this.restart.emit(this.game);
        this.losingDialogRef.close();
       });
     }
  }

  @HostListener("document:keypress", ["$event"])
  public handleKeyboardEvent(event: KeyboardEvent): void {
      this.cameraMovement.moveCamera(event.key);
  }

  @HostListener("mouseup", ["$event"])
  public onMouseup(event: MouseEvent): void {

    if (event.button === MouseInputs.RIGHTCLICK) {
      this.cameraMovement.rightClick = false;
    }
  }
  @HostListener("mousemove", ["$event"])
  public onMousemove(event: MouseEvent): void {
    if (this.cameraMovement.rightClick) {
      this.cameraMovement.rotateCamera(new Point2D(event.movementX, event.movementY));
    }
  }

}
