import { HttpClient } from "@angular/common/http";
import { AfterViewInit, Component, ElementRef, EventEmitter, HostListener, Input, Output, ViewChild } from "@angular/core";
import * as THREE from "three";
import { SceneProcessingStatus as SPStatus} from "../../../../common/communication/errors";
import { MouseInputs } from "../../../../common/models/camera";

import { IScene } from "../../../../common/models/iScene";
import { CameraMovementService } from "../services/cameraMovement.service";
import { CheatModeService } from "../services/cheatMode.service";
import { ObjectDetectorService } from "../services/object-detector.service";
import { SceneParserService } from "../services/sceneParser.service";
interface MousePosition {
  x: number;
  y: number;
}
@Component({
  selector: "app-free-view-image",
  templateUrl: "./free-view-image.component.html",
  styleUrls: ["./free-view-image.component.css"],
})
export class FreeViewImageComponent implements AfterViewInit {
  @ViewChild("myCanvas") public canvas: ElementRef;
  @Input() public sceneData: IScene;
  @Input() public cameraMovement: CameraMovementService;
  @Input() public differencesId: string[];
  @Output() public modifiedObject: EventEmitter<string> = new EventEmitter<string>();
  @Output() public errorIdentification: EventEmitter<boolean> = new EventEmitter<boolean>();
  public scene: THREE.Scene;
  public camera: THREE.PerspectiveCamera;
  public renderer: THREE.WebGLRenderer;
  private sceneParser: SceneParserService;
  private readonly canvasWidth: number = 512;
  private readonly canvasHeight: number = 384;
  private detectObjectService: ObjectDetectorService;
  private cheatMode: CheatModeService;

  private readonly lightColor: number = 0xFFFFFF;
  private light: THREE.DirectionalLight;

  public constructor(private http: HttpClient) {
    this.light = new THREE.DirectionalLight(this.lightColor);
    this.light.castShadow = true;
    this.renderer = new THREE.WebGLRenderer({antialias: true});
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.BasicShadowMap;
  }

  public ngAfterViewInit(): void {
    this.initializeScenes();
    this.animate();
  }
  public initializeScenes(): void {
    this.refreshScenes(this.sceneData);
  }
  public refreshScenes(sceneData: IScene): void {
    this.sceneParser = new SceneParserService(sceneData);
    this.sceneParser.parse();
    this.scene = this.sceneParser.getParsedScene()[0];
    this.camera = this.cameraMovement.camera;
    this.renderer.setSize(this.canvasWidth, this.canvasHeight);
    this.cheatMode = new CheatModeService(sceneData, this.differencesId, this.lightColor);
    this.canvas.nativeElement.appendChild(this.renderer.domElement);
    this.renderer.render(this.scene, this.camera);
    this.detectObjectService = new ObjectDetectorService(this.http, this.sceneParser.meshesMap, this.differencesId);
    this.cameraMovement.cameraCollisionDetection.addSceneToDetect(this.scene);
    this.scene.add(this.light);
  }
  private animate(): void {
    window.requestAnimationFrame(() => this.animate());
    this.renderer.render(this.cheatMode.chooseMode(this.scene), this.camera);
  }
  @HostListener("document:contextmenu", ["$event"])
  public preventContextMenu(): boolean { return false; }

  @HostListener("document:keypress", ["$event"])
  public handleKeyboardEvent(event: KeyboardEvent): void {
      this.cheatMode.toggleCheatMode(event.key);
  }
  @HostListener("mousedown", ["$event"])
  public onMousedown(event: MouseEvent): void {
    if (event.button === MouseInputs.RIGHTCLICK) {
      this.cameraMovement.rightClick = true;
    }
    if (event.button === MouseInputs.LEFTCLICK) {
      const mousePosition: MousePosition = {x: event.offsetX, y: event.offsetY};
      this.detectObjectService.detect(this.cameraMovement.camera, mousePosition , this.scene, this.renderer).then(
        (resp: [SPStatus, string]) => {
          this.modifiedObject.emit(resp[1]);
      }).catch((resp) => {
        this.errorIdentification.emit(true);
      });
    }
  }
}
