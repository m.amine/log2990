import { DOCUMENT } from "@angular/common";
import { AfterViewChecked, Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material";
import { Point } from "../../../../common/models/point";
import { SimpleGame } from "../../../../common/models/simpleGame";
import { ChronometerComponent } from "../chronometer/chronometer.component";
import { LosingDialogComponent } from "../losing-dialog/losing-dialog.component";
import { DifferencesDetectorService } from "../services/differencesDetector.service";
import { MessageSocketService } from "../services/message.socket.service";
import { RoomSocketService } from "../services/room.socket.service";
import { SimpleGameService } from "../services/simpleGame.service";
import { SimpleViewImageComponent } from "../simple-view-image/simple-view-image.component";

import { WinningDialogComponent } from "../winning-dialog/winning-dialog.component";
const successAudio: string = "../../assets/youSmart.mp3";
const errorAudio: string = "../../assets/notSmart.wav";

@Component({
  selector: "app-simple-game-view-multi-player",
  templateUrl: "./simple-game-view-multi-player.component.html",
  styleUrls: ["./simple-game-view-multi-player.component.css"],
})
export class SimpleGameViewMultiPlayerComponent implements AfterViewChecked, OnInit {
  @Input() public currentGameId: string;
  @Input() public game: SimpleGame;
  @Input() public currentUserName: string;
  @Output() public finish: EventEmitter<void>;
  @Output() public restart: EventEmitter<SimpleGame>;
  @ViewChild("modifyImage") public modifyImageComponent: SimpleViewImageComponent;
  @ViewChild("chrono") public chrono: ChronometerComponent;
  public readonly MAX_COUNTER: number = 4;
  public winner: boolean;
  public differencesCount: number;
  public differencesCount2: number;
  public losingDialogRef: MatDialogRef<LosingDialogComponent>;
  public winningDialogRef: MatDialogRef<WinningDialogComponent>;
  private denyProcessing: boolean;

  public constructor(private simpleGameService: SimpleGameService,
                     private differencesDetectorService: DifferencesDetectorService,
                     private msgSocketService: MessageSocketService,
                     private roomSocketService: RoomSocketService,
                     @Inject(DOCUMENT) private document: Document,
                     public dialog: MatDialog) {
    this.finish = new EventEmitter<void>();
    this.restart = new EventEmitter<SimpleGame>();
    this.winner = false;
    this.differencesCount = 0;
    this.differencesCount2 = 0;

    this.denyProcessing = false;

  }

  public reset(): void {
    this.differencesDetectorService.reset();
    this.winner = false;
    this.differencesCount = 0;
    this.differencesCount2 = 0;
    this.denyProcessing = false;
  }

  public ngAfterViewChecked(): void {
    if (this.differencesDetectorService.modifyImageCanvas === undefined ||
        this.differencesDetectorService.modifyImageCanvas !== this.modifyImageComponent.canvasElement) {
      this.differencesDetectorService.game = this.game;
      this.differencesDetectorService.modifyImageCanvas = this.modifyImageComponent.canvasElement;
    }
  }

  public ngOnInit(): void {
    this.msgSocketService.onFindDifferenceMultiPlayerGame();
    this.msgSocketService.onErrorIdentificationMultiPlayerGame();
    this.roomSocketService.onSecondCounterIncrement().subscribe((username: string) => {
      this.differencesCount2++;
      this.verifyWinner(this.differencesCount2, username); });
    this.roomSocketService.onFirstCounterIncrement().subscribe((username: string) => {
      this.differencesCount++;
      this.verifyWinner(this.differencesCount, username); });
    this.roomSocketService.onUpdateImage().subscribe((points: [Point, Point]) => {
      this.differencesDetectorService.processDetection(points[0]);
    });
  }

  // tslint:disable-next-line:max-func-body-length
  public handleClick(points: [Point, Point]): void {
    if (this.denyProcessing) {
      return;
    }
    this.differencesDetectorService.processDetection(points[0])
      .then(() => {
        this.msgSocketService.emitFindDifferenceMultiPlayerGame(this.currentUserName, this.game.gameId as string);
        this.roomSocketService.emitUpdateImages(points , this.game.gameId as string);
        this.winner = this.differencesCount === this.MAX_COUNTER;
        this.winner = this.differencesCount2 === this.MAX_COUNTER;
        const audio: HTMLAudioElement = new Audio(successAudio);
        audio.play();
            })
      .catch(() => {
        this.updateOnFail(points[1]);
        this.msgSocketService.emitErrorIdentificationMultiPlayerGame(this.currentUserName, this.game.gameId as string);

      });
  }
  public verifyWinner(differenceCount: number, username: string): void {
    if (differenceCount === this.MAX_COUNTER && username === this.currentUserName) {
      this.simpleGameService.updateMultiGameRecord(this.currentGameId as string, this.currentUserName,
                                                   this.chrono.getScore()).subscribe((status) => {
           if (status) {
             this.getPositionOfScore(this.currentGameId as string, this.currentUserName,
                                     this.chrono.getScore());
           }
         });
      this.winningDialogRef = this.dialog.open(WinningDialogComponent, {disableClose : true}).componentInstance.finish.subscribe(() => {
      this.dialog.closeAll();
      this.finish.emit();
        });
      this.reset();
    } else if (differenceCount === this.MAX_COUNTER) {
      this.losingDialogRef =  this.dialog.open(LosingDialogComponent, {disableClose : true});
      this.losingDialogRef.componentInstance.finish.subscribe(() => {
      this.finish.emit();
      this.dialog.closeAll();
      });
      this.losingDialogRef.componentInstance.restart.subscribe(() => {
        this.restart.emit(this.game);
        this.losingDialogRef.close();
       });
      this.reset();
     }
    this.game.action =  "Créer";
  }
  private getPositionOfScore(id: string, userName: string, score: number): void {
    this.simpleGameService.getPositionOfMultiGameRecord(id, userName, score).subscribe((position: {index: number}) => {
      if (position.index !== -1) {
        this.msgSocketService.EmitBestScoreFind(userName, position.index,
                                                this.game.name as string, "un contre un"); }
      });
  }

  private updateOnFail(point: Point): void {
    this.setError(point);

    const oneSecond: number = 1000;
    setTimeout(this.unsetError.bind(this), oneSecond);
  }

  private setError(point: Point): void {
    const simpleGameContainer: HTMLElement = this.document.getElementById("simple-game-view-container") as HTMLElement;
    const simpleGameErrorContainer: HTMLElement = this.document.getElementById("simple-game-view-error") as HTMLElement;
    simpleGameContainer.classList.add("denied");
    simpleGameErrorContainer.style.display = "block";
    simpleGameErrorContainer.style.left = point.x.toString() + "px";
    simpleGameErrorContainer.style.top = point.y.toString() + "px";
    this.denyProcessing = true;
    const audio: HTMLAudioElement = new Audio(errorAudio);
    audio.play();
  }

  private unsetError(): void {
    const simpleGameContainer: HTMLElement = this.document.getElementById("simple-game-view-container") as HTMLElement;
    const simpleGameErrorContainer: HTMLElement = this.document.getElementById("simple-game-view-error") as HTMLElement;
    simpleGameContainer.classList.remove("denied");
    simpleGameErrorContainer.style.display = "none";
    this.denyProcessing = false;
  }

}
