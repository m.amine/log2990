import { Component, OnDestroy , OnInit} from "@angular/core";
import { MessageSocketService } from "../services/message.socket.service";

@Component({
  selector: "app-message",
  templateUrl: "./message.component.html",
  styleUrls: ["./message.component.css"],
})

export class MessageComponent implements OnInit , OnDestroy {
  public messages: string[];

  public ngOnInit(): void {
    this.messageSocketService.onNewUserConnected();
    this.messageSocketService.onUserDisconnect();
    this.messageSocketService.onBestScore();
  }
  public ngOnDestroy(): void {
    this.messageSocketService.clearAllMessages();

  }

  public constructor(private messageSocketService: MessageSocketService) {
    this.messages = this.messageSocketService.allServerMessages;
  }
}
