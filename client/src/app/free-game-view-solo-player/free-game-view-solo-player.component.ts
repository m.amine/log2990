import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material";
import { MouseInputs, Point2D } from "../../../../common/models/camera";
import { FreeGame } from "../../../../common/models/freeGame";
import { ChronometerComponent } from "../chronometer/chronometer.component";
import { FreeViewImageComponent } from "../free-view-image/free-view-image.component";
import { CameraMovementService } from "../services/cameraMovement.service";
import { FreeGameService } from "../services/freeGame.service";
import { MessageSocketService } from "../services/message.socket.service";
import { SceneUpdateService } from "../services/scene-update.service";
import { WinningDialogComponent } from "../winning-dialog/winning-dialog.component";

const successAudio: string = "../../assets/youSmart.mp3";

@Component({
  selector: "app-free-game-view-solo-player",
  templateUrl: "./free-game-view-solo-player.component.html",
  styleUrls: ["./free-game-view-solo-player.component.css"],
})

export class FreeGameViewSoloPlayerComponent implements OnInit {
  @ViewChild("background") public background: ElementRef;
  @ViewChild("chrono") public chrono: ChronometerComponent;
  @ViewChild("modifiedScene") public modifiedScene: FreeViewImageComponent;
  @ViewChild("originalScene") public originalScene: FreeViewImageComponent;
  @Output() public finish: EventEmitter<void>;
  @Input() public game: FreeGame;
  @Input() public currentUserName: string;
  public readonly MAX_COUNTER: number = 7;
  public winner: boolean;
  public textVictory: string;
  public cameraMovement: CameraMovementService = new CameraMovementService();
  public differencesCount: number;

  public constructor(private freeGameService: FreeGameService,
                     private msgSocketService: MessageSocketService,
                     private sceneUpdater: SceneUpdateService,
                     public dialog: MatDialog) {
    this.finish = new EventEmitter<void>();
    this.differencesCount = 0;
    this.winner = false;
    this.differencesCount = 0;
  }

  public ngOnInit(): void {
    this.msgSocketService.onFindDifferenceSoloGame();
    this.msgSocketService.onErrorIdentificationSoloGame();
  }
  public updateScenes(modifiedObject: string): void {
    this.sceneUpdater.updateModifiedScene(this.game.modifiedScene, modifiedObject, this.game);
    this.msgSocketService.emitFindDifferenceSoloGame(this.currentUserName);
    this.modifiedScene.initializeScenes();
    this.originalScene.initializeScenes();
    this.differencesCount++;
    this.winner = this.differencesCount === this.MAX_COUNTER;
    const audio: HTMLAudioElement = new Audio(successAudio);
    audio.play();
    if (this.winner) {
      this.dialog.open(WinningDialogComponent,  {disableClose : true}).componentInstance.finish.subscribe(() => {
        this.finish.emit();
        this.dialog.closeAll();
      });
      this.freeGameService.updateSoloGameRecord(this.game.gameId as string, this.currentUserName,
                                                this.chrono.getScore()).subscribe((status) => {
          if (status) {
            this.getPositionOfScore(this.game.gameId as string, this.currentUserName,
                                    this.chrono.getScore());
          }
        });
      this.reset();
    }
  }
  public displayError(error: boolean): void {
    this.msgSocketService.emitErrorIdentificationSoloGame(this.currentUserName);
  }
  private getPositionOfScore(id: string, userName: string, score: number): void {
    this.freeGameService.getPositionOfSoloGameRecord(id, userName, score).subscribe((position: { index: number }) => {
      if (position.index !== -1) {
        this.msgSocketService.EmitBestScoreFind(userName, position.index,
                                                this.game.name as string, "solo");
      }
    });
  }

  public reset(): void {
    this.winner = false;
    this.differencesCount = 0;
  }

  @HostListener("document:keypress", ["$event"])
  public handleKeyboardEvent(event: KeyboardEvent): void {
      this.cameraMovement.moveCamera(event.key);
  }

  @HostListener("mouseup", ["$event"])
  public onMouseup(event: MouseEvent): void {
    if (event.button === MouseInputs.RIGHTCLICK) {
      this.cameraMovement.rightClick = false;
    }
  }
  @HostListener("mousemove", ["$event"])
  public onMousemove(event: MouseEvent): void {
    if (this.cameraMovement.rightClick) {
      this.cameraMovement.rotateCamera(new Point2D(event.movementX, event.movementY));
    }
  }

}
