import { DOCUMENT } from "@angular/common";
import { AfterViewChecked, Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material";
import { Point } from "../../../../common/models/point";
import { SimpleGame } from "../../../../common/models/simpleGame";
import { ChronometerComponent } from "../chronometer/chronometer.component";
import { DifferencesDetectorService } from "../services/differencesDetector.service";
import { MessageSocketService } from "../services/message.socket.service";
import { SimpleGameService } from "../services/simpleGame.service";
import { SimpleViewImageComponent } from "../simple-view-image/simple-view-image.component";
import { WinningDialogComponent } from "../winning-dialog/winning-dialog.component";

const successAudio: string = "../../assets/youSmart.mp3";
const errorAudio: string = "../../assets/notSmart.wav";

@Component({
  selector: "app-simple-game-view-solo-player",
  templateUrl: "./simple-game-view-solo-player.component.html",
  styleUrls: ["./simple-game-view-solo-player.component.css"],
})

export class SimpleGameViewSoloPlayerComponent implements AfterViewChecked, OnInit {
  @Input() public game: SimpleGame;
  @Input() public currentUserName: string;
  @Output() public finish: EventEmitter<void>;
  @ViewChild("modifyImage") public modifyImageComponent: SimpleViewImageComponent;
  @ViewChild("chrono") public chrono: ChronometerComponent;
  public readonly MAX_COUNTER: number = 7;
  public winner: boolean;
  public textVictory: string;
  public differencesCount: number;
  private denyProcessing: boolean;
  public winningDialogRef: MatDialogRef<WinningDialogComponent>;
  public constructor(private simpleGameService: SimpleGameService,
                     private differencesDetectorService: DifferencesDetectorService,
                     private msgSocketService: MessageSocketService,
                     @Inject(DOCUMENT) private document: Document,
                     private dialog: MatDialog) {
    this.finish = new EventEmitter<void>();
    this.winner = false;
    this.differencesCount = 0;
    this.denyProcessing = false;

  }

  public reset(): void {
    this.differencesDetectorService.reset();
    this.winner = false;
    this.differencesCount = 0;
    this.denyProcessing = false;
  }

  public ngAfterViewChecked(): void {
    if (this.differencesDetectorService.modifyImageCanvas === undefined ||
        this.differencesDetectorService.modifyImageCanvas !== this.modifyImageComponent.canvasElement) {
      this.differencesDetectorService.game = this.game;
      this.differencesDetectorService.modifyImageCanvas = this.modifyImageComponent.canvasElement;
    }
  }

  public ngOnInit(): void {
    this.msgSocketService.onFindDifferenceSoloGame();
    this.msgSocketService.onErrorIdentificationSoloGame();

  }

  public handleClick(points: [Point, Point]): void {
    if (!this.denyProcessing) {
    this.differencesDetectorService.processDetection(points[0])
      .then(() => {
        this.msgSocketService.emitFindDifferenceSoloGame(this.currentUserName);
        this.differencesCount++;
        this.winner = this.differencesCount === this.MAX_COUNTER;
        const audio: HTMLAudioElement = new Audio(successAudio);
        audio.play();
        if (this.winner) {
          this.dialog.open(WinningDialogComponent, {disableClose : true}).componentInstance.finish.subscribe(() => {
            this.finish.emit();
            this.dialog.closeAll();
          });
          this.simpleGameService.updateSoloGameRecord(this.game.gameId as string, this.currentUserName,
                                                      this.chrono.getScore()).subscribe((status) => {
              if (status) {
                this.getPositionOfScore(this.game.gameId as string, this.currentUserName,
                                        this.chrono.getScore());
              }
            });
          this.reset();
        }
      })
      .catch(() => {
        this.updateOnFail(points[1]);
        this.msgSocketService.emitErrorIdentificationSoloGame(this.currentUserName);
      });
    }
  }

  private getPositionOfScore(id: string, userName: string, score: number): void {
    this.simpleGameService.getPositionOfSoloGameRecord(id, userName, score).subscribe((position: {index: number}) => {
      if (position.index !== -1) {
        this.msgSocketService.EmitBestScoreFind(userName, position.index,
                                                this.game.name as string, "solo"); }
      });
  }

  private updateOnFail(point: Point): void {
    this.setError(point);

    const oneSecond: number = 1000;
    setTimeout(this.unsetError.bind(this), oneSecond);
  }

  private setError(point: Point): void {
    const simpleGameContainer: HTMLElement = this.document.getElementById("simple-game-view-container") as HTMLElement;
    const simpleGameErrorContainer: HTMLElement = this.document.getElementById("simple-game-view-error") as HTMLElement;
    simpleGameContainer.classList.add("denied");
    simpleGameErrorContainer.style.display = "block";
    simpleGameErrorContainer.style.left = point.x.toString() + "px";
    simpleGameErrorContainer.style.top = point.y.toString() + "px";
    this.denyProcessing = true;
    const audio: HTMLAudioElement = new Audio(errorAudio);
    audio.play();
  }

  private unsetError(): void {
    const simpleGameContainer: HTMLElement = this.document.getElementById("simple-game-view-container") as HTMLElement;
    const simpleGameErrorContainer: HTMLElement = this.document.getElementById("simple-game-view-error") as HTMLElement;
    simpleGameContainer.classList.remove("denied");
    simpleGameErrorContainer.style.display = "none";
    this.denyProcessing = false;
  }

}
