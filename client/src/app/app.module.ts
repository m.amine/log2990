import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatDialogModule } from "@angular/material/dialog";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatSelectModule } from "@angular/material/select";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatToolbarModule } from "@angular/material/toolbar";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { DifferencesCounterComponent } from "./DifferencesCounter/differences-counter.component";
import { AdminViewComponent } from "./admin-view/admin-view.component";
import { FreeViewGameFormComponent } from "./admin-view/free-view-game-form/free-view-game-form.component";
import { GameCardAdminComponent } from "./admin-view/game-card-admin/game-card-admin.component";
import { SimpleViewGameFormComponent } from "./admin-view/simple-view-game-form/simple-view-game-form.component";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ChronometerComponent } from "./chronometer/chronometer.component";
import { FreeGameViewMultiPlayerComponent } from "./free-game-view-multi-player/free-game-view-multi-player.component";
import { FreeGameViewSoloPlayerComponent } from "./free-game-view-solo-player/free-game-view-solo-player.component";
import { FreeViewImageComponent } from "./free-view-image/free-view-image.component";
import { GamesCardListComponent } from "./games-card-list/games-card-list.component";
import { InitialViewComponent } from "./initial-view/initialView.component";
import { LosingDialogComponent } from "./losing-dialog/losing-dialog.component";
import { MessageComponent } from "./message/message.component";
import { ModalComponent } from "./modal/modal.component";
import { LoginService } from "./services/Login.service";
import { FreeGameService } from "./services/freeGame.service";
import { MessageSocketService } from "./services/message.socket.service";
import { ObjectDetectorService } from "./services/object-detector.service";
import { RoomSocketService } from "./services/room.socket.service";
import { SceneUpdateService } from "./services/scene-update.service";
import { SceneParserService } from "./services/sceneParser.service";
import { SimpleGameService } from "./services/simpleGame.service";
import { SocketService } from "./services/socket.service";
import { SimpleGameViewMultiPlayerComponent } from "./simple-game-view-multi-player/simple-game-view-multi-player.component";
import { SimpleGameViewSoloPlayerComponent } from "./simple-game-view-solo-player/simple-game-view-solo-player.component";
import { SimpleViewImageComponent } from "./simple-view-image/simple-view-image.component";
import { WaitingViewComponent } from "./waiting-view/waiting-view.component";
import { WinningDialogComponent } from "./winning-dialog/winning-dialog.component";

@NgModule({
  declarations: [
    AppComponent,
    InitialViewComponent,
    AdminViewComponent,
    GameCardAdminComponent,
    SimpleViewGameFormComponent,
    GamesCardListComponent,
    FreeViewGameFormComponent,
    DifferencesCounterComponent,
    SimpleGameViewSoloPlayerComponent,
    MessageComponent,
    ModalComponent,
    SimpleViewImageComponent,
    ChronometerComponent,
    FreeGameViewSoloPlayerComponent,
    FreeViewImageComponent,
    FreeGameViewMultiPlayerComponent,
    SimpleGameViewMultiPlayerComponent,
    WaitingViewComponent,
    LosingDialogComponent,
    WinningDialogComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    AppRoutingModule,
    RouterModule.forRoot([
      {
        path: "admin", pathMatch: "full",
        component: AdminViewComponent,
      },
      {
        path: "",
        component: InitialViewComponent,
      },
    ]),
    MatCardModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    MatSelectModule,
  ],
  bootstrap: [AppComponent],
  entryComponents: [SimpleViewGameFormComponent, FreeViewGameFormComponent, LosingDialogComponent, WinningDialogComponent],
  providers: [
    LoginService,
    FreeGameService,
    SimpleGameService,
    SocketService,
    SceneParserService,
    ObjectDetectorService,
    MessageSocketService,
    SceneUpdateService,
    RoomSocketService,
  ],
})
export class AppModule { }
