import { Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.css"],
})

export class ModalComponent   {
  @Output() public finish: EventEmitter<void>;
  @Input() public messageVictory: string;
  public constructor() {
    this.finish = new EventEmitter<void>();

  }
  public restartGame(): void {
    this.finish.emit();
  }

}
