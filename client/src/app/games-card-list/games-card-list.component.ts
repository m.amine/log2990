import { Component, Input, OnInit } from "@angular/core";
import { Room } from "../../../../common/communication/room";
import { FreeGame } from "../../../../common/models/freeGame";
import { gameController } from "../../../../common/models/gameController";
import { SimpleGame } from "../../../../common/models/simpleGame";
import { FreeGameService } from "../services/freeGame.service";
import { RoomSocketService } from "../services/room.socket.service";
import { SimpleGameService } from "../services/simpleGame.service";

@Component({
  selector: "app-games-card-list",
  templateUrl: "./games-card-list.component.html",
  styleUrls: ["./games-card-list.component.css"],
})

export class GamesCardListComponent implements OnInit {
  @Input() public currentUserName: string;
  public gameController: gameController;
  public readonly title: String = "Liste des jeux";
  public simpleGames: SimpleGame[] = [];
  public freeGames: FreeGame[] = [];
  public currentFreeGame: FreeGame;
  public currentSimpleGame: SimpleGame;
  private currentGameId: string;

  public constructor(
    private simpleGameService: SimpleGameService,
    private freeGameService: FreeGameService,
    private roomSocketService: RoomSocketService) {
    this.gameController = {
      simpleViewSoloGame: false,
      freeViewSoloGame: false,
      simpleViewMultiGame: false,
      freeViewMultiGame: false,
      waitingView: false,
      cancelFreeViewMultiGame: false,
      cancelSimpleViewMultiGame: false,
    };
  }

  public ngOnInit(): void {
    this.initSimpleGames();
    this.initFreeGames();

    this.roomSocketService.onStartSimpleGame().subscribe(() => {
      this.gameController.waitingView = false;
      this.gameController.simpleViewMultiGame = true;

    });

    this.roomSocketService.onStartFreeGame().subscribe(() => {
      this.gameController.waitingView = false;
      this.gameController.freeViewMultiGame = true;
    });

    this.roomSocketService.onDeleteGame().subscribe((id: string) => {
      if (this.gameController.waitingView && id === this.currentGameId) {
        alert("Someone deleted this game, kicking you out sorry!");
        this.gameController.waitingView = false;
        this.ngOnInit();
      }
    });
  }

  public initSimpleGames(): void {
    this.simpleGames = [];
    this.simpleGameService.getAll().subscribe((games: SimpleGame[]) => {
      this.simpleGames = games;
      this.roomSocketService.getSimpleGamesAction(games);
      this.roomSocketService.onGetSimpleGamesAction().subscribe((gamesInitialized: SimpleGame[]) => {
        this.simpleGames = gamesInitialized;
      });
    });
  }

  public initFreeGames(): void {
    this.freeGames = [];
    this.freeGameService.getAll().subscribe((games: FreeGame[]) => {
      this.freeGames = games;
      this.roomSocketService.getFreeGamesAction(games);
      this.roomSocketService.onGetFreeGamesAction().subscribe((gamesInitialized: FreeGame[]) => {
        this.freeGames = gamesInitialized;
      });
    });
  }

  public playSoloGameSimpleView(game: SimpleGame): void {
    this.currentSimpleGame = game;
    this.gameController.simpleViewSoloGame = true;
  }

  public playSoloGameFreeView(game: FreeGame): void {
    this.currentFreeGame = game;
    this.gameController.freeViewSoloGame = true;
  }

  private createSimpleViewRoom(): Room {
    const room: Room = new Room(this.currentSimpleGame.name as string, this.currentSimpleGame.gameId as string);
    room.addUser(this.currentUserName);

    return room;
  }

  public playMultiGameSimpleView(game: SimpleGame): void {
    this.currentSimpleGame = game;
    this.currentGameId = game.gameId.toString();
    if (this.currentSimpleGame.action === "Créer") {
      this.gameController.cancelSimpleViewMultiGame = true;
      game.action = "Joindre";
      this.roomSocketService.emitCreateRoom(this.createSimpleViewRoom());
      this.roomSocketService.createRoom().subscribe((roomId: string) => {
        this.currentSimpleGame.gameId = roomId;
      });
      this.gameController.waitingView = true;
    } else {
      this.roomSocketService.emitJoinForSimpleRoom(this.createSimpleViewRoom());
      this.roomSocketService.onJoined().subscribe((roomId: string) => {
        this.currentSimpleGame.gameId = roomId;
        this.currentSimpleGame.action = "Créer";
      });
    }
  }

  private createFreeViewRoom(): Room {
    const room: Room = new Room(this.currentFreeGame.name as string, this.currentFreeGame.gameId as string);
    room.addUser(this.currentUserName);

    return room;
  }

  public playMultiGameFreeView(game: FreeGame): void {
    this.currentFreeGame = game;
    this.currentGameId = game.gameId.toString();
    if (this.currentFreeGame.action === "Créer") {
      this.gameController.cancelFreeViewMultiGame = true;
      game.action = "Joindre";
      this.roomSocketService.emitCreateRoom(this.createFreeViewRoom());
      this.roomSocketService.createRoom().subscribe((roomId: string) => {
        this.currentFreeGame.gameId = roomId;
      });
      this.gameController.waitingView = true;
    } else {
      this.roomSocketService.emitJoinForFreeRoom(this.createFreeViewRoom());
      this.roomSocketService.onJoined().subscribe((roomId: string) => {
        this.currentFreeGame.gameId = roomId;
        this.currentFreeGame.action = "Créer";
      });
    }
  }

  public replayNewFreeViewGame(game: FreeGame): void {
    this.gameController.freeViewMultiGame = false;
    this.playMultiGameFreeView(game);
  }
  public replayNewSimpleViewGame(game: SimpleGame): void {
    this.gameController.simpleViewMultiGame = false;
    this.playMultiGameSimpleView(game);
  }

  public finish(): void {
    this.ngOnInit();
    this.gameController.simpleViewSoloGame = false;
    this.gameController.freeViewSoloGame = false;
    this.gameController.freeViewMultiGame = false;
    this.gameController.simpleViewMultiGame = false;
    this.roomSocketService.leaveRoom(this.createFreeViewRoom());
    this.roomSocketService.leaveRoom(this.createSimpleViewRoom());
    this.gameController.waitingView = false;
  }

  public cancelSimpleViewMultiGame(): void {
    this.roomSocketService.leaveRoom(this.createSimpleViewRoom());
    this.currentSimpleGame.action = "Créer";
    this.gameController.simpleViewMultiGame = false;
    this.gameController.waitingView = false;
  }

  public cancelFreeViewMultiGame(): void {
    this.roomSocketService.leaveRoom(this.createFreeViewRoom());
    this.currentFreeGame.action = "Créer";
    this.gameController.freeViewMultiGame = false;
    this.gameController.waitingView = false;
  }

}
