import { Component, OnDestroy } from "@angular/core";
import { DifferencesCounterService } from "../services/differencesCounter.service";

@Component({
  selector: "app-differences-counter",
  templateUrl: "./differences-counter.component.html",
  styleUrls: ["./differences-counter.component.css"],
})

export class DifferencesCounterComponent implements  OnDestroy {

  public constructor(private counter: DifferencesCounterService) {
        this.counter = new DifferencesCounterService();
   }

  public increment(): number {
    return this.counter.incrementCounter();
  }

  public displayCounter(): number {
    return this.counter.displayTotalCounter();
  }

  public ngOnDestroy(): void {
    this.counter.resetCounter();
  }

}
