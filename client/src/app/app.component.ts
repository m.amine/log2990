import { Component, OnInit } from "@angular/core";
import { MessageSocketService } from "./services/message.socket.service";
import { RoomSocketService } from "./services/room.socket.service";
import { SocketService } from "./services/socket.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {

  public constructor(private socketService: SocketService,
                     private messageSocketService: MessageSocketService,
                     private roomSocketService: RoomSocketService) {
                      }

  public ngOnInit(): void {
    this.socketService.initSocket();
    this.messageSocketService.initSocket(this.socketService.socket);
    this.roomSocketService.initSocket(this.socketService.socket);
  }
}
