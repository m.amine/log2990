import { Injectable } from "@angular/core";
import { Chronometer } from "../../../../common/chronometer";

@Injectable({
  providedIn: "root",
})
export class  ChronometerService {
  private chronModel: Chronometer;

  public constructor() {
    this.chronModel = new Chronometer(0, 0);
  }

  public startChronometer(): void {
    this.chronModel.start();
  }

  public stopChronometer(): void {
    this.chronModel.stop();
  }

  public resetChronometer(): void {
    this.chronModel.reset();
  }

  public getTimeInSeconds(): number {
    return this.chronModel.totalSecondes;
  }

  public displayChronometer(): string {
    return this.chronModel.display();
  }
}
