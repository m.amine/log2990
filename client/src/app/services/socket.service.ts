import { Injectable } from "@angular/core";
import * as socketIo from "socket.io-client";

@Injectable({
  providedIn: "root",
})
export class SocketService {
  private readonly SERVER_URL: string = "http://localhost:3000";
  public socket: SocketIOClient.Socket;

  public initSocket(socket?: SocketIOClient.Socket): void {
    this.socket = socket ? socket : socketIo(this.SERVER_URL);
  }

  public emit<T>(event: string, argument: T): void {
    this.socket.emit(event, argument);
  }
}
