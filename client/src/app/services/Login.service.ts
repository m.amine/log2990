import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { of, Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { UserLoginStatus } from "../../../../common/communication/errors";

@Injectable()
export class LoginService {

  public loggedUserName: string;
  public loginStatus: UserLoginStatus;
  private readonly BASE_URL: string = "http://localhost:3000/";
  public constructor(private http: HttpClient) {
    this.loginStatus = UserLoginStatus.NOT_LOGGED_IN;
  }

  public async login(name: string): Promise<UserLoginStatus> {
    return new Promise<UserLoginStatus>((
      resolve: (value?: UserLoginStatus | PromiseLike<UserLoginStatus>) => void,
      reject: (reason?: UserLoginStatus) => void) => {

        this.initLogin(name).subscribe((res: {status: UserLoginStatus}) => {
          this.loginStatus = res["status"];
          if (this.loginStatus === UserLoginStatus.S_OK) {
            this.loggedUserName = name;
            resolve(this.loginStatus);
          } else {
            reject(this.loginStatus);
          }
        });

    });
  }

  private initLogin(name: string): Observable<{status: UserLoginStatus}> {
      return this.http.get<{status: UserLoginStatus}>(this.BASE_URL + "login/" + name).pipe(
        catchError(this.handleError<{status: UserLoginStatus}>("login")));
  }

  private handleError<T>(request: string, result?: T): (error: Error) => Observable<T> {
    return (error: Error): Observable<T> => {
      return of(result as T);
    };
  }
}
