import { Injectable } from "@angular/core";
import * as THREE from "three";
import { KeyBoardInputs } from "../../../../common/models/camera";

@Injectable()
export class CameraCollisionDetectionService {

    public camera: THREE.PerspectiveCamera;
    public virtualScene: THREE.Scene;
    public rayCaster: THREE.Raycaster;
    public readonly cameraRadius: number = 5;

    public constructor(camera: THREE.PerspectiveCamera) {
        this.camera = camera;
        this.virtualScene = new THREE.Scene();
        this.rayCaster = new THREE.Raycaster();
    }

    public detect(deltaX: number, deltaZ: number, input: string): void {
        this.rayCaster.set(this.camera.position, this.getDirection(input));
        const intersections: THREE.Intersection[] = this.rayCaster.intersectObjects(this.virtualScene.children);
        if (intersections.length > 0 && intersections[0].distance < this.cameraRadius) {
            this.camera.translateX(-deltaX);
            this.camera.translateZ(-deltaZ);
        }
    }

    private getDirection(input: string): THREE.Vector3 {
        const halfDivider: number = 2;
        const orientationX: number = Math.cos(this.camera.rotation.z + Math.PI / halfDivider);
        const orientationY: number = Math.sin(this.camera.rotation.z + Math.PI / halfDivider);
        let direction: THREE.Vector3 = new THREE.Vector3(0, 0, 0);

        switch (input) {
            case KeyBoardInputs.FORWARD:
                direction = this.camera.getWorldDirection(new THREE.Vector3);
                break;
            case KeyBoardInputs.BACKWARDS:
                direction = this.camera.getWorldDirection(new THREE.Vector3).multiplyScalar(-1);
                break;
            case KeyBoardInputs.LEFT:
                direction = this.camera.getWorldDirection(new THREE.Vector3).cross(new THREE.Vector3(-orientationX, -orientationY, 0));
                break;
            case KeyBoardInputs.RIGHT:
                direction = this.camera.getWorldDirection(new THREE.Vector3).cross(new THREE.Vector3(orientationX, orientationY, 0));
                break;
            default:
        }

        return direction;
    }

    public addSceneToDetect(scene: THREE.Scene): void {
        scene.children.forEach((object: THREE.Mesh) => {
            const newObject: THREE.Mesh = object.clone();
            this.virtualScene.add(newObject);
        });
    }
}
