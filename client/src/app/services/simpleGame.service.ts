import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { of, Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { SimpleGame } from "../../../../common/models/simpleGame";

@Injectable({
  providedIn: "root",
})

export class SimpleGameService {

  private readonly BASE_URL: string = "http://localhost:3000/simpleGame/";

  public constructor(private http: HttpClient) { }

  public getAll(): Observable<SimpleGame[]> {
    return this.http.get<SimpleGame[]>(this.BASE_URL).pipe(
      catchError(this.handleError<SimpleGame[]>("getAll")));
  }

  public async deleteGame(id: string): Promise<void> {
    return this.http.delete<void>(this.BASE_URL + id).pipe(
      catchError(this.handleError<void>("delete"))).toPromise();
  }

  public async resetGame(id: string): Promise<void> {
    return this.http.patch<void>(this.BASE_URL  + id, {}).pipe(
      catchError(this.handleError<void>("reset"))).toPromise();
  }

  public updateSoloGameRecord(id: string, name: string, score: number): Observable<boolean> {
    const header: HttpHeaders = new HttpHeaders({ "content-type": "application/json" });

    return this.http.put<boolean>( this.BASE_URL + "updateSoloRecord/" + id ,
                                   JSON.stringify({ id : id , userName: name, newScore: score }), { headers: header }).pipe(
      catchError(this.handleError<boolean>("updateSimpleViewSoloGameRecord")));

  }

  public getPositionOfSoloGameRecord(id: string, name: string, score: number): Observable<Object> {
    const header: HttpHeaders = new HttpHeaders({ "content-type": "application/json" });

    return this.http.put<Object>( this.BASE_URL + "getPositionOfSoloRecord/" + id ,
                                  JSON.stringify({ id : id , userName: name, newScore: score }), { headers: header }).pipe(
      catchError(this.handleError<Object>("getPositionOfRecord")));

  }

  public updateMultiGameRecord(id: string, name: string, score: number): Observable<boolean> {
    const header: HttpHeaders = new HttpHeaders({ "content-type": "application/json" });

    return this.http.put<boolean>( this.BASE_URL + "updateMultiRecord/" + id ,
                                   JSON.stringify({ id : id , userName: name, newScore: score }), { headers: header }).pipe(
      catchError(this.handleError<boolean>("updateSimpleViewMultiGameRecord")));

  }

  public getPositionOfMultiGameRecord(id: string, name: string, score: number): Observable<Object> {
    const header: HttpHeaders = new HttpHeaders({ "content-type": "application/json" });

    return this.http.put<Object>( this.BASE_URL + "getPositionOfMultiRecord/" + id ,
                                  JSON.stringify({ id : id , userName: name, newScore: score }), { headers: header }).pipe(
      catchError(this.handleError<Object>("getPositionOfRecord")));

  }

  private handleError<T>(request: string, result?: T): (error: Error) => Observable<T> {
    return (error: Error): Observable<T> => {
      return of(result as T);
    };
  }

}
