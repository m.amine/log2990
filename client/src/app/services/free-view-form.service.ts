import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { of, Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { SceneGenerationStatus as SGStatus } from "../../../../common/communication/errors";
import { ServerResponse } from "../../../../common/communication/serverResponse";
import { FreeGame } from "../../../../common/models/freeGame";
import { IScene } from "../../../../common/models/iScene";
import { Pair } from "../../../../common/models/types";
import { SceneParserService } from "./sceneParser.service";

interface FreeGameData {
  name: string;
  oScene: IScene;
  mScene: IScene;
  differencesId: string[];
  thumbnail: Uint8Array;
}

export interface SceneGeneratorData {
  scenes: Pair<IScene>;
  differencesIds: string[];
}

export type SGResponse = ServerResponse<SGStatus, SceneGeneratorData>;

@Injectable({
  providedIn: "root",
})
export class FreeViewFormService {
  private readonly BASE_URL: string = "http://localhost:3000";
  public constructor(private http: HttpClient) { }

  // tslint:disable-next-line:no-any
  public async submitFreeViewForm( value: any): Promise<void> {

    return new Promise<void>((
      resolve: (value?: void | PromiseLike<void>) => void,
      reject: (reason?: void) => void) => {
        this.requestScenes(value).subscribe(async (response: SGResponse) => {
          if (response.status === SGStatus.S_OK) {
            await this.createGameData(response.response as SceneGeneratorData, value.gameName)
            .then((gameData: FreeGameData) => {
              this.requestFreeGameCreation(gameData).subscribe(() => resolve());
            });
          } else {
            reject();
          }
        });
      });
  }

  private requestFreeGameCreation(freeGameData: FreeGameData): Observable<FreeGame> {

    return this.http.post<FreeGame>(this.BASE_URL + "/freeGame/create", freeGameData).pipe(
      catchError(this.handleError<FreeGame>("freeGameCreation")));
  }

  // tslint:disable-next-line:no-any
  private requestScenes(value: any): Observable<SGResponse> {
    return this.http.post<SGResponse>(this.BASE_URL + "/sceneGenerator/generate", value).pipe(
      catchError(this.handleError<SGResponse>("generateScenes")));
  }

  private async createGameData(data: SceneGeneratorData, gameName: string): Promise<FreeGameData> {
    return new Promise<FreeGameData>(async (
      resolve: (value?: FreeGameData | PromiseLike<FreeGameData>) => void,
      reject: (reason?: void) => void) => {
        const sceneParser: SceneParserService = new SceneParserService(data.scenes.first);
        sceneParser.parse();
        const thumbnail: Uint8Array = sceneParser.getThumbnail();

        resolve({
          name: gameName,
          oScene: data.scenes.first,
          mScene: data.scenes.second,
          differencesId: data.differencesIds,
          thumbnail: thumbnail,
        });
  });
  }

  private handleError<T>(request: string, result?: T): (error: Error) => Observable<T> {
    return (error: Error): Observable<T> => {
      return of(result as T);
    };
  }

}
