import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import * as THREE from "three";
import { SceneProcessingStatus as SPStatus } from "../../../../common/communication/errors";
import { VerifyDifferenceService } from "./verify-difference.service";

interface MousePosition {
  x: number;
  y: number;
}
@Injectable({
  providedIn: "root",
})
export class ObjectDetectorService {
  private readonly SCALE: number = 2;
  public point3D: THREE.Vector3;
  private verifyDifferenceService: VerifyDifferenceService;
  private meshMap: [string, number][];
  private _differencesId: string[];
  private intersection: THREE.Intersection[];

  public constructor(private http: HttpClient, newMeshMap: [string, number][] , diffrencesId: string[]) {
    this.point3D = new THREE.Vector3();
    this.verifyDifferenceService = new VerifyDifferenceService(this.http);
    this.meshMap = newMeshMap;
    this._differencesId = diffrencesId;
  }

  public async detect(camera: THREE.Camera, mouse: MousePosition, scene: THREE.Scene , renderer: THREE.WebGLRenderer):
  Promise<[SPStatus, string]> {
    this.raycasterMesh(camera, mouse, scene, renderer);

    return new Promise<[SPStatus, string]>((
      resolve: (value?: [SPStatus, string] | PromiseLike<[SPStatus, string]>) => void,
      reject: (reason?: SPStatus) => void) => {
          (this.intersection.length > 0) ?
            this.verifyDifferenceService.isDifference(this.intersection[0].object.id, this.meshMap , this._differencesId).then(
              (resp: [SPStatus, string]) => {
                resolve(resp);
            }).catch((resp: SPStatus) => {
              reject(resp);
            }) :
           reject(SPStatus.E_NOT_DIFFERENCE); });
  }
  public raycasterMesh(camera: THREE.Camera , mouse: MousePosition, scene: THREE.Scene, renderer: THREE.WebGLRenderer ): number {
    this.point3D.x = (mouse.x / renderer.domElement.clientWidth) * this.SCALE - 1;
    this.point3D.y = -(mouse.y / renderer.domElement.clientHeight) * this.SCALE + 1;
    this.point3D.z = 0;
    const raycaster: THREE.Raycaster = new THREE.Raycaster();
    raycaster.setFromCamera(this.point3D, camera);
    this.intersection = raycaster.intersectObjects(scene.children);

    return (this.intersection.length);
  }
}
