import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { of, Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { SceneProcessingStatus, SceneProcessingStatus as SPStatus} from "../../../../common/communication/errors";
import { DifferencesMappedService } from "./differencesMapped";

export type DifferenceRequest = [string, string[]];
export type VerificationResponse = [SPStatus, string];
@Injectable({
  providedIn: "root",
})
export class VerifyDifferenceService {
  private readonly BASE_URL: string = "http://localhost:3000/differenceValidator3D/detectDifferences3D";
  public constructor(private http: HttpClient) {
  }
  public async isDifference(objectId: number, meshMap: [string, number][], differencesId: string[]):
  Promise<[SPStatus, string]> {
    const sceneObjectId: string = DifferencesMappedService.meshIdToSceneObjectId(meshMap, objectId);

    return new Promise<VerificationResponse>((
      resolve: (value?: VerificationResponse | PromiseLike<VerificationResponse>) => void,
      reject: (reason?: SceneProcessingStatus) => void) => {
        this.initDifferenceRequest([sceneObjectId , differencesId]).subscribe((response: [SPStatus]) => {
          (response[0] === SPStatus.S_OK) ? resolve([response[0], sceneObjectId]) : reject(response[0]);
         });
        });
  }

  private initDifferenceRequest(differenceRequest: DifferenceRequest): Observable<[SPStatus]> {
    return this.http.post<[SPStatus]>(this.BASE_URL, differenceRequest).pipe(
      catchError(this.handleError<[SPStatus]>("differenceRequest")));
  }

  private handleError<T>(request: string, result?: T): (error: Error) => Observable<T> {
    return (error: Error): Observable<T> => {
      return of(result as T);
    };
  }
}
