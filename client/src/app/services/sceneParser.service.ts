import { Injectable } from "@angular/core";
import * as THREE from "three";
import { GeometricObjectType, IGeometricObject } from "../../../../common/models/iGeometricObject";
import { IPoint3D } from "../../../../common/models/iPoint3D";
import { Scene } from "../utils/scene";

export type ParsedScene = [THREE.Scene, THREE.PerspectiveCamera];
export type MeshMap = [string, number];

@Injectable()
export class SceneParserService {
    private scene: THREE.Scene;
    private camera: THREE.PerspectiveCamera;
    private loadedScene: Scene;
    public meshesMap: MeshMap[];
    private readonly lines: number = 20;

    public constructor(loadedScene: Scene, w: number = window.innerWidth, h: number = window.innerHeight) {
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(loadedScene.backgroundColor);
        const fov: number = 70;
        const far: number = 10000;
        const xPosition: number = 0;
        const yPosition: number = 500;
        const zPosition: number = 0;
        this.camera = new THREE.PerspectiveCamera(fov, w / h, 1, far);
        this.camera.position.x = xPosition;
        this.camera.position.y = yPosition;
        this.camera.position.z = zPosition;

        this.loadedScene = loadedScene;
        this.meshesMap = [];
    }

    public parse(): void {
        this.loadedScene.decoration.forEach((decoration: IGeometricObject) => this.generateGeometry(decoration, false));
        this.loadedScene.objects.forEach((object: IGeometricObject) => this.generateGeometry(object, true));

        this.scene.add(this.parseBounds());
    }

    public getParsedScene(): ParsedScene {
        return [this.scene, this.camera];
    }

    public getThumbnail(): Uint8Array {
        const renderer: THREE.WebGLRenderer = new THREE.WebGLRenderer({ preserveDrawingBuffer: true });
        const width: number = 640;
        const height: number = 480;
        renderer.setSize(width, height);
        renderer.render(this.scene, this.camera);
        const dataUrl: string = renderer.domElement.toDataURL();

        return this.toArrayBuffer(dataUrl);
    }

    private toArrayBuffer(dataUrl: string): Uint8Array {
        const BASE64_MARKER: string = ";base64,";
        const base64Index: number = dataUrl.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        const base64: string = dataUrl.substring(base64Index);
        const raw: string = window.atob(base64);
        const rawLength: number = raw.length;
        const array: Uint8Array = new Uint8Array(new ArrayBuffer(rawLength));

        for (let i: number = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }

        return array;
    }

    private generateGeometry(object: IGeometricObject, meshesMapFlag: boolean): void {
        const mesh: THREE.Mesh = this.selectGeometry(object);

        if (meshesMapFlag) {
            this.meshesMap.push([object.id, mesh.id]);
        }

        this.addMesh(mesh, object.position, object.rotation);
    }

    private selectGeometry(object: IGeometricObject): THREE.Mesh {
        switch (object.type) {
            case GeometricObjectType.BOX:
                return this.addBox(object.w, object.h, object.d, object.hexColor, object.castShadow);

            case GeometricObjectType.SPHERE:
                return this.addSphere(object.r, object.hexColor, object.castShadow);

            case GeometricObjectType.CYLINDER:
                return this.addCylinder(object.r, object.h, object.hexColor, object.castShadow);

            case GeometricObjectType.CONE:
                return this.addCone(object.r, object.h, object.hexColor, object.castShadow);

            case GeometricObjectType.PYRAMID:
                return this.addPyramid(object.r, object.hexColor, object.castShadow);

            case GeometricObjectType.PLANE:
                return this.addPlane(object.w, object.h, object.d, object.hexColor, object.castShadow);

            default:
                throw new Error("Invalid Geometry Type");
        }
    }

    private addBox(w: number, h: number, d: number, hexColor: number, castShadow: boolean): THREE.Mesh {
        const geometry: THREE.BoxGeometry = new THREE.BoxGeometry(w, h, d);
        const material: THREE.Material = (castShadow) ? new THREE.MeshPhongMaterial({ color: hexColor, wireframe: false }) :
            new THREE.MeshBasicMaterial({ color: hexColor, wireframe: false });
        geometry.center();

        return new THREE.Mesh(geometry, material);
    }

    private addSphere(r: number, hexColor: number, castShadow: boolean): THREE.Mesh {
        const geometry: THREE.SphereGeometry = new THREE.SphereGeometry(r, this.lines, this.lines);
        const material: THREE.Material = (castShadow) ? new THREE.MeshPhongMaterial({ color: hexColor, wireframe: false }) :
            new THREE.MeshBasicMaterial({ color: hexColor, wireframe: false });
        geometry.center();

        return new THREE.Mesh(geometry, material);
    }

    private addCylinder(r: number, h: number, hexColor: number, castShadow: boolean): THREE.Mesh {
        const geometry: THREE.CylinderGeometry = new THREE.CylinderGeometry(r, r, h, this.lines, this.lines);
        const material: THREE.Material = (castShadow) ? new THREE.MeshPhongMaterial({ color: hexColor, wireframe: false }) :
            new THREE.MeshBasicMaterial({ color: hexColor, wireframe: false });
        geometry.center();

        return new THREE.Mesh(geometry, material);
    }

    private addCone(r: number, h: number, hexColor: number, castShadow: boolean): THREE.Mesh {
        const geometry: THREE.ConeGeometry = new THREE.ConeGeometry(r, h, this.lines, this.lines);
        const material: THREE.Material = (castShadow) ? new THREE.MeshPhongMaterial({ color: hexColor, wireframe: false }) :
            new THREE.MeshBasicMaterial({ color: hexColor, wireframe: false });
        geometry.center();

        return new THREE.Mesh(geometry, material);
    }

    private addPyramid(r: number, hexColor: number, castShadow: boolean): THREE.Mesh {
        const geometry: THREE.TetrahedronGeometry = new THREE.TetrahedronGeometry(r);
        const material: THREE.Material = (castShadow) ? new THREE.MeshPhongMaterial({ color: hexColor, wireframe: false }) :
            new THREE.MeshBasicMaterial({ color: hexColor, wireframe: false });
        geometry.center();

        return new THREE.Mesh(geometry, material);
    }

    private addPlane(w: number, h: number, d: number, hexColor: number, castShadow: boolean): THREE.Mesh {
        return this.addBox(w, d, h, hexColor, castShadow);
    }

    private addMesh(mesh: THREE.Mesh, position: IPoint3D, rotation: IPoint3D): void {
        mesh.position.set(position.x, position.y, position.z);
        mesh.rotation.set(rotation.x, rotation.y, rotation.z);
        this.scene.add(mesh);
    }

    private parseBounds(): THREE.Mesh {
        const bounds: IPoint3D = this.loadedScene.boundingBoxDimension.dimension;
        const boundsGeometry: THREE.BoxBufferGeometry = new THREE.BoxBufferGeometry(
            bounds.x, bounds.y, bounds.z, this.lines, this.lines, this.lines);
        const boundsMaterial: THREE.MeshBasicMaterial = new THREE.MeshBasicMaterial({color: 0x000000, wireframe: true});
        boundsMaterial.side = THREE.DoubleSide;

        const mesh: THREE.Mesh = new THREE.Mesh(boundsGeometry, boundsMaterial);
        const position: IPoint3D = this.loadedScene.boundingBoxDimension.position;
        mesh.position.set(position.x, position.y, position.z);

        return mesh;
    }
}
