import { Injectable } from "@angular/core";
import { FreeGame } from "../../../../common/models/freeGame";
import { IGeometricObject } from "../../../../common/models/iGeometricObject";
import { IScene } from "../../../../common/models/iScene";

@Injectable({
  providedIn: "root",
})
export class SceneUpdateService {
  public updateModifiedScene(modifiedScene: IScene, modifiedObject: string, game: FreeGame): IScene {
    const spliceIdx: number = game.differencesId.findIndex((id: string) => id === modifiedObject);
    if (spliceIdx !== -1) {
      game.differencesId.splice(spliceIdx, 1);
    }
    if (this.originalSceneContains(game.originalScene, modifiedObject)) {
      game.modifiedScene.objects.forEach((object: IGeometricObject, index: number) => {
        if (object.id === modifiedObject) {
          modifiedScene.objects.splice(index, 1);
        }
      });
    } else {
      game.originalScene.objects.forEach((object: IGeometricObject, index: number) => {
        if (object.id === modifiedObject) {
          const newObject: IGeometricObject = game.originalScene.objects[index];
          modifiedScene.objects.push(newObject);
        }
      });
    }

    return modifiedScene;
  }
  private originalSceneContains(originalScene: IScene, modifiedObject: string ): boolean {
    return originalScene.objects.find((object: IGeometricObject) => object.id === modifiedObject) === undefined;
  }
}
