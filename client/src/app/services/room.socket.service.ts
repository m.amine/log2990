import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Subscriber } from "rxjs/internal/Subscriber";
import { Room } from "../../../../common/communication/room";
import { FreeGame } from "../../../../common/models/freeGame";
import { Point } from "../../../../common/models/point";
import { SimpleGame } from "../../../../common/models/simpleGame";
import { SocketService } from "./socket.service";

@Injectable({
  providedIn: "root",
})

export class RoomSocketService extends SocketService {
  public constructor() {
    super();
  }

  public onGetSimpleGamesAction(): Observable<SimpleGame[]> {
    return new Observable<SimpleGame[]>((observer: Subscriber<SimpleGame[]>) => {
      this.socket.on("getSimpleGamesAction", (games: SimpleGame[]) => {
        observer.next(games);
      });
    });
  }

  public onGetFreeGamesAction(): Observable<FreeGame[]> {
    return new Observable<FreeGame[]>((observer: Subscriber<FreeGame[]>) => {
      this.socket.on("getFreeGamesAction", (games: FreeGame[]) => {
        observer.next(games);
      });
    });
  }

  public onStartSimpleGame(): Observable<void> {
    return new Observable<void>((observer: Subscriber<void>) => {
      this.socket.on("startSimpleGame", () => {
        observer.next();
      });
    });
  }

  public onStartFreeGame(): Observable<void> {

    return new Observable<void>((observer: Subscriber<void>) => {
    this.socket.on("startFreeGame", () => {
        observer.next();
      });
    });
  }

  public onJoined(): Observable<string> {
    return new Observable<string>((observer: Subscriber<string>) => {
  this.socket.on("joined", (roomId: string) => {
        observer.next(roomId);
      });
    });
  }

  public onSecondCounterIncrement(): Observable<string> {
    return new Observable<string>((observer: Subscriber<string>) => {
      this.socket.on("incrementSecondCounter", (username: string) => {
        observer.next(username);
      });
    });
  }

  public onFirstCounterIncrement(): Observable<string> {
    return new Observable<string>((observer: Subscriber<string>) => {
      this.socket.on("incrementFirstCounter", (username: string) => {
        observer.next(username);
      });
    });
  }

  public onUpdateImage(): Observable<[Point, Point]> {
    return new Observable<[Point, Point]>((observer: Subscriber<[Point, Point]>) => {
      this.socket.on("findPoints", ( point: [Point, Point]) => {
        observer.next(point);
      });
    });
  }
  public onUpdateScenes(): Observable<FreeGame> {
    return new Observable<FreeGame>((observer: Subscriber<FreeGame>) => {
      this.socket.on("updateScene", ( game: FreeGame) => {
        observer.next(game);
      });
    });
  }
  public onDeleteGame(): Observable<string> {
    return new Observable<string>((observer: Subscriber<string>) => {
      this.socket.on("deleteGame", (id: string) => {
        observer.next(id);
      });
    });
  }

  public getSimpleGamesAction(games: SimpleGame[]): void {
    this.socket.emit("getSimpleGamesAction", games);
  }

  public getFreeGamesAction(games: FreeGame[]): void {
    this.socket.emit("getFreeGamesAction", games);
  }

  public emitJoinForFreeRoom(room: Room): void {
    this.socket.emit("joinFreeRoom", room);
  }
  public emitJoinForSimpleRoom(room: Room): void {
    this.socket.emit("joinSimpleRoom", room);
  }

  public emitCreateRoom(room: Room): void {
    this.socket.emit("createRoom", room);
  }

  public leaveRoom(room: Room): void {
    this.socket.emit("leaveRoom", room);
  }
  public emitUpdateScene(game: FreeGame): void {
    this.socket.emit("updateScene", game, game.gameId as string);
  }
  public emitUpdateImages(point: [Point, Point], gameId: string): void {
    this.socket.emit("updateImage", point , gameId);
  }
  public createRoom(): Observable<string> {

    return new Observable<string>((observer: Subscriber<string>) => {
      this.socket.on("createdRoom", (roomId: string) => {
        observer.next(roomId);
      });
    });
  }
}
