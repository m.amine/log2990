import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { ImageProcessingStatus as IPStatus } from "../../../../common/communication/errors";

@Injectable({
  providedIn: "root",
})
export class SimpleViewFormService {

 public constructor(private http: HttpClient) { }
 private readonly BASE_URL: string = "http://localhost:3000";
 public response: {status: IPStatus, imgs: string[]};
 public imageStatus: boolean[];
 private readonly IMAGE_STATUS_LENGTH: number = 4;
 // tslint:disable-next-line:no-any
 public async submitSimpleForm(value: any): Promise<IPStatus | string[]> {
  return new Promise<IPStatus | string[]>((
    resolve: (value?: string[] | PromiseLike<string[]>) => void,
    reject: (reason?: IPStatus) => void) => {
      this.http.post(this.BASE_URL + "/imageProcessing/generateDifferences", value).subscribe(
        (res: {status: IPStatus, imgs: string[]}) => {
          if (res["status"] === IPStatus.S_OK) {
            resolve(res["imgs"]);
          } else {
            reject(res["status"]);
          }
          this.response = res;
        });
    });
}
 public responseHandler(): boolean[] {
  this.imageStatus = new Array(this.IMAGE_STATUS_LENGTH).fill(false);
  switch (this.response.status) {
    case IPStatus.E_INVALID_DIFFERENCES_COUNT:
       this.imageStatus[IPStatus.E_INVALID_DIFFERENCES_COUNT] = true;
       break;
    case IPStatus.E_INVALID_IMAGE_SIZE:
      this.imageStatus[IPStatus.E_INVALID_IMAGE_SIZE] = true;
      break;
    case IPStatus.E_INVALID_IMAGE_TYPE:
      this.imageStatus[IPStatus.E_INVALID_IMAGE_TYPE] = true;
      break;
    default:
      break;
  }

  return this.imageStatus;
 }
}
