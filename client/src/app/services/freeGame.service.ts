import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { of, Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { FreeGame } from "../../../../common/models/freeGame";

@Injectable({
  providedIn: "root",
})

export class FreeGameService {

  private readonly BASE_URL: string = "http://localhost:3000/freeGame/";

  public constructor(private http: HttpClient) { }

  public getAll(): Observable<FreeGame[]> {
    return this.http.get<FreeGame[]>(this.BASE_URL + "getAll").pipe(
      catchError(this.handleError<FreeGame[]>("getAll")));
  }

  public async deleteGame(id: string): Promise<void> {
    return this.http.delete<void>(this.BASE_URL + id).pipe(
      catchError(this.handleError<void>("delete"))).toPromise();
  }

  public async resetGame(id: string): Promise<void> {
    return this.http.patch<void>(this.BASE_URL  + id, {}).pipe(
      catchError(this.handleError<void>("reset"))).toPromise();
  }

  public updateSoloGameRecord(id: string, name: string, score: number): Observable<boolean> {
    const header: HttpHeaders = new HttpHeaders({ "content-type": "application/json" });

    return this.http.put<boolean>(this.BASE_URL + "updateSoloRecord/" + id,
                                  JSON.stringify({ id: id, userName: name, newScore: score }), { headers: header }).pipe(
        catchError(this.handleError<boolean>("updateFreeViewSoloGameRecord")));

  }

  public getPositionOfSoloGameRecord(id: string, name: string, score: number): Observable<object> {
    const header: HttpHeaders = new HttpHeaders({ "content-type": "application/json" });

    return this.http.put<object>(this.BASE_URL + "getPositionOfSoloRecord/" + id,
                                 JSON.stringify({ id: id, userName: name, newScore: score }), { headers: header }).pipe(
        catchError(this.handleError<object>("getPositionOfRecord")));

  }

  public updateMultiGameRecord(id: string, name: string, score: number): Observable<boolean> {
    const header: HttpHeaders = new HttpHeaders({ "content-type": "application/json" });

    return this.http.put<boolean>(this.BASE_URL + "updateMultiRecord/" + id,
                                  JSON.stringify({ id: id, userName: name, newScore: score }), { headers: header }).pipe(
        catchError(this.handleError<boolean>("updateFreeViewMultiGameRecord")));

  }

  public getPositionOfMultiGameRecord(id: string, name: string, score: number): Observable<object> {
    const header: HttpHeaders = new HttpHeaders({ "content-type": "application/json" });

    return this.http.put<object>(this.BASE_URL + "getPositionOfMultiRecord/" + id,
                                 JSON.stringify({ id: id, userName: name, newScore: score }), { headers: header }).pipe(
        catchError(this.handleError<object>("getPositionOfRecord")));

  }

  public getFreeGameWithId(id: string): Observable<FreeGame> {
    return this.http.get<FreeGame>(this.BASE_URL + id).pipe(
      catchError(this.handleError<FreeGame>("getGameWithId")));
  }

  private handleError<T>(request: string, result?: T): (error: Error) => Observable<T> {
    return (error: Error): Observable<T> => {
      return of(result as T);
    };
  }

}
