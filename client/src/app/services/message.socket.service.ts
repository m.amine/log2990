import { Injectable } from "@angular/core";
import * as dateformat from "dateformat";
import { SocketService } from "./socket.service";

@Injectable()
export class MessageSocketService extends SocketService {
  public messages: string[];

  public constructor() {
    super();
    this.messages = new Array<string>();
  }

  public get allServerMessages(): string[] {
    return this.messages;
  }
  public clearAllMessages(): void {
    this.messages = new Array<string>();

  }
  public onNewUserConnected(): void {
    this.socket.on("loggedIn", (name: string) => {
      this.messages.push(dateformat(new Date(), "isoTime") + " - " + name + " vient de se connecter. ");
    });
  }
  public onUserDisconnect(): void {
    this.socket.on("logout", (name: string) => {
      this.messages.push(dateformat(new Date(), "isoTime") + " - " + name + " vient de se déconnecter. ");
    });
  }

  public emitFindDifferenceSoloGame(playerName: string): void {
    this.socket.emit("find difference in solo game", playerName);
  }
  public onFindDifferenceSoloGame(): void {
    this.socket.on("find difference in solo game", () => {
      this.messages.push(dateformat(new Date(), "isoTime") + " - " + "Différence trouvée.");
    });
  }
  public emitErrorIdentificationSoloGame(playerName: string): void {
    this.socket.emit("error identification in solo game", playerName);
  }
  public onErrorIdentificationSoloGame(): void {
    this.socket.on("error identification in solo game", () => {
      this.messages.push(dateformat(new Date(), "isoTime") + " - " + "Erreur.");
    });
  }
  public emitFindDifferenceMultiPlayerGame(playerName: string, gameId: string): void {
    this.socket.emit("find difference in multiPlayer game", playerName, gameId);
  }

  public onFindDifferenceMultiPlayerGame(): void {
    this.socket.on("find difference in multiPlayer game", (playerName: string) => {
      this.messages.push(dateformat(new Date(), "isoTime") + " - " + " Différence trouvée par " + playerName + ".");
    });
  }
  public emitErrorIdentificationMultiPlayerGame(playerName: string, gameId: string): void {
    this.socket.emit("error identification in multiPlayer game", playerName, gameId);
  }

  public onErrorIdentificationMultiPlayerGame(): void {
    this.socket.on("error identification in multiPlayer game", (playerName: string) => {
      this.messages.push(dateformat(new Date(), "isoTime") + " - " + " Erreur par " + playerName + ".");
    });
  }

  public EmitBestScoreFind(playerName: string, position: number,
                           gameName: string, playerPosition: string): void {
    this.socket.emit("found best score", playerName, this.gettingPlayerPosition(position), gameName, playerPosition);
  }

  public onBestScore(): void {
    this.socket.on("best score", (playerName: string, position: string,
                                  gameName: string, totalOfPlayers: string) => {
      this.messages.push(dateformat(new Date(), "isoTime") + " - " + playerName + " obtient la " + position +
        " place dans les meilleurs temps du jeu " + gameName + " en " + totalOfPlayers + ".");
    });
  }

  private gettingPlayerPosition(position: number): string {
    return position === 0 ? "Première" : position === 1 ? "Deuxième" : "Troisième";
  }
}
