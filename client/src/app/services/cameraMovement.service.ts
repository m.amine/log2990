import { Injectable } from "@angular/core";
import * as THREE from "three";
import { KeyBoardInputs, Point2D } from "../../../../common/models/camera";
import { CameraCollisionDetectionService } from "./cameraCollisionDetection.service";

@Injectable()
export class CameraMovementService {
    public readonly angle: number = 0.01;
    public readonly step: number = 5;
    public camera: THREE.PerspectiveCamera;
    public rightClick: boolean;
    public deltaMouseXY: Point2D;
    public cameraSetted: boolean;
    public cameraCollisionDetection: CameraCollisionDetectionService;

    public constructor() {
        this.cameraSetted = false;
        const fov: number = 75;
        const far: number = 10000;
        const xPosition: number = 0;
        const yPosition: number = 100;
        const zPosition: number = 0;
        this.camera = new THREE.PerspectiveCamera(fov, window.innerWidth / window.innerHeight, 1, far);

        this.camera.position.x = xPosition;
        this.camera.position.y = yPosition;
        this.camera.position.z = zPosition;
        this.rightClick = false;
        this.deltaMouseXY = new Point2D(0, 0);

        this.cameraCollisionDetection = new CameraCollisionDetectionService(this.camera);
    }

    public setCamera(camera: THREE.PerspectiveCamera): void {
        if (!this.cameraSetted) {
            this.camera = camera;
            this.cameraSetted = true;
        }
    }

    public moveCamera(input: string): void {
        let deltaX: number = 0;
        let deltaZ: number = 0;

        switch (input) {
            case KeyBoardInputs.FORWARD:
                deltaZ = -this.step;
                break;
            case KeyBoardInputs.BACKWARDS:
                deltaZ = this.step;
                break;
            case KeyBoardInputs.LEFT:
                deltaX = -this.step;
                break;
            case KeyBoardInputs.RIGHT:
                deltaX = this.step;
                break;
            default:
        }
        this.camera.translateX(deltaX);
        this.camera.translateZ(deltaZ);
        this.cameraCollisionDetection.detect(deltaX, deltaZ, input);
    }

    public rotateCamera(delta: Point2D): void {
        this.camera.rotateX(-delta.y * this.angle);
        this.camera.rotateY(-delta.x * this.angle);
    }
}
