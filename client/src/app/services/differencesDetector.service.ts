import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { of, Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { ImageProcessingStatus as IPStatus } from "../../../../common/communication/errors";
import { Pixel } from "../../../../common/models/pixel";
import { Point } from "../../../../common/models/point";
import { SimpleGame } from "../../../../common/models/simpleGame";
import { PixelMap } from "../../../../common/models/types";

@Injectable({
  providedIn: "root",
})

export class DifferencesDetectorService {

  private readonly BASE_URL: string = "http://localhost:3000/imageProcessing/detectDifference";
  public modifyImageCanvas: HTMLCanvasElement;
  public game: SimpleGame;
  private invalidPoints: Point[];

  public constructor(private http: HttpClient) {
    this.invalidPoints = [];
   }

  public reset(): void {
    this.invalidPoints = [];
   }

  public async processDetection(point: Point): Promise<IPStatus> {
    return new Promise<IPStatus>((
      resolve: (value?: IPStatus | PromiseLike<IPStatus>) => void,
      reject: (reason?: IPStatus) => void) => {

        this.initDetectorRequest(point).subscribe((response: [IPStatus, PixelMap]) => {
          if (response[0] === IPStatus.S_OK) {
            const pointIndex: number = this.invalidPoints.findIndex((currentPoint: Point) => {
              return (point.x === currentPoint.x && point.y === currentPoint.y);
            });

            if (pointIndex !== -1) {
              reject(IPStatus.E_NOT_FOUND);
            }

            this.processImage(response[1]);
            resolve(response[0]);

          } else {
            reject(response[0]);
          }
        });
    });
  }

  private initDetectorRequest(point: Point): Observable<[IPStatus, PixelMap]>  {
    const params: {point: Point, game: SimpleGame} = {point: point, game: this.game};

    return this.http.post<[IPStatus, PixelMap]>(this.BASE_URL, params).pipe(
      catchError(this.handleError<[IPStatus, PixelMap]>("detectorRequest")));
  }

  // tslint:disable-next-line:max-func-body-length
  private processImage(pixelMap: PixelMap): void {
    const context: (CanvasRenderingContext2D | null) = this.modifyImageCanvas.getContext("2d");
    if (context != null) {
      const imageData: ImageData = context.getImageData(0, 0, this.modifyImageCanvas.width, this.modifyImageCanvas.height);
      const pixels: Uint8ClampedArray = imageData.data;
      const pixelOffset: number = 4;
      const blueOffset: number = 2;

      pixelMap.forEach((pixel: {position: Point, value: Pixel}) => {
        const x: number = pixel.position.x;
        const y: number = pixel.position.y;
        const index: number = ((y * imageData.width) + x) * pixelOffset;
        pixels[index] = pixel.value.r;
        pixels[index + 1] = pixel.value.g;
        pixels[index + blueOffset] = pixel.value.b;
        this.invalidPoints.push(pixel.position);
      });

      context.putImageData(imageData, 0, 0);
    }
  }

  private handleError<T>(request: string, result?: T): (error: Error) => Observable<T> {
    return (error: Error): Observable<T> => {
      return of(result as T);
    };
  }

}
