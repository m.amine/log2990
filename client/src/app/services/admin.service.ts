import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { of, Observable } from "rxjs";
import { catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})

export class AdminService {

  private readonly BASE_URL: string = "http://localhost:3000";

  public constructor(private http: HttpClient) { }

  public create(body: string): Observable<boolean> {
      return this.http.post<boolean>(this.BASE_URL + "/imageProcessing/generateDifferences", body ).pipe(
        catchError(this.handleError<boolean>("post")));
  }

  private handleError<T>(request: string, result?: T): (error: Error) => Observable<T> {
    return (error: Error): Observable<T> => {
      return of(result as T);
    };
  }

}
