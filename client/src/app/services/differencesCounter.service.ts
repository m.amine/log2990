import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class DifferencesCounterService {
  private count: number;

  public constructor() {
    this.count = 0;
  }

  public incrementCounter(): number {
    return this.count++;
  }

  public resetCounter(): void {
    this.count = 0;
  }

  public displayTotalCounter(): number {
    return this.count;
  }
}
