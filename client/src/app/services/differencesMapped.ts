import { Injectable } from "@angular/core";
import { MeshMap } from "./sceneParser.service";

@Injectable()
export class DifferencesMappedService {
    public static sceneObjetIdToMeshId(meshesMap: MeshMap[], sceneObjectId: string): number {
        let i: number;
        for (i = 0; i < meshesMap.length; i++) {
            if (meshesMap[i][0] === sceneObjectId) {
                return meshesMap[i][1];
            }
        }
        throw new Error("Invalid sceneObjectId");
    }
    public static meshIdToSceneObjectId(meshesMap: MeshMap[], meshId: number): string {
        let i: number;
        for (i = 0; i < meshesMap.length; i++) {
            if (meshesMap[i][1] === meshId) {
                return meshesMap[i][0];
            }
        }
        throw new Error("Invalid meshId");
    }
}
