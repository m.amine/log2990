import { Injectable } from "@angular/core";
import * as THREE from "three";
import { KeyBoardInputs } from "../../../../common/models/camera";
import { IGeometricObject } from "../../../../common/models/iGeometricObject";
import { IScene, ObjectColor } from "../../../../common/models/iScene";
import { SceneParserService } from "./sceneParser.service";

@Injectable()
export class CheatModeService {
    public toggle: boolean;
    public cheatedSceneData: IScene;
    public readonly frameRate: number = 36;
    private blink: number;
    private cheatedScene: THREE.Scene;
    private readonly halfFactor: number = 2;
    private light: THREE.DirectionalLight;

    public constructor(scene: IScene, differencesId: string[], lightColor: number) {
        this.toggle = false;
        this.blink = 0;
        this.light = new THREE.DirectionalLight(lightColor);
        this.light.castShadow = true;
        this.initCheatedScene(scene, differencesId);
    }

    public chooseMode(scene: THREE.Scene): THREE.Scene {
        return this.toggle ? this.cheatMode(scene) : scene;
    }

    private cheatMode(scene: THREE.Scene): THREE.Scene {
        this.blink++;

        return this.isOriginalScene() ? scene : this.cheatedScene;
    }

    private isOriginalScene(): boolean {
        return this.blink % this.frameRate > this.frameRate / this.halfFactor;
    }

    private async initCheatedScene(scene: IScene, differencesId: string[]): Promise<void> {
        this.cheatedSceneData = JSON.parse(JSON.stringify(scene));
        differencesId.forEach((diffId: string) => {
            this.cheatedSceneData.objects.forEach((object: IGeometricObject, index: number) => {
                if (object.id === diffId) {
                    this.cheatedSceneData.objects[index].hexColor = ObjectColor.RED;
                    this.cheatedSceneData.objects[index].castShadow = false;
                }
            });
        });

        const cheatedSceneParser: SceneParserService = new SceneParserService(this.cheatedSceneData);
        cheatedSceneParser.parse();
        this.cheatedScene = cheatedSceneParser.getParsedScene()[0];
        this.cheatedScene.add(this.light);
    }

    public toggleCheatMode(input: string): void {
        if (input === KeyBoardInputs.CHEAT) {
            this.toggle = !this.toggle;
            this.blink = 0;
        }
    }
}
