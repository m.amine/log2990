import { Component, OnDestroy, OnInit} from "@angular/core";
import {ChronometerService} from "../services/chronometer.service";

@Component({
  selector: "app-chronometer",
  templateUrl: "./chronometer.component.html",
  styleUrls: ["./chronometer.component.css"],
})
export class ChronometerComponent implements OnInit, OnDestroy {

  public constructor(private service: ChronometerService) {
    this.service = new ChronometerService();
   }

  public ngOnInit(): void {
    this.service.startChronometer();
  }

  public ngOnDestroy(): void {
    this.service.stopChronometer();
  }

  public getScore(): number {
    return this.service.getTimeInSeconds();
  }

}
