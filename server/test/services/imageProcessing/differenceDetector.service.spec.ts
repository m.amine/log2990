import { assert } from "chai";
import * as fs from "fs";
import { ImageProcessingStatus as IPStatus } from "../../../../common/communication/errors";
import { Point } from "../../../../common/models/point";
import { SimpleGame } from "../../../../common/models/simpleGame";
import { PixelMap } from "../../../../common/models/types";
import { DifferenceDetector } from "../../../app/services/imageProcessing/differenceDetector.service";

const originalImagePath: string = "./test/res/imageProcessing/validTest.bmp";
const differencesImagePath: string = "./test/res/imageProcessing/expectedTest.bmp";
const oPath: string = "./res/images/differenceDetectorTestOriginal.bmp";
const dPath: string = "./res/images/differenceDetectorTestDifferences.bmp";
const baseUrl: string = "http://localhost:3000/image/";
const game: SimpleGame = {
  gameId: "id",
  name: "name",
  originalImage: "originalImage",
  modifiedImage: "modifiedImage",
  differencesImage: "differencesImage",
  soloTime: [],
  vsTime: [],
  action: "",
};

// tslint:disable:no-magic-numbers
const validPoints: [Point, number][] = [
    [new Point(239, 47), 241],
    [new Point(91, 168), 277],
    [new Point(102, 284), 450],
    [new Point(244, 370), 414],
    [new Point(343, 198), 237],
    [new Point(552, 250), 353],
    [new Point(430, 388), 371],
];

const invalidPoints: Point[] = [
  new Point(-1, -1),
  new Point(1000, 1000),
  new Point(218, 233),
  new Point(494, 105),
  new Point(65, 413),
  new Point(90, 81),
  new Point(330, 428),
];
// tslint:enable:no-magic-numbers

describe("Difference Detector", () => {
  before(() => {
    fs.writeFileSync(oPath, fs.readFileSync(originalImagePath));
    fs.writeFileSync(dPath, fs.readFileSync(differencesImagePath));
    game.originalImage = baseUrl + "differenceDetectorTestOriginal.bmp";
    game.differencesImage = baseUrl + "differenceDetectorTestDifferences.bmp";
  });

  it("should find the differences if given the right position", (done: MochaDone) => {
    validPoints.forEach((value: [Point, number]) => {
        const differenceDetector: DifferenceDetector = new DifferenceDetector(value[0], game);
        const response: [IPStatus, PixelMap] = differenceDetector.detect();
        assert.strictEqual(response[0], IPStatus.S_OK);
        assert.strictEqual(response[1].length, value[1]);
    });
    done();
  });

  it("should return an error if the position given is invalid", (done: MochaDone) => {
    invalidPoints.forEach((point: Point) => {
        const differenceDetector: DifferenceDetector = new DifferenceDetector(point, game);
        const response: [IPStatus, PixelMap] = differenceDetector.detect();
        assert.strictEqual(response[0], IPStatus.E_NOT_FOUND);
        assert.strictEqual(response[1].length, 0);
    });
    done();
  });

  after(() => {
    fs.unlinkSync(oPath);
    fs.unlinkSync(dPath);
  });

});
