import { assert } from "chai";
import * as fs from "fs";
import { ImageProcessingStatus as IPStatus } from "../../../../common/communication/errors";
import { DifferencesGenerator } from "../../../app/services/imageProcessing/generateDifferences.service";

const serviceGeneratorDifferences: DifferencesGenerator = new DifferencesGenerator();
const validImagePath: string = "./test/res/imageProcessing/validTest.bmp";
const validModImagePath: string = "./test/res/imageProcessing/validModTest.bmp";
const expectedImagePath: string = "./test/res/imageProcessing/expectedTest.bmp";
const invalidSizeImagePath: string = "./test/res/imageProcessing/invalidSizeTest.bmp";
const invalidTypeImagePath: string = "./test/res/imageProcessing/invalidTypeTest.png";
const invalidDiffCountImagePath: string = "./test/res/imageProcessing/invalidDiffCountTest.bmp";

describe("GenerateDifferences", () => {

    it("should generate for a valid image a 7 differences image", (done: MochaDone) => {
        const oBuf: Buffer = fs.readFileSync(validImagePath);
        const mBuf: Buffer = fs.readFileSync(validModImagePath);
        const eBuf: Buffer = fs.readFileSync(expectedImagePath);
        const oValidBuffer: ArrayBuffer = oBuf.buffer.slice(oBuf.byteOffset, oBuf.byteOffset + oBuf.byteLength);
        const mValidBuffer: ArrayBuffer = mBuf.buffer.slice(mBuf.byteOffset, mBuf.byteOffset + mBuf.byteLength);

        const result: { status: IPStatus, imgs: string[] } = serviceGeneratorDifferences.start(oValidBuffer, mValidBuffer);

        assert.strictEqual(result["status"], IPStatus.S_OK);

        const dIdx: number = 2;
        const expectedLength: number = 3;
        const dFilename: string = result["imgs"][dIdx].split("/").pop() as string;
        const dPath: string = "./res/images/" + dFilename;
        const dBuf: Buffer = fs.readFileSync(dPath);

        // Clean up
        const oFilename: string = result["imgs"][0].split("/").pop() as string;
        const mFilename: string = result["imgs"][1].split("/").pop() as string;
        fs.unlinkSync("./res/images/" + oFilename);
        fs.unlinkSync("./res/images/" + mFilename);
        fs.unlinkSync(dPath);

        // Assertions
        assert.strictEqual(result["imgs"].length, expectedLength);
        assert.strictEqual(dBuf.toString("base64"), eBuf.toString("base64"));
        done();
    });

    it("should generate a bad status if the image hasn't a valid size", (done: MochaDone) => {
        const oBuf: Buffer = fs.readFileSync(validImagePath);
        const mBuf: Buffer = fs.readFileSync(invalidSizeImagePath);
        const oValidBuffer: ArrayBuffer = oBuf.buffer.slice(oBuf.byteOffset, oBuf.byteOffset + oBuf.byteLength);
        const mValidBuffer: ArrayBuffer = mBuf.buffer.slice(mBuf.byteOffset, mBuf.byteOffset + mBuf.byteLength);

        const result: { status: IPStatus, imgs: string[] } = serviceGeneratorDifferences.start(oValidBuffer, mValidBuffer);

        assert.strictEqual(result["status"], IPStatus.E_INVALID_IMAGE_SIZE);
        done();
    });

    it("should generate a bad status if the image hasn't a valid type", (done: MochaDone) => {
        const oBuf: Buffer = fs.readFileSync(validImagePath);
        const mBuf: Buffer = fs.readFileSync(invalidTypeImagePath);
        const oValidBuffer: ArrayBuffer = oBuf.buffer.slice(oBuf.byteOffset, oBuf.byteOffset + oBuf.byteLength);
        const mValidBuffer: ArrayBuffer = mBuf.buffer.slice(mBuf.byteOffset, mBuf.byteOffset + mBuf.byteLength);

        const result: { status: IPStatus, imgs: string[] } = serviceGeneratorDifferences.start(oValidBuffer, mValidBuffer);

        assert.strictEqual(result["status"], IPStatus.E_INVALID_IMAGE_TYPE);
        done();
    });

    it("should generate a bad status if the image hasn't the right amount of differences", (done: MochaDone) => {
        const oBuf: Buffer = fs.readFileSync(validImagePath);
        const mBuf: Buffer = fs.readFileSync(invalidDiffCountImagePath);
        const oValidBuffer: ArrayBuffer = oBuf.buffer.slice(oBuf.byteOffset, oBuf.byteOffset + oBuf.byteLength);
        const mValidBuffer: ArrayBuffer = mBuf.buffer.slice(mBuf.byteOffset, mBuf.byteOffset + mBuf.byteLength);

        const result: { status: IPStatus, imgs: string[] } = serviceGeneratorDifferences.start(oValidBuffer, mValidBuffer);

        assert.strictEqual(result["status"], IPStatus.E_INVALID_DIFFERENCES_COUNT);
        done();
    });

});
