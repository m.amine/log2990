import { assert } from "chai";
import * as fs from "fs";
import { ImageProcessingStatus as IPStatus } from "../../../../common/communication/errors";
import { ServerResponse } from "../../../../common/communication/serverResponse";
import { Point } from "../../../../common/models/point";
import { DifferencesValidator } from "../../../app/services/imageProcessing/differencesValidator.service";
import { Bitmap } from "../../../app/utils/bitmap";

type Response = ServerResponse<IPStatus, Point[]>;
const validImagePath: string = "./test/res/imageProcessing/validTest.bmp";
const validModImagePath: string = "./test/res/imageProcessing/validModTest.bmp";
const invalidDiffCountImagePath: string = "./test/res/imageProcessing/invalidDiffCountTest.bmp";

describe("Differences Validator", () => {

    it("should return S_OK if the images have 7 differences", (done: MochaDone) => {
        const differencesValidator: DifferencesValidator = new DifferencesValidator();
        const oBuf: Buffer = fs.readFileSync(validImagePath);
        const mBuf: Buffer = fs.readFileSync(validModImagePath);
        const oValidBuffer: ArrayBuffer = oBuf.buffer.slice(oBuf.byteOffset, oBuf.byteOffset + oBuf.byteLength);
        const mValidBuffer: ArrayBuffer = mBuf.buffer.slice(mBuf.byteOffset, mBuf.byteOffset + mBuf.byteLength);

        const oValidBmp: Bitmap = new Bitmap();
        oValidBmp.read(oValidBuffer);
        const mValidBmp: Bitmap = new Bitmap();
        mValidBmp.read(mValidBuffer);

        const result: Response = differencesValidator.validate(oValidBmp, mValidBmp);
        assert.strictEqual(result.status, IPStatus.S_OK);
        done();
    });

    it("should return a bad status if there's not 7 differences", (done: MochaDone) => {
        const differencesValidator: DifferencesValidator = new DifferencesValidator();
        const oBuf: Buffer = fs.readFileSync(validImagePath);
        const mBuf: Buffer = fs.readFileSync(invalidDiffCountImagePath);
        const oValidBuffer: ArrayBuffer = oBuf.buffer.slice(oBuf.byteOffset, oBuf.byteOffset + oBuf.byteLength);
        const mValidBuffer: ArrayBuffer = mBuf.buffer.slice(mBuf.byteOffset, mBuf.byteOffset + mBuf.byteLength);

        const oValidBmp: Bitmap = new Bitmap();
        oValidBmp.read(oValidBuffer);
        const mValidBmp: Bitmap = new Bitmap();
        mValidBmp.read(mValidBuffer);

        const result: Response = differencesValidator.validate(oValidBmp, mValidBmp);
        assert.strictEqual(result.status, IPStatus.E_INVALID_DIFFERENCES_COUNT);
        done();
    });

});
