import { assert } from "chai";
import * as fs from "fs";
import { ImageGetter } from "../../../app/services/imageProcessing/getImage.service";

const filename: string = "imageGetterTestOriginal.bmp";
const validFilePath: string = "./test/res/imageProcessing/validTest.bmp";
const validNewPath: string = "./res/images/imageGetterTestOriginal.bmp";
let imgBuffer: Buffer;

describe("Image Getter Service", () => {

    before(() => {
        imgBuffer = fs.readFileSync(validFilePath);
        fs.writeFileSync(validNewPath, imgBuffer);
    });

    it("should get the right Image", (done: MochaDone) => {
        assert.strictEqual(ImageGetter.get(filename).toString("base64"), imgBuffer.toString("base64"));
        done();
    });

    it("should get the right image extension", (done: MochaDone) => {
        assert.strictEqual(ImageGetter.getImageExtension(filename), "bmp");
        done();
    });

    after(() => {
        fs.unlinkSync(validNewPath);
    });
});
