// tslint:disable
import { assert } from "chai";
import { Floor, FloorType } from "../../../../app/services/sceneGenerator/thematicGeneration/floor";
import { GeometricObject } from "../../../../app/services/sceneGenerator/geometricGeneration/geometricObject";
import { GeometricObjectType } from "../../../../../common/models/iGeometricObject";


const W: number = 10;
const H: number = 10;
const FLOOR: Floor = new Floor(W, H, FloorType.FOREST);

describe("Floor", () => {

    it("should get the correct attributes", (done: MochaDone) => {
        assert.equal(FLOOR.width, W);
        assert.equal(FLOOR.height, H);
        assert.equal(FLOOR.type, FloorType.FOREST);
        assert.equal(FLOOR.depth, 1);
        FLOOR.setPosition(0, 0, 0);
        assert.equal(FLOOR.position.x, 0);
        assert.equal(FLOOR.position.y, 0);
        assert.equal(FLOOR.position.z, 0);
        assert.equal(FLOOR.hexColor, FloorType.FOREST.valueOf());
        done();
    });

    it("should export the floor correctly", (done: MochaDone) => {
        const plane: GeometricObject = FLOOR.export();
        assert.equal(plane.type, GeometricObjectType.PLANE);
        done();
    });

});
