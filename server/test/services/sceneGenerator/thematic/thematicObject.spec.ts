// tslint:disable
import { assert } from "chai";
import { ThematicObject } from "../../../../app/services/sceneGenerator/thematicGeneration/thematicObject";
import { ThematicGeometry } from "../../../../app/services/sceneGenerator/thematicGeneration/thematicGeometry";
import { Point3D } from "../../../../app/utils/point3D";
import { BoundingBox } from "../../../../app/utils/boundingBox";
import { GeometricObject } from "../../../../app/services/sceneGenerator/geometricGeneration/geometricObject";
import { GeometricObjectType } from "../../../../../common/models/iGeometricObject";

const THEMATIC_OBJECT: ThematicObject = new ThematicObject();
const SPHERE_RADIUS: number = 10;
const SPHERE_OFFSET_1: Point3D = new Point3D(0, 0, 0);
const SPHERE_OFFSET_2: Point3D = new Point3D(0, -1, 0);
describe("ThematicObject", () => {

    it("should add a geometry", (done: MochaDone) => {
        const geometry1: ThematicGeometry = ThematicGeometry.sphere(SPHERE_RADIUS, SPHERE_OFFSET_1, 0);
        const geometry2: ThematicGeometry = ThematicGeometry.sphere(SPHERE_RADIUS, SPHERE_OFFSET_2, 0);
        THEMATIC_OBJECT.addGeometry(geometry1);
        THEMATIC_OBJECT.addGeometry(geometry2);

        assert.equal(THEMATIC_OBJECT.geometries.length, 2);
        done();
    });

    it("should get and set the position", (done: MochaDone) => {
        THEMATIC_OBJECT.position = new Point3D(0, 0, 0);
        assert.equal(THEMATIC_OBJECT.position.x, 0);
        assert.equal(THEMATIC_OBJECT.position.y, 0);
        assert.equal(THEMATIC_OBJECT.position.z, 0);
        done();
    });

    it("should return a valid bounding box", (done: MochaDone) => {
        const bbox: BoundingBox = THEMATIC_OBJECT.bbox;

        assert.equal(bbox.position.x, 0);
        assert.equal(bbox.position.y, 0);
        assert.equal(bbox.position.z, 0);

        assert.equal(bbox.dimension.x, SPHERE_RADIUS);
        assert.equal(bbox.dimension.y, SPHERE_RADIUS + 1);
        assert.equal(bbox.dimension.z, SPHERE_RADIUS);

        done();
    });

    it("should export to an array geometric objects", (done: MochaDone) => {
        const objects: GeometricObject[] = THEMATIC_OBJECT.export();

        objects.forEach((obj: GeometricObject) => {
            assert.equal(obj.type, GeometricObjectType.SPHERE);
            assert.equal(obj.r, SPHERE_RADIUS);
        });

        done();
    });



});
