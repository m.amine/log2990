// tslint:disable
import { assert } from "chai";
import { ThematicObjectGenerator } from "../../../../app/services/sceneGenerator/thematicGeneration/objectsGenerator/thematicObjectGenerator";
import { FloorType } from "../../../../app/services/sceneGenerator/thematicGeneration/floor";
import { ThematicObject } from "../../../../app/services/sceneGenerator/thematicGeneration/thematicObject";

describe("ThematicObjectGenerator", () => {

    it("should generate correctly a Thematic Object", (done: MochaDone) => {
        const obj: ThematicObject = ThematicObjectGenerator.randomObject(FloorType.FOREST);
        assert.isNotNull(obj);
        done();
    });

    it("should throw an error for an invalid floor type", (done: MochaDone) => {
        try {
            ThematicObjectGenerator.randomObject(10);
        } catch (e) {
            assert.equal((e as Error).message, "Invalid floor type");
        }
        
        done();
    });

});
