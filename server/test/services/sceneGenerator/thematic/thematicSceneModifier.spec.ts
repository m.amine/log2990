// tslint:disable

import { ThematicScene } from "../../../../app/services/sceneGenerator/thematicGeneration/thematicScene";
import { SceneModification } from "../../../../../common/models/iScene";
import { ThematicSceneModifier } from "../../../../app/services/sceneGenerator/thematicGeneration/thematicSceneModifier";
import { assert } from "chai";
import { ThematicObject } from "../../../../app/services/sceneGenerator/thematicGeneration/thematicObject";

const SCENE: ThematicScene = new ThematicScene();
const SCENE_OBJECTS: number = 10;
let thematicSceneModifier: ThematicSceneModifier;
const INVALID_SCENE_MODIFICATION_INPUT: number = 3;
const DIFFERENCES_COUNT: number = 7;

describe("ThematicSceneModifierThematic", () => {

    before(() => {
        SCENE.generate(SCENE_OBJECTS);
    });

    it("should add DIFFERENCES_COUNT objects to modified scene ", (done: MochaDone) => {
        const ALLOW_ADD: number[] = [SceneModification.ADD];
        thematicSceneModifier = new ThematicSceneModifier(ALLOW_ADD);

        let modifiedScene: ThematicScene = new ThematicScene();
        modifiedScene = thematicSceneModifier.modify(SCENE);

        const IS_ADDED_MODIFIED: boolean = modifiedScene.objects.length - SCENE.objects.length === DIFFERENCES_COUNT;

        assert.isTrue(IS_ADDED_MODIFIED);
        done();
    });

    it("should not change original scene's objects in the modified scene when adding objects ", (done: MochaDone) => {
        const ALLOW_ADD: number[] = [SceneModification.ADD];
        thematicSceneModifier = new ThematicSceneModifier(ALLOW_ADD);

        let modifiedScene: ThematicScene = new ThematicScene();

        modifiedScene = thematicSceneModifier.modify(SCENE);

        let areInModified: boolean = true;
        SCENE.objects.forEach((originalObject: ThematicObject) => {
            if (modifiedScene.objects.findIndex((modifiedObject: ThematicObject) =>
                modifiedObject.id === originalObject.id && modifiedObject.checksum() === originalObject.checksum()) === -1) {
                areInModified = false;
            }
        });

        assert.isTrue(areInModified);
        done();
    });

    it("should remove DIFFERENCES_COUNT objects of modified scene ", (done: MochaDone) => {
        const ALLOW_REMOVE: number[] = [SceneModification.REMOVE];
        thematicSceneModifier = new ThematicSceneModifier(ALLOW_REMOVE);

        let modifiedScene: ThematicScene = new ThematicScene();
        modifiedScene = thematicSceneModifier.modify(SCENE);

        const IS_REMOVE_MODIFIED: boolean = SCENE.objects.length - modifiedScene.objects.length === DIFFERENCES_COUNT;

        assert.isTrue(IS_REMOVE_MODIFIED);
        done();
    });

    it("should not change original scene's objects in the modified scene when removing objects ", (done: MochaDone) => {
        const ALLOW_REMOVE: number[] = [SceneModification.REMOVE];
        thematicSceneModifier = new ThematicSceneModifier(ALLOW_REMOVE);

        let modifiedScene: ThematicScene = new ThematicScene();
        modifiedScene = thematicSceneModifier.modify(SCENE);

        let areInModified: boolean = true;
        modifiedScene.objects.forEach((modifiedObject: ThematicObject) => {
            if (SCENE.objects.findIndex((originalObject: ThematicObject) =>
                modifiedObject.id === originalObject.id && modifiedObject.checksum() === originalObject.checksum()) === -1) {
                areInModified = false;
            }
        });

        assert.isTrue(areInModified);
        done();
    });

    it("should change DIFFERENCES_COUNT objects of modified scene ", (done: MochaDone) => {
        const ALLOW_CHANGE: number[] = [SceneModification.CHANGE];
        thematicSceneModifier = new ThematicSceneModifier(ALLOW_CHANGE);

        let modifiedScene: ThematicScene = new ThematicScene();
        modifiedScene = thematicSceneModifier.modify(SCENE);

        let differences: number = 0;

        SCENE.objects.forEach((originalObject: ThematicObject, index: number) => {
            if (originalObject.id === modifiedScene.objects[index].id &&
                originalObject.checksum() !== modifiedScene.objects[index].checksum()) {
                differences++;
            }
        });

        const IS_CHANGED_MODIFIED: boolean = differences === DIFFERENCES_COUNT;

        assert.isTrue(IS_CHANGED_MODIFIED);
        done();
    });

    it("should keep the same length when changing the modified scene ", (done: MochaDone) => {
        const ALLOW_CHANGE: number[] = [SceneModification.CHANGE];
        thematicSceneModifier = new ThematicSceneModifier(ALLOW_CHANGE);

        let modifiedScene: ThematicScene = new ThematicScene();
        modifiedScene = thematicSceneModifier.modify(SCENE);

        const ARE_SAME_LENGTH: boolean = SCENE.objects.length === modifiedScene.objects.length;

        assert.isTrue(ARE_SAME_LENGTH);
        done();
    });

    it("should respect the range of the modifiedScene.objects.length ", (done: MochaDone) => {
        const ALLOW_ALL: number[] = [SceneModification.ADD, SceneModification.REMOVE, SceneModification.CHANGE];
        thematicSceneModifier = new ThematicSceneModifier(ALLOW_ALL);

        let modifiedScene: ThematicScene = new ThematicScene();
        modifiedScene = thematicSceneModifier.modify(SCENE);

        const IS_IN_RANGE: boolean = modifiedScene.objects.length >= SCENE_OBJECTS - DIFFERENCES_COUNT &&
            modifiedScene.objects.length <= SCENE_OBJECTS + DIFFERENCES_COUNT;

        assert.isTrue(IS_IN_RANGE);
        done();
    });

    it("should throw an exception when this.DIFFERENCES_COUNT > originalScene.objects.length ", (done: MochaDone) => {
        const ALLOW_REMOVE_CHANGE: number[] = [SceneModification.REMOVE, SceneModification.CHANGE];
        thematicSceneModifier = new ThematicSceneModifier(ALLOW_REMOVE_CHANGE);

        SCENE.objects.splice(DIFFERENCES_COUNT - 1, SCENE.objects.length - DIFFERENCES_COUNT + 1);

        assert.throws(() => thematicSceneModifier.modify(SCENE), Error, "There is more differences than original objects");
        done();
    });

    it("should throw an exception when allowedModifications contains an invalid input ", (done: MochaDone) => {
        const ALLOW_INVALID_INPUT: number[] = [INVALID_SCENE_MODIFICATION_INPUT];
        thematicSceneModifier = new ThematicSceneModifier(ALLOW_INVALID_INPUT);

        assert.throws(() => thematicSceneModifier.modify(SCENE), Error, "Invalid SceneModification input");
        done();
    });
// tslint:disable-next-line:max-file-line-count
});