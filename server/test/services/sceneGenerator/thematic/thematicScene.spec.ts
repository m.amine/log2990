import { assert } from "chai";
import { ThematicScene } from "../../../../app/services/sceneGenerator/thematicGeneration/thematicScene";

const SCENE_OBJECTS: number = 200;

describe("ThematicScene", () => {

    it("should generate correctly the thematic scene", (done: MochaDone) => {
        const thematicScene: ThematicScene = new ThematicScene();
        thematicScene.generate(SCENE_OBJECTS);

        // Correct Amount of objects
        assert.equal(thematicScene.objects.length, SCENE_OBJECTS);
        assert.equal(thematicScene.objectsCount, SCENE_OBJECTS);

        // Correct Amount of floors
        let floorsAmount: number = SCENE_OBJECTS / thematicScene.OBJECTS_PER_FLOOR;
        floorsAmount = (SCENE_OBJECTS % thematicScene.OBJECTS_PER_FLOOR === 0) ? floorsAmount : floorsAmount + 1;
        floorsAmount = Math.floor(floorsAmount);
        assert.equal(thematicScene.floors.length, floorsAmount);
        assert.equal(thematicScene.floors.length, thematicScene.rows * thematicScene.blocks);
        done();
    });

});
