import { assert } from "chai";
import { SceneModification, SceneType } from "../../../../common/models/iScene";
import { GeometricObject } from "../../../app/services/sceneGenerator/geometricGeneration/geometricObject";
import { GeometricObjectGenerator } from "../../../app/services/sceneGenerator/geometricGeneration/geometricObjectGenerator";
import { GeometricScene } from "../../../app/services/sceneGenerator/geometricGeneration/geometricScene";
import { SceneModifier } from "../../../app/services/sceneGenerator/geometricGeneration/geometricSceneModifier";

const SCENE: GeometricScene = new GeometricScene();
const SCENE_OBJECTS: number = 10;
let currentSceneType: SceneType;
let SCENE_OBJECT_GEN: GeometricObjectGenerator;
let sceneModifier: SceneModifier;
const INVALID_SCENE_MODIFICATION_INPUT: number = 3;

describe("SceneModifierGeometric", () => {

    before(() => {
        currentSceneType = SceneType.CLASSIC;

        SCENE.backgroundColor = 0x000000;
        SCENE.type = currentSceneType;
        SCENE.objects = [];

        SCENE_OBJECT_GEN = new GeometricObjectGenerator(SCENE_OBJECTS, SCENE.boundingBoxDimension.dimension.x);
        for (let i: number = 0; i < SCENE_OBJECTS; i++) {
            SCENE.objects.push(SCENE_OBJECT_GEN.randomSceneObject(SCENE));
        }
    });

    it("should add DIFFERENCES_COUNT objects to modified scene ", (done: MochaDone) => {
        const ALLOW_ADD: number[] = [SceneModification.ADD];
        sceneModifier = new SceneModifier(ALLOW_ADD);

        let modifiedScene: GeometricScene = new GeometricScene();
        modifiedScene = sceneModifier.modify(SCENE);

        const IS_ADDED_MODIFIED: boolean = modifiedScene.objects.length - SCENE.objects.length === sceneModifier.DIFFERENCES_COUNT;

        assert.isTrue(IS_ADDED_MODIFIED);
        done();
    });

    it("should not change original scene's objects in the modified scene when adding objects ", (done: MochaDone) => {
        const ALLOW_ADD: number[] = [SceneModification.ADD];
        sceneModifier = new SceneModifier(ALLOW_ADD);

        let modifiedScene: GeometricScene = new GeometricScene();

        modifiedScene = sceneModifier.modify(SCENE);

        let areInModified: boolean = true;
        SCENE.objects.forEach((originalObject: GeometricObject) => {
            if (modifiedScene.objects.findIndex((modifiedObject: GeometricObject) =>
                modifiedObject.id === originalObject.id && modifiedObject.checksum() === originalObject.checksum()) === -1) {
                areInModified = false;
            }
        });

        assert.isTrue(areInModified);
        done();
    });

    it("should remove DIFFERENCES_COUNT objects of modified scene ", (done: MochaDone) => {
        const ALLOW_REMOVE: number[] = [SceneModification.REMOVE];
        sceneModifier = new SceneModifier(ALLOW_REMOVE);

        let modifiedScene: GeometricScene = new GeometricScene();
        modifiedScene = sceneModifier.modify(SCENE);

        const IS_REMOVE_MODIFIED: boolean = SCENE.objects.length - modifiedScene.objects.length === sceneModifier.DIFFERENCES_COUNT;

        assert.isTrue(IS_REMOVE_MODIFIED);
        done();
    });

    it("should not change original scene's objects in the modified scene when removing objects ", (done: MochaDone) => {
        const ALLOW_REMOVE: number[] = [SceneModification.REMOVE];
        sceneModifier = new SceneModifier(ALLOW_REMOVE);

        let modifiedScene: GeometricScene = new GeometricScene();
        modifiedScene = sceneModifier.modify(SCENE);

        let areInModified: boolean = true;
        modifiedScene.objects.forEach((modifiedObject: GeometricObject) => {
            if (SCENE.objects.findIndex((originalObject: GeometricObject) =>
                modifiedObject.id === originalObject.id && modifiedObject.checksum() === originalObject.checksum()) === -1) {
                areInModified = false;
            }
        });

        assert.isTrue(areInModified);
        done();
    });

    it("should change DIFFERENCES_COUNT objects of modified scene ", (done: MochaDone) => {
        const ALLOW_CHANGE: number[] = [SceneModification.CHANGE];
        sceneModifier = new SceneModifier(ALLOW_CHANGE);

        let modifiedScene: GeometricScene = new GeometricScene();
        modifiedScene = sceneModifier.modify(SCENE);

        let differences: number = 0;

        SCENE.objects.forEach((originalObject: GeometricObject, index: number) => {
            if (originalObject.id === modifiedScene.objects[index].id &&
                originalObject.checksum() !== modifiedScene.objects[index].checksum()) {
                differences++;
            }
        });

        const IS_CHANGED_MODIFIED: boolean = differences === sceneModifier.DIFFERENCES_COUNT;

        assert.isTrue(IS_CHANGED_MODIFIED);
        done();
    });

    it("should keep the same length when changing the modified scene ", (done: MochaDone) => {
        const ALLOW_CHANGE: number[] = [SceneModification.CHANGE];
        sceneModifier = new SceneModifier(ALLOW_CHANGE);

        let modifiedScene: GeometricScene = new GeometricScene();
        modifiedScene = sceneModifier.modify(SCENE);

        const ARE_SAME_LENGTH: boolean = SCENE.objects.length === modifiedScene.objects.length;

        assert.isTrue(ARE_SAME_LENGTH);
        done();
    });

    it("should respect the range of the modifiedScene.objects.length ", (done: MochaDone) => {
        const ALLOW_ALL: number[] = [SceneModification.ADD, SceneModification.REMOVE, SceneModification.CHANGE];
        sceneModifier = new SceneModifier(ALLOW_ALL);

        let modifiedScene: GeometricScene = new GeometricScene();
        modifiedScene = sceneModifier.modify(SCENE);

        const IS_IN_RANGE: boolean = modifiedScene.objects.length >= SCENE_OBJECTS - sceneModifier.DIFFERENCES_COUNT &&
            modifiedScene.objects.length <= SCENE_OBJECTS + sceneModifier.DIFFERENCES_COUNT;

        assert.isTrue(IS_IN_RANGE);
        done();
    });

    it("should throw an exception when this.DIFFERENCES_COUNT > originalScene.objects.length ", (done: MochaDone) => {
        const ALLOW_REMOVE_CHANGE: number[] = [SceneModification.REMOVE, SceneModification.CHANGE];
        sceneModifier = new SceneModifier(ALLOW_REMOVE_CHANGE);

        SCENE.objects.splice(sceneModifier.DIFFERENCES_COUNT - 1, SCENE.objects.length - sceneModifier.DIFFERENCES_COUNT + 1);

        assert.throws(() => sceneModifier.modify(SCENE), Error, "There is more differences than original objects");
        done();
    });

    it("should throw an exception when allowedModifications contains an invalid input ", (done: MochaDone) => {
        const ALLOW_INVALID_INPUT: number[] = [INVALID_SCENE_MODIFICATION_INPUT];
        sceneModifier = new SceneModifier(ALLOW_INVALID_INPUT);

        assert.throws(() => sceneModifier.modify(SCENE), Error, "Invalid SceneModification input");
        done();
    });
// tslint:disable-next-line:max-file-line-count
});
