import { assert } from "chai";
import { SceneCollisionDetector } from "../../../app/services/sceneGenerator/geometricGeneration/geometricCollisionDetector";
import { GeometricObject } from "../../../app/services/sceneGenerator/geometricGeneration/geometricObject";
import { GeometricObjectGenerator } from "../../../app/services/sceneGenerator/geometricGeneration/geometricObjectGenerator";
import { GeometricScene } from "../../../app/services/sceneGenerator/geometricGeneration/geometricScene";

const scene: GeometricScene = new GeometricScene();
const SCENE_OBJECTS: number = 100;
const sceneObjectGen: GeometricObjectGenerator = new GeometricObjectGenerator(SCENE_OBJECTS, scene.boundingBoxDimension.dimension.x);
const sceneCollisionDetector: SceneCollisionDetector = new SceneCollisionDetector();

describe("SceneObjectGenerator", () => {

    it("should generate a new SceneObject that doesn't collide with any objects of the scene", (done: MochaDone) => {
        for (let i: number = 0; i < SCENE_OBJECTS; i++) {
            const sceneObj: GeometricObject = sceneObjectGen.randomSceneObject(scene) as GeometricObject;
            assert.isFalse(sceneCollisionDetector.detect(scene, sceneObj));
            scene.objects.push(sceneObj);
        }
        done();
    });

});
