// tslint:disable
import { assert } from "chai";
import { SceneGenerationStatus as SGStatus } from "../../../../common/communication/errors";
import { ServerResponse } from "../../../../common/communication/serverResponse";
import { SceneGenerator, SceneGeneratorResponse as SGResponse } from "../../../app/services/sceneGenerator/sceneGenerator.service";

const SCENE_OBJECTS: number = 200;
const DIFF_COUNT: number = 7;
const ALLOWED_MODIFICATIONS: boolean[] = [true, true, false];

describe("SceneGenerator", () => {

    it("should generate correctly the original and modified Scene Geometric", (done: MochaDone) => {
        const sceneGenerator: SceneGenerator = new SceneGenerator();
        const response: ServerResponse<SGStatus, SGResponse> =
        sceneGenerator.generateScenes(SCENE_OBJECTS, ALLOWED_MODIFICATIONS, 0);
        assert.isNotNull(response.response);
        assert.strictEqual((response.response as SGResponse).scenes.first.objects.length, SCENE_OBJECTS);
        assert.isAtLeast((response.response as SGResponse).scenes.second.objects.length, SCENE_OBJECTS - DIFF_COUNT);
        assert.isAtMost((response.response as SGResponse).scenes.second.objects.length, SCENE_OBJECTS + DIFF_COUNT);
        done();
    });

    it("should generate correctly the original and modified Scene Thematic", (done: MochaDone) => {
        const sceneGenerator: SceneGenerator = new SceneGenerator();
        const response: ServerResponse<SGStatus, SGResponse> =
        sceneGenerator.generateScenes(SCENE_OBJECTS, ALLOWED_MODIFICATIONS, 1);
        assert.isNotNull(response.response);
        const objects: number = (response.response as SGResponse).scenes.first.objects.length;
        assert.isAtLeast(objects, SCENE_OBJECTS);
        done();
    });

    it("should return an error if the object count is invalid", (done: MochaDone) => {
        const sceneGenerator: SceneGenerator = new SceneGenerator();
        let response: ServerResponse<SGStatus, SGResponse> = sceneGenerator.generateScenes(5, ALLOWED_MODIFICATIONS, 0);
        assert.isNull(response.response);
        assert.equal(response.status, SGStatus.E_INVALID_OBJECT_COUNT);

        response = sceneGenerator.generateScenes(5, ALLOWED_MODIFICATIONS, 300);
        assert.isNull(response.response);
        assert.equal(response.status, SGStatus.E_INVALID_OBJECT_COUNT);
        done();
    });

});
