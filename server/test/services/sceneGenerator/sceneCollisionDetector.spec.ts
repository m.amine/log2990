import { assert } from "chai";
import { GeometricObjectType } from "../../../../common/models/iGeometricObject";
import { SceneCollisionDetector } from "../../../app/services/sceneGenerator/geometricGeneration/geometricCollisionDetector";
import { GeometricObject } from "../../../app/services/sceneGenerator/geometricGeneration/geometricObject";
import { GeometricScene } from "../../../app/services/sceneGenerator/geometricGeneration/geometricScene";
import { Point3D } from "../../../app/utils/point3D";

const scene: GeometricScene = new GeometricScene();
const size: number = 10;
const halfFactor: number = 2;
const sLeft: number = (size / halfFactor) * -1;
const sRight: number = (size / halfFactor);
const sceneCollisionDetector: SceneCollisionDetector = new SceneCollisionDetector();

const collisionsMatrix: Point3D[] = [
    new Point3D(0, 0, 0),
    new Point3D(sLeft, sLeft, sLeft),
    new Point3D(sLeft, sLeft, sRight),
    new Point3D(sLeft, sRight, sLeft),
    new Point3D(sLeft, sRight, sRight),
    new Point3D(sRight, sLeft, sLeft),
    new Point3D(sRight, sLeft, sRight),
    new Point3D(sRight, sRight, sLeft),
    new Point3D(sRight, sRight, sRight),
];

const noCollisionsMatrix: Point3D[] = [
    new Point3D(sLeft, sLeft, sLeft - size),
    new Point3D(sLeft, sLeft, sRight + size),
    new Point3D(sLeft - size, sRight, sLeft),
    new Point3D(sLeft, sRight + size, sRight),
    new Point3D(sRight + size, sLeft, sLeft),
    new Point3D(sRight, sLeft - size, sRight),
    new Point3D(sRight + size, sRight + size, sLeft - size),
    new Point3D(sRight + size, sRight + size, sRight - size),
];

describe("SceneCollisionDetector", () => {

    before(() => {
            const sceneObject: GeometricObject = new GeometricObject();
            sceneObject.type = GeometricObjectType.BOX;
            sceneObject.w = size;
            sceneObject.h = size;
            sceneObject.d = size;
            sceneObject.position = new Point3D(0, 0, 0);
            scene.objects.push(sceneObject);
    });

    it("should detect collisions if there is with different sides and shapes", (done: MochaDone) => {
        const maxEnum: number = (Object.keys(GeometricObjectType).length / halfFactor) - 1;

        for (let i: number = 0; i < maxEnum; i++) {
            collisionsMatrix.forEach((pos: Point3D) => {
                const sceneObject: GeometricObject = new GeometricObject();
                sceneObject.type = i;
                sceneObject.w = size;
                sceneObject.h = size;
                sceneObject.d = size;
                sceneObject.r = size / halfFactor;
                sceneObject.position = pos;
                assert.isTrue(sceneCollisionDetector.detect(scene, sceneObject));
            });
        }
        done();
    });

    it("should not detect collisions if there's none with multiples sides and shapes", (done: MochaDone) => {
        const maxEnum: number = (Object.keys(GeometricObjectType).length / halfFactor) - 1;

        for (let i: number = 0; i < maxEnum; i++) {
            noCollisionsMatrix.forEach((pos: Point3D, idx: number) => {
                const sceneObject: GeometricObject = new GeometricObject();
                sceneObject.type = i;
                sceneObject.w = size;
                sceneObject.h = size;
                sceneObject.d = size;
                sceneObject.r = size / halfFactor;
                sceneObject.position = pos;
                assert.isFalse(sceneCollisionDetector.detect(scene, sceneObject));
            });
        }
        done();
    });

});
