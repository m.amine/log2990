// tslint:disable
import { assert } from "chai";
import { GeometricScene } from "../../../../app/services/sceneGenerator/geometricGeneration/geometricScene";
import { Point3D } from "../../../../app/utils/point3D";
import { SceneType } from "../../../../../common/models/iScene";
import { GeometricObject } from "../../../../app/services/sceneGenerator/geometricGeneration/geometricObject";
import { GeometricObjectType } from "../../../../../common/models/iGeometricObject";

describe("GeometricScene", () => {

    it("should copy correctly the geometric scene", (done: MochaDone) => {
        const obj: GeometricObject = new GeometricObject();
        obj.type = GeometricObjectType.BOX;

        const deco: GeometricObject = new GeometricObject();
        deco.type = GeometricObjectType.PLANE;

        const scene: GeometricScene = new GeometricScene();
        scene.backgroundColor = 0;
        scene.boundingBoxDimension = {dimension: new Point3D(0, 0, 0), position: new Point3D(0, 0, 0)};
        scene.type = SceneType.CLASSIC;
        scene.objects.push(obj);
        scene.decoration.push(deco);

        const copy: GeometricScene = new GeometricScene();
        copy.copy(scene);

        assert.strictEqual(copy.backgroundColor, scene.backgroundColor);
        assert.strictEqual(copy.type, scene.type);
        assert.strictEqual(copy.objects.length, scene.objects.length);
        assert.strictEqual(copy.decoration.length, scene.decoration.length);
        done();
    });

});
