import { assert } from "chai";
import { SceneProcessingStatus as SPStatus} from "../../../common/communication/errors";
import {DifferenceDetector3D} from "../../app/services/differenceDetector3D.service";

describe("Difference Detector 3D Service  ", () => {

    it("should validate a difference ", (done: Mocha.Done) => {
        let status: SPStatus;
        const service: DifferenceDetector3D = new DifferenceDetector3D() ;
        const sceneObjectId: string = "one";
        const idDifferences: string[] = ["one", "two"];
        status = service.validateDifference(sceneObjectId, idDifferences);
        assert.equal(status, SPStatus.S_OK);
        done();
    });

    it("should not validate a difference ", (done: Mocha.Done) => {
        let status: SPStatus;
        const service: DifferenceDetector3D = new DifferenceDetector3D() ;
        const sceneObjectId: string = "three";
        const idDifferences: string[] = ["one", "two"];
        status = service.validateDifference(sceneObjectId, idDifferences);
        assert.equal(status, SPStatus.E_NOT_DIFFERENCE);
        done();
    });
  });
