import {BestGameRecord}from "../../app/services/bestGameRecord.service";
import {Record}from "../../../common/models/record";
import { assert } from "chai";


describe("Best Game Record ", () => {
    let record:Record[];
    
    it("should add a record in a table of record", (done: MochaDone) => {
        record=[{time: 4, holder: "hanane "},{time: 5, holder: "Abdel"},{time: 7, holder: "Boussari "}];
        assert.isDefined(BestGameRecord.addRecordInGame(record, "space",6));
      done();
    });
  
    it("should get a right position after added a Record and sort the table of record", (done: MochaDone) => {
        record=[{time: 4, holder: "hanane "},{time: 5, holder: "Abdel"},{time: 7, holder: "Boussari "}];
        BestGameRecord.addRecordInGame(record, "space",6);
        assert.equal(BestGameRecord.getPositionOfRecord(record,6,"space"),2,"these index are strictly equal");
      done();
    });

    it("should keep the oldest best time at the best place on the table of Record when there is a tie.", (done: MochaDone) => {
        record=[{time: 4, holder: "hanane "},{time: 5, holder: "Abdel"},{time: 7, holder: "Boussari "}];
        BestGameRecord.addRecordInGame(record, "space",5);
        assert.equal(BestGameRecord.getPositionOfRecord(record,5,"Abdel"),1,"these index are strictly equal");
      done();
    });
  
 
  });
  