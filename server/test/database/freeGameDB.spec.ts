import { assert, expect } from "chai";
import { IScene } from "../../../common/models/iScene";
import { Record } from "../../../common/models/record";
import { FreeGameDB } from "../../app/database/databaseProxy/freeGameDB";
import { IFreeGame } from "../../app/database/freeGame/IFreeGame";
import { FREEGAME } from "../../app/database/freeGame/freeGame.model";
import { Utils } from "../../app/utils/utils";

const service: FreeGameDB = new FreeGameDB();
let game: IFreeGame;
const SCENE: IScene = {} as IScene;
const SCENEMODIFIED: IScene = {} as IScene;
const n: number = 3;

describe("FreeGameDB", () => {

    it("Create a Free Game", () => {
        it("should generate random scores if argument is false", (done: MochaDone) => {
            const GAME: IFreeGame = service.newGame(game.name, game.thumbnail, game.originalScene, game.modifiedScene, game.differencesId);
            expect(GAME.vsTime).to.lengthOf(n);

            let previousTime: number = 0;
            GAME.soloTime.forEach((record: Record) => {
                assert.isAbove(previousTime, record.time);
                previousTime = record.time;
            });

            previousTime = 0;
            GAME.vsTime.forEach((record: Record) => {
                assert.isAbove(previousTime, record.time);
                previousTime = record.time;
            });
            done();

        });
    });
    it("should reset a new free game", (done: MochaDone) => {
        const sTime: Record[] = Utils.generateRandomRecords(n);
        const vsTime: Record[] = Utils.generateRandomRecords(n);
        game = new FREEGAME({
            gameId: Utils.uniqueId(),
            name: "name",
            thumbnail: "",
            originalScene: SCENE,
            modifiedScene: SCENEMODIFIED,
            soloTime: sTime,
            vsTime: vsTime,
        });

        const GAME: IFreeGame = service.newGame(game.name, game.thumbnail, game.originalScene, game.modifiedScene, game.differencesId);
        service.reset(game.name);
        let previousTime: number = 0;
        GAME.soloTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });
        previousTime = 0;
        GAME.vsTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });
        expect(GAME.vsTime).to.lengthOf(n);
        done();

    });

    it("should get all free Game ", (done: MochaDone) => {
        const GAME: IFreeGame = service.newGame(game.name, game.thumbnail, game.originalScene, game.modifiedScene, game.differencesId);

        let previousTime: number = 0;
        GAME.soloTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });

        previousTime = 0;
        GAME.vsTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });
        service.getAll();
        assert.isNotEmpty(GAME);
        done();

    });
    it("should delete free Game ", (done: MochaDone) => {
        const GAME: IFreeGame = service.newGame(game.name, game.thumbnail, game.originalScene, game.modifiedScene, game.differencesId);

        let previousTime: number = 0;
        GAME.soloTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });

        previousTime = 0;
        GAME.vsTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });

        service.delete(game.id);
        expect(GAME.isDeleted);
        done();
    });
});
