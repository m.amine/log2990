import { assert, expect } from "chai";
import { Record } from "../../../common/models/record";
import { SimpleGameDB } from "../../app/database/databaseProxy/simpleGameDB";
import { ISimpleGame } from "../../app/database/simpleGame/ISimpleGame";
import { SIMPLEGAME } from "../../app/database/simpleGame/simpleGame.model";
import { Utils } from "../../app/utils/utils";

const service: SimpleGameDB = new SimpleGameDB();
let game: ISimpleGame;
const n: number = 3;

describe("SimpleGameDB", () => {

    it("Create a Simple Game", () => {
        it("should generate random scores if argument is false", (done: MochaDone) => {
            const GAME: ISimpleGame = service.newGame(game.name, game.originalImage, game.modifiedImage, game.differencesImage);

            expect(GAME.vsTime).to.lengthOf(n);

            let previousTime: number = 0;
            GAME.soloTime.forEach((record: Record) => {
                assert.isAbove(previousTime, record.time);
                previousTime = record.time;
            });

            previousTime = 0;
            GAME.vsTime.forEach((record: Record) => {
                assert.isAbove(previousTime, record.time);
                previousTime = record.time;
            });
            done();
        });
    });
    it("should reset a new Simple game", (done: MochaDone) => {
        const sTime: Record[] = Utils.generateRandomRecords(n);
        const vsTime: Record[] = Utils.generateRandomRecords(n);
        game = new SIMPLEGAME({
            gameId: Utils.uniqueId(),
            name: "name",
            originalImage: "",
            modifiedImage: "",
            differencesImage: "",
            soloTime: sTime,
            vsTime: vsTime,

        });
        const GAME: ISimpleGame = service.newGame(game.name, game.originalImage, game.modifiedImage, game.differencesImage);
        service.reset(game.name);
        let previousTime: number = 0;
        GAME.soloTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });
        previousTime = 0;
        GAME.vsTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });
        expect(GAME.vsTime).to.lengthOf(n);
        done();
    });
    it("should get all simple Game ", (done: MochaDone) => {
        const GAME: ISimpleGame = service.newGame(game.name, game.originalImage, game.modifiedImage, game.differencesImage);

        let previousTime: number = 0;
        GAME.soloTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });

        previousTime = 0;
        GAME.vsTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });
        service.getAll();
        assert.isNotEmpty(GAME);
        done();

    });
    it("should delete simple Game ", (done: MochaDone) => {
        const GAME: ISimpleGame = service.newGame(game.name, game.originalImage, game.modifiedImage, game.differencesImage);

        let previousTime: number = 0;
        GAME.soloTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });

        previousTime = 0;
        GAME.vsTime.forEach((record: Record) => {
            assert.isAbove(record.time, previousTime);
            previousTime = record.time;
        });

        service.delete(game.id);
        expect(GAME.isDeleted);
        done();
    });

});
