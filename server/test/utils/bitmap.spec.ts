import { assert } from "chai";
import { ImageProcessingStatus as IPStatus } from "../../../common/communication/errors";
import { Pixel } from "../../../common/models/pixel";
import { Point } from "../../../common/models/point";
import { Bitmap } from "../../app/utils/bitmap";

const validBitmap: Bitmap = new Bitmap();
const invalidBitmap: Bitmap = new Bitmap();

const validImageString: string = "Qk1GAAAAAAAAADYAAAAoAAAAAgAAAAIAAAABABgAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAA3oeBzEg/AADuwb3YcWoAAA==";
let buf: Buffer = Buffer.from(validImageString, "base64");
const validBuffer: ArrayBuffer = buf.buffer.slice(buf.byteOffset, buf.byteOffset + buf.byteLength);

const invalidImageString: string = "ABCDAAAAAAAAADYAAAAoAAAAAgAAAAIAAAABABgAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAA3oeBzEg/AADuwb3YcWoAAA==";
buf = Buffer.from(invalidImageString, "base64");
const invalidBuffer: ArrayBuffer = buf.buffer.slice(buf.byteOffset, buf.byteOffset + buf.byteLength);

const expectedWidth: number = 2;
const expectedHeight: number = 2;

describe("Bitmap", () => {

    it("should read a non corrupted bitmap buffer", (done: MochaDone) => {
        assert.strictEqual(validBitmap.read(validBuffer), IPStatus.S_OK);
        done();
    });

    it("should copy the header of another Bitmap", (done: MochaDone) => {
        const copyBitmap: Bitmap = new Bitmap();
        copyBitmap.shallowCopy(validBitmap);
        assert.isTrue(copyBitmap.isBmp());
        assert.strictEqual(copyBitmap.width(), validBitmap.width());
        assert.strictEqual(copyBitmap.height(), validBitmap.height());
        done();
    });

    it("should convert back to the original buffer", (done: MochaDone) => {
        const outBuffer: ArrayBuffer = validBitmap.toBuffer();
        assert.strictEqual(outBuffer.byteLength, validBuffer.byteLength);
        done();
    });

    it("should convert back to the original base64 string", (done: MochaDone) => {
        const outImageString: string = validBitmap.toDataURL();
        const stringHeader: string = "data:image/bmp;base64,";
        assert.strictEqual(outImageString, stringHeader + validImageString);
        done();
    });

    it("should be a valid bmp file", (done: MochaDone) => {
        assert.isTrue(validBitmap.isBmp());
        done();
    });

    it("should have a valid width", (done: MochaDone) => {
        assert.strictEqual(validBitmap.width(), expectedWidth);
        done();
    });

    it("should have a valid height", (done: MochaDone) => {
        assert.strictEqual(validBitmap.height(), expectedHeight);
        done();
    });

    it("should be able to read all the pixels", (done: MochaDone) => {
        const otherBitmap: Bitmap = new Bitmap();
        otherBitmap.shallowCopy(validBitmap); // Black pixels
        for (let i: number = 0; i < otherBitmap.width(); i++) {
            for (let j: number = 0; j < otherBitmap.height(); j++) {
                assert.isTrue(otherBitmap.pixel(i, j).equals(new Pixel(0, 0, 0)));
            }
        }
        done();
    });

    it("should be able to read set a pixel", (done: MochaDone) => {
        const otherBitmap: Bitmap = new Bitmap();
        otherBitmap.shallowCopy(validBitmap); // Black pixels
        assert.isTrue(otherBitmap.pixel(0, 0).equals(new Pixel(0, 0, 0)));
        const pixVal: number = 100;
        otherBitmap.setPixel(0, 0, new Pixel(pixVal, pixVal, pixVal));
        assert.isTrue(otherBitmap.pixel(0, 0).equals(new Pixel(pixVal, pixVal, pixVal)));
        done();
    });

    it("should detect different pixels", (done: MochaDone) => {
        const otherBitmap: Bitmap = new Bitmap();
        otherBitmap.shallowCopy(validBitmap);
        const points: Point[] = validBitmap.getDifferentPixels(otherBitmap);
        const expectDifferences: number = 4;
        assert.strictEqual(points.length, expectDifferences);
        done();
    });

    it("should not read a corrupted bitmap buffer", (done: MochaDone) => {
        assert.notStrictEqual(invalidBitmap.read(invalidBuffer), IPStatus.S_OK);
        done();
    });

});
