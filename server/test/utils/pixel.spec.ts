import { assert } from "chai";
import { Pixel } from "../../../common/models/pixel";

describe("Pixel", () => {

    it("should be equal if the pixel is the same", (done: MochaDone) => {
        const pix1: Pixel = new Pixel(0, 0, 0);
        const pix2: Pixel = new Pixel(0, 0, 0);
        assert.isTrue(pix1.equals(pix2));
        done();
    });

    it("should be not equal if the pixel is not the same", (done: MochaDone) => {
        const pix1: Pixel = new Pixel(0, 0, 0);
        const pix2: Pixel = new Pixel(0, 0, 1);
        assert.isNotTrue(pix1.equals(pix2));
        done();
    });
});
