import { assert } from "chai";
import { Point } from "../../../common/models/point";

describe("Point", () => {

    it("should be equal if the point is the same", (done: MochaDone) => {
        const p1: Point = new Point(0, 0);
        const p2: Point = new Point(0, 0);
        assert.isTrue(p1.equals(p2));
        done();
    });

    it("should be not equal if the point is not the same", (done: MochaDone) => {
        const p1: Point = new Point(0, 0);
        const p2: Point = new Point(0, 1);
        assert.isNotTrue(p1.equals(p2));
        done();
    });
});
