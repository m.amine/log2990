import { assert } from "chai";
import { Record } from "../../../common/models/record";
import { Utils } from "../../app/utils/utils";

const numberOfRecords: number = 3;
const min: number = 0;
const max: number = 100;
const generateMultipleIds: number = 10;

describe("Utils", () => {

    it("should generate a random number in the specified range", (done: MochaDone) => {
        const randomValue: number = Utils.rand(min, max);
        assert.isTrue(randomValue >= min);
        assert.isTrue(randomValue <= max);
        done();
    });

    it("should sort the random records", (done: MochaDone) => {
        const records: Record[] = Utils.generateRandomRecords(numberOfRecords);
        for (let i: number = 0; i < numberOfRecords - 1; i++) {
            assert.isAbove(records[i + 1].time, records[i].time);
        }
        done();
    });

    it("should return a unique id", (done: MochaDone) => {
        const tableIds: string[] = [];

        for (let i: number = 0; i < generateMultipleIds; i++) {
            tableIds.push(Utils.uniqueId());
        }

        tableIds.forEach((uniqueId: string, index: number) => {
            for (let i: number = index + 1; i < tableIds.length; i++) {
                assert.isTrue(uniqueId !== tableIds[i]);
            }
        });
        done();

    });
});
