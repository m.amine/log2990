import { ImageProcessingStatus as IPStatus } from "../../../../common/communication/errors";
import { Pixel } from "../../../../common/models/pixel";
import { Point } from "../../../../common/models/point";
import { SimpleGame } from "../../../../common/models/simpleGame";
import { PixelMap } from "../../../../common/models/types";
import { Bitmap } from "../../utils/bitmap";
import { Utils } from "../../utils/utils";

const WHITE: number = 255;
const DETECTION_COLOR: Pixel = new Pixel(WHITE, WHITE, WHITE);

export class DifferenceDetector {
    private point: Point;
    private pointNeighbors: Point[];
    private game: SimpleGame;
    private originalImage: Bitmap;
    private differencesImage: Bitmap;

    public constructor(point: Point, game: SimpleGame) {
        this.point = point;
        this.game = game;
        this.pointNeighbors = [];
    }

    public detect(): [IPStatus, PixelMap] {
        const differencesImageStatus: [IPStatus, Bitmap] = this.fetchBitmap(this.game.differencesImage.toString());
        if (differencesImageStatus[0] !== IPStatus.S_OK) {
            return [differencesImageStatus[0], []];
        }

        const originalImageStatus: [IPStatus, Bitmap] = this.fetchBitmap(this.game.originalImage.toString());
        if (originalImageStatus[0] !== IPStatus.S_OK) {
            return [originalImageStatus[0], []];
        }

        this.originalImage = originalImageStatus[1];
        this.differencesImage = differencesImageStatus[1];

        if (!this.pixelValid(this.point.x, this.point.y)) {
            return [IPStatus.E_NOT_FOUND, []];
        }

        this.findNeighboringPixels(this.point);

        return [IPStatus.S_OK, this.generatePixelMap()];
    }

    private fetchBitmap(url: string): [IPStatus, Bitmap] {
        const filename: string = url.split("/").pop() as string;
        const path: string = Utils.rootPath() + "res/images/" + filename;
        const bitmap: Bitmap = new Bitmap();
        const status: IPStatus = bitmap.fromPath(path);

        return [status, bitmap];
    }

    private pixelValid(x: number, y: number): boolean {
        let validPixel: boolean = x >= 0 && x < this.differencesImage.width();
        validPixel = validPixel && y >= 0 && y < this.differencesImage.height();
        validPixel = validPixel && this.differencesImage.pixel(x, y).equals(DETECTION_COLOR);

        return validPixel;
    }

    private findNeighboringPixels(currentPoint: Point): void {
        for (let i: number = currentPoint.x - 1; i <= currentPoint.x + 1; i++) {
            for (let j: number = currentPoint.y - 1; j <= currentPoint.y + 1; j++) {
                if (this.pixelValid(i, j)) {
                    const newPoint: Point = new Point(i, j);
                    if (!this.pointNeighbors.some((point: Point) => point.equals(newPoint))) {
                        this.pointNeighbors.push(newPoint);
                        this.findNeighboringPixels(newPoint);
                    }
                }
            }
        }
    }

    private generatePixelMap(): PixelMap {
        const pixelMap: PixelMap = [];
        this.pointNeighbors.forEach((point: Point) => {
            const pixel: Pixel = this.originalImage.pixel(point.x, point.y);
            pixelMap.push({position: point, value: pixel});
        });

        return pixelMap;
    }
}
