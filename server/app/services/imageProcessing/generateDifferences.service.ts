import { ImageProcessingStatus as IPStatus } from "../../../../common/communication/errors";
import { ServerResponse } from "../../../../common/communication/serverResponse";
import { Pixel } from "../../../../common/models/pixel";
import { Point } from "../../../../common/models/point";
import { Bitmap } from "../../utils/bitmap";
import { DifferencesValidator } from "./differencesValidator.service";

type ValidatorResponse = ServerResponse<IPStatus, Point[]>;
const DIFFERENCE_COLOR: number = 255;

export class DifferencesGenerator {

    private static readonly IMAGE_HEIGHT: number = 480;
    private static readonly IMAGE_WIDTH: number = 640;
    private validatorResponse: ValidatorResponse;
    private differencesValidator: DifferencesValidator;

    public constructor() {
        this.differencesValidator = new DifferencesValidator();
    }

    public start(original: ArrayBuffer, modified: ArrayBuffer): { status: IPStatus, imgs: string[] } {
        const originalImg: Bitmap = new Bitmap();
        let status: IPStatus = originalImg.read(original);
        if (status !== IPStatus.S_OK) { return ({ status: status, imgs: [] }); }

        const modifiedImg: Bitmap = new Bitmap();
        status = modifiedImg.read(modified);
        if (status !== IPStatus.S_OK) { return ({ status: status, imgs: [] }); }

        status = this.validate(originalImg, modifiedImg);
        if (status !== IPStatus.S_OK) { return ({ status: status, imgs: [] }); }

        const outImg: Bitmap = this.generate(originalImg, this.validatorResponse.response as Point[]);

        const oImg: string = originalImg.save();
        const mImg: string = modifiedImg.save();
        const dImg: string = outImg.save();

        return ({ status: IPStatus.S_OK, imgs: [oImg, mImg, dImg] });
    }

    private validate(original: Bitmap, modified: Bitmap): IPStatus {
        if (original.height() !== DifferencesGenerator.IMAGE_HEIGHT ||
            original.width() !== DifferencesGenerator.IMAGE_WIDTH) {
            return IPStatus.E_INVALID_IMAGE_SIZE;
        }

        if (original.height() !== modified.height() || original.width() !== modified.width()) {
            return IPStatus.E_INVALID_IMAGE_SIZE;
        }

        this.validatorResponse = this.differencesValidator.validate(original, modified);

        return this.validatorResponse.status;
    }

    private generate(original: Bitmap, whitePixels: Point[]): Bitmap {

        const outImage: Bitmap = new Bitmap();
        outImage.shallowCopy(original);

        whitePixels.forEach((p: Point) => outImage.setPixel(p.x, p.y, new Pixel(DIFFERENCE_COLOR, DIFFERENCE_COLOR, DIFFERENCE_COLOR)));

        return outImage;
    }

}
