import * as fs from "fs";
import { Utils } from "../../utils/utils";

export class ImageGetter {

    public static get(filename: string): Buffer {
        const imgPath: string = Utils.rootPath() + "res/images/" + filename;

        return fs.readFileSync(imgPath);
    }

    public static getImageExtension(filename: string): string {
        return filename.split(".")[1];
    }
}
