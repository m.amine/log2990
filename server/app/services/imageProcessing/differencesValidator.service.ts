import { ImageProcessingStatus as IPStatus } from "../../../../common/communication/errors";
import { ServerResponse } from "../../../../common/communication/serverResponse";
import { Point } from "../../../../common/models/point";
import { Bitmap } from "../../utils/bitmap";

type Response = ServerResponse<IPStatus, Point[]>;
export class DifferencesValidator {

    private static readonly ALLOWED_DIFFERENCES: number = 7;
    private diffPointsStretched: Point[];

    public constructor() {
        this.diffPointsStretched = [];
    }

    public validate(original: Bitmap, modified: Bitmap): Response {
        const differencesCount: number = this.countDifferences(original, modified);

        if (differencesCount === DifferencesValidator.ALLOWED_DIFFERENCES) {
            return { status: IPStatus.S_OK, response: this.diffPointsStretched };
        }

        return { status: IPStatus.E_INVALID_DIFFERENCES_COUNT, response: null };
    }

    private countDifferences(original: Bitmap, modified: Bitmap): number {
        const diffPoints: Point[] = original.getDifferentPixels(modified);

        diffPoints.forEach((point: Point) => this.pixelStretch(point, this.diffPointsStretched));
        const diffPointsIndexes: number[] = [];

        // Getting all the indexes of the pixels (it's easier to work with ints)
        this.diffPointsStretched.forEach((e: Point, i: number) => { diffPointsIndexes.push(i); });

        let indexesGroups: number[][] = [];
        indexesGroups = this.sortNeighborPixels(this.diffPointsStretched, diffPointsIndexes, indexesGroups);

        return this.mergeGroups(indexesGroups);
    }

    private sortNeighborPixels(
        diffPoints: Point[],
        diffPointsIndexes: number[],
        indexesGroups: number[][]): number[][] {
        while (diffPointsIndexes.length !== 0) {
            const current: number = diffPointsIndexes.pop() as number;
            const groupId: number = this.whichGroup(current, indexesGroups);
            if (groupId === -1) {
                const group: number[] = [];
                group.push(current);

                diffPointsIndexes.forEach((other: number) => {
                    if (this.isNeighbor(diffPoints[current], diffPoints[other])) {
                        group.push(other);
                    }
                });
                indexesGroups.push(group);
            } else {
                diffPointsIndexes.forEach((other: number) => {
                    if (this.isNeighbor(diffPoints[current], diffPoints[other])) {
                        if (this.whichGroup(other, indexesGroups) !== groupId) {
                            indexesGroups[groupId].push(other);
                        }
                    }
                });
            }
        }

        return indexesGroups;
    }

    private mergeGroups(indexesGroups: number[][]): number {
        let count: number = 0;
        let found: boolean = false;
        while (indexesGroups.length !== 0) {
            found = false;
            for (let i: number = 1; i < indexesGroups.length; i++) {
                if (indexesGroups[0].some((r: number) => indexesGroups[i].indexOf(r) >= 0)) {
                    indexesGroups[i].concat(indexesGroups[0]);
                    indexesGroups.splice(0, 1);
                    found = true;
                    break;
                }
            }

            if (!found) {
                indexesGroups.splice(0, 1);
                count++;
            }
        }

        return count;
    }

    private isNeighbor(a: Point, b: Point): boolean {
        if (a.x === b.x || a.x === b.x + 1 || a.x === b.x - 1) {
            if (a.y === b.y || a.y === b.y + 1 || a.y === b.y - 1) {
                return true;
            }
        }

        return false;
    }

    private whichGroup(e: number, groups: number[][]): number {
        let ret: number = -1;

        groups.forEach((group: number[], i: number) => {
            if (group.indexOf(e) !== -1) {
                ret = i;
            }
        });

        return ret;
    }

    private pixelStretch(point: Point, points: Point[]): Point[] {
        const x: number = point.x;
        const y: number = point.y;
        const RANGE: number = 3;

        for (let i: number = x - RANGE; i <= x + RANGE; i++) {
            for (let j: number = y - RANGE; j <= y + RANGE; j++) {
                if (this.pixelValid(i, j, x, y)) {
                    const newPoint: Point = new Point(i, j);
                    if (!points.some((p: Point) => p.equals(newPoint))) {
                        points.push(newPoint);
                    }
                }
            }
        }

        return points;
    }

    private pixelValid(i: number, j: number, x: number, y: number): boolean {
        const HIGH_RANGE: number = 3;
        const LOW_RANGE: number = 2;
        const outOfRange: boolean = (i < 0 && j < 0) || (i !== x && (j === y - HIGH_RANGE || j === y + HIGH_RANGE)) ||
            (i === x + HIGH_RANGE && (j === y + LOW_RANGE || j === y - LOW_RANGE)) ||
            (i === x - HIGH_RANGE && (j === y + LOW_RANGE || j === y - LOW_RANGE));

        return !outOfRange;
    }

}
