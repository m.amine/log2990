import { SceneProcessingStatus as SPStatus} from "../../../common/communication/errors";

export class DifferenceDetector3D {

    public validateDifference(sceneObjectId: string, idDifferences: string[]): SPStatus {
        if (idDifferences.find((differenceId: string) => differenceId === sceneObjectId) === undefined) {
            return SPStatus.E_NOT_DIFFERENCE;
        } else {
            return SPStatus.S_OK;
        }
    }
}
