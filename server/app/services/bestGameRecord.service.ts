import { Record } from "../../../common/models/record";
const MAXPOSITIONOFRECORD: number = 3;

export class BestGameRecord {

    public static addRecordInGame(records: Record[], userName: string, newScore: number): Record[] {

        records.pop();
        records[MAXPOSITIONOFRECORD] = {time: newScore, holder: userName };

        records.sort((a: Record, b: Record) => a.time - b.time);

        return records.slice(0, MAXPOSITIONOFRECORD);
    }

    public static getPositionOfRecord(records: Record[], score: number, userName: string): number {

       return records.findIndex((i: Record) => i.time === score && i.holder === userName);
    }

}
