import { SceneGenerationStatus as SGStatus } from "../../../../common/communication/errors";
import { ServerResponse } from "../../../../common/communication/serverResponse";
import { SceneModification, SceneType } from "../../../../common/models/iScene";
import { Pair } from "../../../../common/models/types";
import { Utils } from "../../utils/utils";
import { GeometricObject } from "./geometricGeneration/geometricObject";
import { GeometricObjectGenerator } from "./geometricGeneration/geometricObjectGenerator";
import { GeometricScene } from "./geometricGeneration/geometricScene";
import { SceneModifier } from "./geometricGeneration/geometricSceneModifier";
import { ThematicScene } from "./thematicGeneration/thematicScene";
import { ThematicSceneModifier } from "./thematicGeneration/thematicSceneModifier";

const MIN_SCENE_OBJECTS: number = 10;
const MAX_SCENE_OBJECTS: number = 200;

export interface SceneGeneratorResponse {
    scenes: Pair<GeometricScene>;
    differencesIds: string[];
}

type SGResponse = SceneGeneratorResponse;

export class SceneGenerator {
    private originalScene: GeometricScene;
    private modifiedScene: GeometricScene;
    private objectsCount: number;
    private sceneObjectGen: GeometricObjectGenerator;
    private sceneType: SceneType;
    private sceneModifications: SceneModification[];
    private differencesIds: string[];

    public constructor() {
        this.originalScene = new GeometricScene();
        this.modifiedScene = new GeometricScene();
        this.objectsCount = 0;
    }

    public generateScenes(objectsCount: number, checkBoxes: boolean[], sceneType: number): ServerResponse<SGStatus, SGResponse> {

        if (objectsCount < MIN_SCENE_OBJECTS || objectsCount > MAX_SCENE_OBJECTS) {
            return this.failureResponse(SGStatus.E_INVALID_OBJECT_COUNT);
        }

        this.sceneType = SceneType[SceneType[sceneType]];
        this.objectsCount = objectsCount;
        this.sceneObjectGen = new GeometricObjectGenerator(objectsCount, this.originalScene.boundingBoxDimension.dimension.x);
        this.sceneModifications = this.convertToModifications(checkBoxes);

        this.generate();

        return this.successResponse();
    }

    private convertToModifications(checkBoxes: boolean[]): SceneModification[] {
        const modification: SceneModification[] = [];
        checkBoxes.forEach((value: boolean, index: number) => {
            if (value) {
                modification.push(index);
            }
        });

        return modification;
    }

    private generate(): void {
        if (this.sceneType === SceneType.CLASSIC) {
            this.newGeometricScenes();
        } else {
            this.newThematicScenes();
        }
    }

    private newGeometricScenes(): void {
        this.originalScene.backgroundColor = GeometricObjectGenerator.COLORS[Utils.rand(0, GeometricObjectGenerator.COLORS.length - 1)];
        this.originalScene.type = this.sceneType;

        for (let i: number = 0; i < this.objectsCount; i++) {
            const sceneObject: GeometricObject = this.sceneObjectGen.randomSceneObject(this.originalScene);
            this.originalScene.objects.push(sceneObject);
        }

        const sceneModifier: SceneModifier = new SceneModifier(this.sceneModifications);
        this.modifiedScene = sceneModifier.modify(this.originalScene);
        this.differencesIds = sceneModifier.differencesId;
    }

    private newThematicScenes(): void {
        const original: ThematicScene = new ThematicScene().generate(this.objectsCount);
        this.originalScene = original.export();
        const sceneModifier: ThematicSceneModifier = new ThematicSceneModifier(this.sceneModifications);
        const modified: ThematicScene = sceneModifier.modify(original);
        this.modifiedScene = modified.export();
        this.differencesIds = sceneModifier.differencesId;
    }

    private failureResponse(status: SGStatus): ServerResponse<SGStatus, SGResponse> {
        return {
            status: status,
            response: null,
        };
    }

    private successResponse(): ServerResponse<SGStatus, SGResponse> {
        return {
            status: SGStatus.S_OK,
            response: {
                scenes: {first: this.originalScene, second: this.modifiedScene},
                differencesIds: this.differencesIds,
            },
        };
    }
}
