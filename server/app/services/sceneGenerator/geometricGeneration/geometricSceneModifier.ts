import { SceneModification } from "../../../../../common/models/iScene";
import { Utils } from "../../../utils/utils";
import { GeometricObject } from "./geometricObject";
import { GeometricObjectGenerator } from "./geometricObjectGenerator";
import { GeometricScene } from "./geometricScene";

export class SceneModifier {
    public readonly DIFFERENCES_COUNT: number = 7;
    private allowedModifications: SceneModification[];
    private modifiedScene: GeometricScene;
    private sceneObjectGenerator: GeometricObjectGenerator;
    private nonModifiedObjectsChecksum: string[];
    private modificationCount: number;
    public differencesId: string[];

    public constructor(allowedModifications: SceneModification[]) {
        this.allowedModifications = [];
        this.allowedModifications = allowedModifications;
        this.nonModifiedObjectsChecksum = [];
        this.modificationCount = 0;
        this.differencesId = [];
    }

    public modify(originalScene: GeometricScene): GeometricScene {
        this.copy(originalScene);
        this.sceneObjectGenerator = new GeometricObjectGenerator(this.modifiedScene.objects.length,
                                                                 this.modifiedScene.boundingBoxDimension.dimension.x);

        const REMOVE_ALLOWED: boolean = this.allowedModifications.findIndex((modification: SceneModification) =>
            modification === SceneModification.REMOVE) !== -1;
        const CHANGE_ALLOWED: boolean = this.allowedModifications.findIndex((modification: SceneModification) =>
            modification === SceneModification.CHANGE) !== -1;

        this.modificationCount = 0;

        if ((REMOVE_ALLOWED || CHANGE_ALLOWED) && this.DIFFERENCES_COUNT > originalScene.objects.length) {
            throw new Error("There is more differences than original objects");
        } else {
            while (this.modificationCount < this.DIFFERENCES_COUNT) {
                this.addModification();
            }
        }

        this.modificationCount = 0;

        return this.modifiedScene;
    }

    private copy(originalScene: GeometricScene): void {
        this.modifiedScene = new GeometricScene();
        this.modifiedScene.copy(originalScene);

        originalScene.objects.forEach((object: GeometricObject) => {
            this.nonModifiedObjectsChecksum.push(object.checksum());
        });
    }

    private addModification(): void {
        const CURRENT_MODIFICATION: SceneModification = this.allowedModifications[Utils.rand(0, this.allowedModifications.length - 1)];

        switch (CURRENT_MODIFICATION) {
            case SceneModification.ADD:
                this.add();
                break;
            case SceneModification.REMOVE:
                this.remove();
                break;
            case SceneModification.CHANGE:
                this.change();
                break;
            default:
                throw new Error("Invalid SceneModification input");
        }

        this.modificationCount++;
    }

    private add(): void {
        const SCENE_OBJECT: GeometricObject = this.sceneObjectGenerator.randomSceneObject(this.modifiedScene);
        this.modifiedScene.objects.push(SCENE_OBJECT);
        this.differencesId.push(SCENE_OBJECT.id);
    }

    private remove(): void {
        const REMOVE_CHECKSUM_IDX: number = Utils.rand(0, this.nonModifiedObjectsChecksum.length - 1);
        const REMOVE_CHECKSUM: string = this.nonModifiedObjectsChecksum[REMOVE_CHECKSUM_IDX];

        this.modifiedScene.objects.forEach((object: GeometricObject, index: number) => {
            if (object.checksum() === REMOVE_CHECKSUM) {
                this.differencesId.push(this.modifiedScene.objects[index].id);
                this.modifiedScene.objects.splice(index, 1);
            }
        });

        this.nonModifiedObjectsChecksum.splice(REMOVE_CHECKSUM_IDX, 1);
    }

    private change(): void {
        const CHANGE_CHECKSUM_IDX: number = Utils.rand(0, this.nonModifiedObjectsChecksum.length - 1);
        const CHANGE_CHECKSUM: string = this.nonModifiedObjectsChecksum[CHANGE_CHECKSUM_IDX];
        let randomColor: number = GeometricObjectGenerator.COLORS[Utils.rand(0, GeometricObjectGenerator.COLORS.length - 1)];

        const CHANGE_IDX: number = this.modifiedScene.objects.findIndex((object: GeometricObject) =>
            object.checksum() === CHANGE_CHECKSUM);
        const CURRENT_COLOR: number = this.modifiedScene.objects[CHANGE_IDX].hexColor;

        while (CURRENT_COLOR === randomColor) {
            randomColor = GeometricObjectGenerator.COLORS[Utils.rand(0, GeometricObjectGenerator.COLORS.length - 1)];
        }

        this.modifiedScene.objects[CHANGE_IDX].hexColor = randomColor;
        this.differencesId.push(this.modifiedScene.objects[CHANGE_IDX].id);
        this.nonModifiedObjectsChecksum.splice(CHANGE_CHECKSUM_IDX, 1);
    }
}
