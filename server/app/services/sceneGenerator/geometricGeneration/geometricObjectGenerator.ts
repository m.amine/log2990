import { GeometricObjectType } from "../../../../../common/models/iGeometricObject";
import { ObjectColor } from "../../../../../common/models/iScene";
import { Point3D } from "../../../utils/point3D";
import { Utils } from "../../../utils/utils";
import { SceneCollisionDetector } from "./geometricCollisionDetector";
import { GeometricObject } from "./geometricObject";
import { GeometricScene } from "./geometricScene";

export class GeometricObjectGenerator {
    public static readonly COLORS: number[] = [
        ObjectColor.GREEN,
        ObjectColor.BLUE,
        ObjectColor.YELLOW,
        ObjectColor.CYAN,
        ObjectColor.WHITE,
    ];

    private readonly PADDING_RATIO: number = 10;
    private BOUNDS: number;
    private objectsCount: number = 1;
    private refSize: number = 0;

    public constructor(objectsCount: number, sceneBound: number) {
        this.objectsCount = objectsCount;
        this.BOUNDS = sceneBound;
        this.calculateRefSize();
    }

    public randomSceneObject(scene: GeometricScene): GeometricObject {
        const sceneObject: GeometricObject = new GeometricObject();
        sceneObject.id = Utils.uniqueId();
        const halfFactor: number = 2;

        const maxEnum: number = (Object.keys(GeometricObjectType).length / halfFactor) - 1;
        do {
            sceneObject.type = GeometricObjectType[GeometricObjectType[Utils.rand(0, maxEnum)]];
        } while (sceneObject.type === GeometricObjectType.PLANE);

        sceneObject.hexColor = GeometricObjectGenerator.COLORS[Utils.rand(0, GeometricObjectGenerator.COLORS.length - 1)];

        const sceneCollisionDetector: SceneCollisionDetector = new SceneCollisionDetector();
        do {
            sceneObject.w = this.randomSize();
            sceneObject.h = sceneObject.w;
            sceneObject.d = sceneObject.w;
            sceneObject.r = this.randomSize() / halfFactor;

            sceneObject.position = this.randomPosition();
        } while (sceneCollisionDetector.detect(scene, sceneObject));

        sceneObject.rotation = this.randomRotation();

        return sceneObject;
    }

    private randomSize(): number {
        const minRef: number = 0.5;
        const maxRef: number = 1.5;

        return Utils.rand(Math.round(this.refSize * minRef), this.refSize * maxRef);
    }

    private randomPosition(): Point3D {
        const halfFactor: number = 0.5;
        const maxRef: number = 1.5;
        const size: number = this.refSize * maxRef;
        const lower: number = Math.round((this.BOUNDS * halfFactor) * -1) + size;
        const upper: number = Math.floor(this.BOUNDS * halfFactor) - size;

        const x: number = Utils.rand(lower, upper);
        const y: number = Utils.rand(lower, upper);
        const z: number = Utils.rand(lower, upper);

        return new Point3D(x, y, z);
    }

    private randomRotation(): Point3D {
        const lower: number = 0;
        const upper: number = 360;

        const x: number = Utils.rand(lower, upper);
        const y: number = Utils.rand(lower, upper);
        const z: number = Utils.rand(lower, upper);

        return new Point3D(x, y, z);
    }

    private calculateRefSize(): void {
        const exponent: number = 3;
        const objectSpace: number = Math.pow(this.BOUNDS, exponent) / this.objectsCount;
        this.refSize = Math.floor(Math.cbrt(objectSpace) / this.PADDING_RATIO);
    }
}
