import { IBound } from "../../../../../common/models/iBound";
import { IScene, SceneType } from "../../../../../common/models/iScene";
import { Point3D } from "../../../utils/point3D";
import { GeometricObject } from "./geometricObject";

const BOUNDING_BOX_DIMENSION: number = 1600;
export class GeometricScene implements IScene {
    public backgroundColor: number;
    public decoration: GeometricObject[] = [];
    public objects: GeometricObject[] = [];
    public type: SceneType;
    public boundingBoxDimension: IBound;

    public constructor() {
        this.decoration = [];
        this.objects = [];
        this.boundingBoxDimension = {
            dimension: new Point3D(BOUNDING_BOX_DIMENSION, BOUNDING_BOX_DIMENSION, BOUNDING_BOX_DIMENSION),
            position: new Point3D(0, 0, 0),
        };
    }

    public copy(scene: GeometricScene): void {
        this.backgroundColor = scene.backgroundColor;
        this.type = scene.type;
        this.boundingBoxDimension = JSON.parse(JSON.stringify(scene.boundingBoxDimension));

        scene.objects.forEach((originalObject: GeometricObject, index: number) => {
            this.objects[index] = new GeometricObject();
            this.objects[index].copy(originalObject);
        });

        scene.decoration.forEach((originalObject: GeometricObject, index: number) => {
            this.decoration[index] = new GeometricObject();
            this.decoration[index].copy(originalObject);
        });
    }
}
