import { GeometricObjectType, IGeometricObject } from "../../../../../common/models/iGeometricObject";
import { Point3D } from "../../../utils/point3D";
import { ThematicGeometry } from "../thematicGeneration/thematicGeometry";

export class GeometricObject implements IGeometricObject {
    public id: string;
    public hexColor: number;
    public position: Point3D;
    public rotation: Point3D;
    public castShadow: boolean;
    public type: GeometricObjectType;
    public w: number;
    public h: number;
    public d: number;
    public r: number;

    public constructor() {
        this.castShadow = true;
        this.w = 0;
        this.h = 0;
        this.d = 0;
        this.r = 0;
        this.position = new Point3D(0, 0, 0);
        this.rotation = new Point3D(0, 0, 0);
    }

    public static fromGeometry(geometry: ThematicGeometry): GeometricObject {
        const object: GeometricObject = new GeometricObject();
        object.id = geometry.id;
        object.hexColor = geometry.hexColor;
        object.position = geometry.absolutePosition;
        object.rotation = new Point3D(0, 0, 0);
        object.type = geometry.type;
        object.w = geometry.w;
        object.h = geometry.h;
        object.d = geometry.d;
        object.r = geometry.r;

        return object;
    }

    public copy(object: GeometricObject): void {
        this.id = object.id;
        this.hexColor = object.hexColor;
        this.position = new Point3D(object.position.x, object.position.y, object.position.z);
        this.rotation = new Point3D(object.rotation.x, object.rotation.y, object.rotation.z);
        this.castShadow = object.castShadow;
        this.type = object.type;
        this.w = object.w;
        this.h = object.h;
        this.d = object.d;
        this.r = object.r;
    }

    public checksum(): string {
        return this.type.toString() +
            this.w.toString() +
            this.h.toString() +
            this.d.toString() +
            this.r.toString() +
            this.hexColor +
            this.position.x.toString() +
            this.position.y.toString() +
            this.position.z.toString() +
            this.rotation.x.toString() +
            this.rotation.y.toString() +
            this.rotation.z.toString();
    }
}
