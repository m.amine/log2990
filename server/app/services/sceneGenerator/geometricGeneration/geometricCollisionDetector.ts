import { GeometricObjectType } from "../../../../../common/models/iGeometricObject";
import { GeometricObject } from "./geometricObject";
import { GeometricScene } from "./geometricScene";

export class SceneCollisionDetector {

    public detect(scene: GeometricScene, scObj: GeometricObject): boolean {
        return scene.objects.some((curObj: GeometricObject) => this.areColliding(scObj, curObj));
    }

    private areColliding(obj1: GeometricObject, obj2: GeometricObject): boolean {
        const radius1: number = this.boundingBoxDimension(obj1);
        const radius2: number = this.boundingBoxDimension(obj2);

        // Collision in x axis
        let colliding: boolean = this.areCollidingOnAxis(obj1.position.x, obj2.position.x, radius1, radius2);

        // Collision in y axis
        colliding = colliding && this.areCollidingOnAxis(obj1.position.y, obj2.position.y, radius1, radius2);

        // Collision in z axis
        colliding = colliding && this.areCollidingOnAxis(obj1.position.z, obj2.position.z, radius1, radius2);

        return colliding;
    }

    private areCollidingOnAxis(pos1: number, pos2: number, measure1: number, measure2: number): boolean {
        const posL: number = (pos1 > pos2) ? pos2 : pos1;
        const posR: number = (pos1 > pos2) ? pos1 : pos2;
        const measureL: number = (pos1 > pos2) ? measure2 : measure1;
        const measureR: number = (pos1 > pos2) ? measure1 : measure2;

        return (posL + measureL) >= (posR - measureR);
    }

    private boundingBoxDimension(obj: GeometricObject): number {
        let measure: number = 0;
        const factor: number = 2;

        switch (obj.type) {
            case GeometricObjectType.BOX:
                measure = obj.w;
                break;

            case GeometricObjectType.SPHERE:
            case GeometricObjectType.PYRAMID:
                measure = obj.r * factor;
                break;

            default:
                const diameter: number = obj.r * factor;
                measure = (diameter > obj.h) ? diameter : obj.h;
                break;
        }

        return Math.round(measure / factor);
    }

}
