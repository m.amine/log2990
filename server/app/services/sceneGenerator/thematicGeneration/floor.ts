import { GeometricObjectType } from "../../../../../common/models/iGeometricObject";
import { Point3D } from "../../../utils/point3D";
// import { Utils } from "../../../utils/utils";
import { GeometricObject } from "../geometricGeneration/geometricObject";

export enum FloorType {
    STREET = 0x5B5B5B,
    FOREST = 0x75B029,
}

export class Floor {
    public width: number;
    public height: number;
    public depth: number;
    public position: Point3D;
    public type: FloorType;

    public static randomFloorType(): FloorType {
        // let type: FloorType;
        // const halfFactor: number = 0.5;

        // do {
        //     const maxEnum: number = (Object.keys(FloorType).length * halfFactor) - 1;
        //     type = FloorType[FloorType[Utils.rand(0, maxEnum)]];
        // } while (type === FloorType.STREET); // Floors

        return FloorType.FOREST;
    }

    public static copy(floor: Floor): Floor {
        const newFloor: Floor = new Floor(floor.width, floor.height, floor.type);
        newFloor.position = new Point3D(floor.position.x, floor.position.y, floor.position.z);
        newFloor.depth = floor.depth;

        return newFloor;
    }

    public constructor(width: number, height: number, type: FloorType = FloorType.STREET) {
        this.width = width;
        this.height = height;
        this.position = new Point3D(0, 0, 0);
        this.depth = 1;
        this.type = type;
    }

    public setPosition(x: number, y: number, z: number): void {
        this.position.x = x;
        this.position.y = y;
        this.position.z = z;
    }

    public get hexColor(): number {
        return this.type.valueOf();
    }

    public export(): GeometricObject {
        const object: GeometricObject = new GeometricObject();
        object.type = GeometricObjectType.PLANE;
        object.w = this.width;
        object.h = this.height;
        object.d = this.depth;
        object.hexColor = this.hexColor;
        object.position = this.position;
        object.rotation = new Point3D(0, 0, 0);

        return object;
    }

}
