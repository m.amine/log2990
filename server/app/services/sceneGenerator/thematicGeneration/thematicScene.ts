import { Point3D } from "../../../utils/point3D";
import { Utils } from "../../../utils/utils";
import { GeometricScene } from "../geometricGeneration/geometricScene";
import { Floor } from "./floor";
import { ThematicObjectGenerator } from "./objectsGenerator/thematicObjectGenerator";
import { ThematicObject } from "./thematicObject";

const SCALE_FACTOR: number = 500;
const BLOCK_SIZE: number = 4;
const FULL_SIZE: number = 5;
const halfFactor: number = 0.5;
const SKY_COLOR: number = 0x74B0F7;
const MIN_OBJECTS_PER_FLOOR: number = 2;
const MAX_OBJECTS_PER_FLOOR: number = 5;
const SCENE_HEIGHT: number = 1000;
const FLOOR_DEPTH: number = 10;

interface SceneLayout {
    blocks: number;
    rows: number;
}

export class ThematicScene {
    public plane: Floor;
    public floors: Floor[];
    public blocks: number;
    public rows: number;
    public objects: ThematicObject[];
    public objectsCount: number;
    public OBJECTS_PER_FLOOR: number = 1;

    public static copy(other: ThematicScene): ThematicScene {
        const newScene: ThematicScene = new ThematicScene();
        newScene.plane = Floor.copy(other.plane);
        newScene.blocks = other.blocks;
        newScene.rows = other.rows;
        newScene.objectsCount = other.objectsCount;
        newScene.OBJECTS_PER_FLOOR = other.OBJECTS_PER_FLOOR;

        other.floors.forEach((floor: Floor) => {
            newScene.floors.push(Floor.copy(floor));
        });

        other.objects.forEach((object: ThematicObject) => {
            newScene.objects.push(ThematicObject.copy(object));
        });

        return newScene;
    }

    public constructor() {
        this.floors = [];
        this.objects = [];
    }

    public export(): GeometricScene {
        const scene: GeometricScene = new GeometricScene();

        scene.backgroundColor = SKY_COLOR;
        scene.decoration.push(this.plane.export());

        scene.boundingBoxDimension = {
            dimension: new Point3D(this.plane.width, SCENE_HEIGHT, this.plane.height),
            position: new Point3D(0, SCENE_HEIGHT * halfFactor - 1, 0),
        };

        this.floors.forEach((floor: Floor) => scene.decoration.push(floor.export()));
        this.objects.forEach((object: ThematicObject) => scene.objects = scene.objects.concat(object.export()));

        return scene;
    }

    public generate(objectsCount: number): ThematicScene {
        this.OBJECTS_PER_FLOOR = Utils.rand(MIN_OBJECTS_PER_FLOOR, MAX_OBJECTS_PER_FLOOR);
        const layout: SceneLayout = this.calculateLayout(objectsCount);
        this.initializeScene(layout.blocks, layout.rows, objectsCount);
        this.initializeFloors();
        this.initializeObjects();

        return this;
    }

    private calculateLayout(objectsCount: number): SceneLayout {
        let floorCount: number = Math.floor(objectsCount / this.OBJECTS_PER_FLOOR);
        floorCount = (objectsCount % this.OBJECTS_PER_FLOOR === 0) ?  floorCount : floorCount + 1;
        const factors: [number, number] = Utils.getRandomFactors(floorCount);
        const blocks: number = factors[0];
        const rows: number = factors[1];

        return {blocks: blocks, rows: rows};
    }

    private initializeScene(blocks: number, rows: number, objectsCount: number): void {
        const width: number = (BLOCK_SIZE + FULL_SIZE * (blocks - 1)) * SCALE_FACTOR;
        const height: number = (BLOCK_SIZE + FULL_SIZE * (rows - 1)) * SCALE_FACTOR;
        this.plane = new Floor(width, height);
        this.blocks = blocks;
        this.rows = rows;
        this.objectsCount = objectsCount;
    }

    private initializeFloors(): void {
        const yPos: number = 1;
        const dimension: number = BLOCK_SIZE * SCALE_FACTOR;
        const gap: number = (FULL_SIZE - BLOCK_SIZE) * SCALE_FACTOR;
        let startX: number = this.plane.position.x - this.plane.width * halfFactor;

        for (let i: number = 0; i < this.blocks; i++) {
            let startY: number = this.plane.position.z - this.plane.height * halfFactor;
            for (let j: number = 0; j < this.rows; j++) {
                const floor: Floor = new Floor(dimension, dimension, Floor.randomFloorType());
                const xPos: number = startX + dimension * halfFactor;
                const zPos: number = startY + dimension * halfFactor;
                floor.setPosition(xPos, yPos, zPos);
                floor.depth = FLOOR_DEPTH;
                this.floors.push(floor);
                startY += dimension + gap;
            }
            startX += dimension + gap;
        }
    }

    private initializeObjects(): void {
        this.floors.forEach((floor: Floor) => {
            for (let i: number = 0; i < this.OBJECTS_PER_FLOOR && this.objects.length < this.objectsCount; i++) {
                this.objects.push(this.randomObject(floor));
            }
        });
    }

    public randomObject(floor: Floor): ThematicObject {
        const object: ThematicObject = ThematicObjectGenerator.randomObject(floor.type);
        const dim: Point3D = object.dimension;

        do {
            const y: number = floor.position.y + dim.y * halfFactor;

            const minX: number = floor.position.x - floor.width * halfFactor + dim.x * halfFactor;
            const maxX: number = floor.position.x + floor.width * halfFactor - dim.x * halfFactor;
            const x: number = Utils.rand(minX, maxX);

            const minZ: number = floor.position.z - floor.height * halfFactor + dim.z * halfFactor;
            const maxZ: number = floor.position.z + floor.height * halfFactor - dim.z * halfFactor;
            const z: number = Utils.rand(minZ, maxZ);
            object.position = new Point3D(x, y, z);
        } while (this.checkInternalCollisions(object));

        return object;
    }

    private checkInternalCollisions(object: ThematicObject): boolean {
        return this.objects.some((sceneObject: ThematicObject) => sceneObject.bbox.isColliding(object.bbox));
    }
}
