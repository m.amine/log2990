import { BoundingBox } from "../../../utils/boundingBox";
import { Point3D } from "../../../utils/point3D";
import { Utils } from "../../../utils/utils";
import { GeometricObject } from "../geometricGeneration/geometricObject";
import { ThematicGeometry } from "./thematicGeometry";

const halfFactor: number = 0.5;

export class ThematicObject {
    private internalPosition: Point3D;
    public geometries: ThematicGeometry[];
    public id: string;

    public static copy(object: ThematicObject): ThematicObject {
        const newObject: ThematicObject = new ThematicObject();
        newObject.position = new Point3D(object.position.x, object.position.y, object.position.z);
        newObject.id = object.id;

        object.geometries.forEach((geometry: ThematicGeometry) => {
            newObject.geometries.push(ThematicGeometry.copy(geometry));
        });

        return newObject;
    }

    public constructor() {
        this.id = Utils.uniqueId();
        this.geometries = [];
    }

    public addGeometry(geometry: ThematicGeometry): void {
        geometry.id = this.id;
        this.geometries.push(geometry);
    }

    public export(): GeometricObject[] {
        const geometricObjects: GeometricObject[] = [];

        this.geometries.forEach((geometry: ThematicGeometry) => {
            geometricObjects.push(GeometricObject.fromGeometry(geometry));
        });

        return geometricObjects;
    }

    public get position(): Point3D {
        return this.internalPosition;
    }

    public set position(position: Point3D) {
        this.internalPosition = position;
        this.geometries.forEach((geometry: ThematicGeometry) => {
            geometry.setAbsolutePosition(this.internalPosition);
        });
    }

    public get dimension(): Point3D {
        const bigNumber: number = 10000;
        const smallNumber: number = -10000;
        let minX: number, maxX: number, minY: number, maxY: number, minZ: number, maxZ: number;
        minX = minY = minZ = bigNumber;
        maxX = maxY = maxZ = smallNumber;

        this.geometries.forEach((geometry: ThematicGeometry) => {
            const dimension: Point3D = geometry.dimension;
            const position: Point3D = geometry.relativePosition;

            minX = (minX < position.x - (dimension.x * halfFactor)) ? minX : position.x - (dimension.x * halfFactor);
            minY = (minY < position.y - (dimension.y * halfFactor)) ? minY : position.y - (dimension.y * halfFactor);
            minZ = (minZ < position.z - (dimension.z * halfFactor)) ? minZ : position.z - (dimension.z * halfFactor);

            maxX = (maxX > position.x + (dimension.x * halfFactor)) ? maxX : position.x + (dimension.x * halfFactor);
            maxY = (maxY > position.y + (dimension.y * halfFactor)) ? maxY : position.y + (dimension.y * halfFactor);
            maxZ = (maxZ > position.z + (dimension.z * halfFactor)) ? maxZ : position.z + (dimension.z * halfFactor);
        });

        return new Point3D(maxX - minX, maxY - minY, maxZ - minZ);
    }

    public get bbox(): BoundingBox {
        return new BoundingBox(this.position, this.dimension);
    }

    public checksum(): string {
        return this.id +
            this.position.x.toString() +
            this.position.y.toString() +
            this.position.z.toString() +
            this.geometries.map((geometry: ThematicGeometry) => geometry.checksum()).join("");
    }
}
