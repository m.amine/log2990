import { GeometricObjectType } from "../../../../../common/models/iGeometricObject";
import { Point3D } from "../../../utils/point3D";

export class ThematicGeometry {
    public id: string;
    public relativePosition: Point3D;
    public absolutePosition: Point3D;
    public type: GeometricObjectType;
    public hexColor: number;
    public w: number;
    public h: number;
    public d: number;
    public r: number;

    public static copy(geometry: ThematicGeometry): ThematicGeometry {
        const newGeometry: ThematicGeometry = new ThematicGeometry();
        newGeometry.id = geometry.id;
        newGeometry.relativePosition = this.copy3DPoint(geometry.relativePosition);
        newGeometry.absolutePosition = this.copy3DPoint(geometry.absolutePosition);
        newGeometry.type = geometry.type;
        newGeometry.hexColor = geometry.hexColor;
        newGeometry.w = geometry.w;
        newGeometry.h = geometry.h;
        newGeometry.d = geometry.d;
        newGeometry.r = geometry.r;

        return newGeometry;
    }

    public static copy3DPoint(position: Point3D): Point3D {
        return new Point3D(position.x, position.y, position.z);
    }

    public constructor() {
        this.w = 0;
        this.h = 0;
        this.d = 0;
        this.r = 0;
    }

    public static cube(radius: number, offset: Point3D, hexColor: number): ThematicGeometry {
        const newCube: ThematicGeometry = new ThematicGeometry();
        newCube.type = GeometricObjectType.BOX;
        newCube.h = radius;
        newCube.w = radius;
        newCube.d = radius;
        newCube.relativePosition = offset;
        newCube.hexColor = hexColor;

        return newCube;
    }

    public static cone(radius: number, height: number, offset: Point3D, hexColor: number): ThematicGeometry {
        const newCone: ThematicGeometry = new ThematicGeometry();
        newCone.type = GeometricObjectType.CONE;
        newCone.r = radius;
        newCone.h = height;
        newCone.relativePosition = offset;
        newCone.hexColor = hexColor;

        return newCone;
    }

    public static sphere(radius: number, offset: Point3D, hexColor: number): ThematicGeometry {
        const newSphere: ThematicGeometry = new ThematicGeometry();
        newSphere.type = GeometricObjectType.SPHERE;
        newSphere.r = radius;
        newSphere.relativePosition = offset;
        newSphere.hexColor = hexColor;

        return newSphere;
    }

    public static cylinder(radius: number, height: number, offset: Point3D, hexColor: number): ThematicGeometry {
        const newCylinder: ThematicGeometry = new ThematicGeometry();
        newCylinder.type = GeometricObjectType.CYLINDER;
        newCylinder.r = radius;
        newCylinder.h = height;
        newCylinder.relativePosition = offset;
        newCylinder.hexColor = hexColor;

        return newCylinder;
    }

    public setAbsolutePosition(position: Point3D): void {
        this.absolutePosition = new Point3D(0, 0, 0);
        this.absolutePosition.x = this.relativePosition.x + position.x;
        this.absolutePosition.y = this.relativePosition.y + position.y;
        this.absolutePosition.z = this.relativePosition.z + position.z;
    }

    public get dimension(): Point3D {
        let x: number, y: number, z: number;

        switch (this.type) {
            case GeometricObjectType.BOX:
                x = y = z = this.w;
                break;

            case GeometricObjectType.CONE:
            case GeometricObjectType.CYLINDER:
                x = z = this.r;
                y = this.h;
                break;

            case GeometricObjectType.SPHERE:
            case GeometricObjectType.PYRAMID:
                x = y = z = this.r;
                break;

            default:
                throw new Error("Invalid Geometry type");
        }

        return new Point3D(x, y, z);
    }

    public checksum(): string {

        return this.id +
            this.relativePosition.x.toString() +
            this.relativePosition.y.toString() +
            this.relativePosition.z.toString() +
            this.absolutePosition.x.toString() +
            this.absolutePosition.y.toString() +
            this.absolutePosition.z.toString() +
            this.type.toString() +
            this.hexColor.toString() +
            this.w.toString() +
            this.h.toString() +
            this.d.toString() +
            this.r.toString();
    }

}
