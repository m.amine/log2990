import { Point3D } from "../../../../utils/point3D";
import { Utils } from "../../../../utils/utils";
import { ThematicGeometry } from "../thematicGeometry";
import { ThematicObject } from "../thematicObject";

export enum TreeType {
    NORMAL,
    FIR,
    TRUNK,
    CUTTED,
    BUSH,
}

const GREEN: number = 0x75B029;
const BROWN: number = 0x42342E;
const halfFactor: number = 0.5;
const heightDivider: number = 20;
const widthDivider: number = 5;
const LEAF_COLORS: number[] = [GREEN];
const TRUNK_COLORS: number[] = [BROWN];
const TREE_HEIGHT: number = 200;
const TREE_WIDTH: number = 120;

export class TreeGenerator {

    public static randomTree(): ThematicObject {
        return this.Tree(this.randomTreeType());
    }

    private static randomTreeType(): TreeType {
        const maxEnum: number = (Object.keys(TreeType).length * halfFactor) - 1;

        return Utils.rand(0, maxEnum) as TreeType;
    }

    private static Tree(type: TreeType): ThematicObject {
        switch (type) {
            case TreeType.NORMAL:
                return this.normal();

            case TreeType.FIR:
                return this.fir();

            case TreeType.TRUNK:
                return this.trunk();

            case TreeType.CUTTED:
                return this.cutted();

            case TreeType.BUSH:
                return this.bush();

            default:
                throw new Error("Invalid Tree Type");
        }
    }

    private static normal(): ThematicObject {
        const { h, w } = this.scale();
        const sphereRadius: number = w * halfFactor;
        const cylinderRadius: number = w / widthDivider;
        const cylinderHeight: number = h - h / heightDivider;

        const sphereYOffset: number = (h * halfFactor) - (sphereRadius * halfFactor);
        const cylinderYOffset: number = (h * halfFactor) - (cylinderHeight * halfFactor);

        const sphere: ThematicGeometry = ThematicGeometry.sphere(sphereRadius, new Point3D(0, sphereYOffset, 0), this.randomLeafColor());
        const cylinder: ThematicGeometry = ThematicGeometry.cylinder(
            cylinderRadius, cylinderHeight, new Point3D(0, cylinderYOffset, 0), this.randomTrunkColor());

        const tree: ThematicObject = new ThematicObject();
        tree.addGeometry(sphere);
        tree.addGeometry(cylinder);

        return tree;
    }

    private static fir(): ThematicObject {
        const { h, w } = this.scale();
        const coneRadius: number = w * halfFactor;
        const coneHeight: number = h - h / widthDivider;
        const cylinderRadius: number = w / widthDivider;
        const cylinderHeight: number = h - h / heightDivider;

        const coneYOffset: number = (h * halfFactor) - (coneRadius * halfFactor);
        const cylinderYOffset: number = (h * halfFactor) - (cylinderHeight * halfFactor);

        const cone: ThematicGeometry = ThematicGeometry.cone(
            coneRadius, coneHeight , new Point3D(0, coneYOffset, 0), this.randomLeafColor());
        const cylinder: ThematicGeometry = ThematicGeometry.cylinder(
            cylinderRadius, cylinderHeight, new Point3D(0, cylinderYOffset, 0), this.randomTrunkColor());

        const tree: ThematicObject = new ThematicObject();
        tree.addGeometry(cone);
        tree.addGeometry(cylinder);

        return tree;
    }

    private static trunk(): ThematicObject {
        const { h, w } = this.scale();
        const cylinderRadius: number = w / widthDivider;
        const cylinderHeight: number = h - h / heightDivider;

        const cylinder: ThematicGeometry = ThematicGeometry.cylinder(
            cylinderRadius, cylinderHeight, new Point3D(0, 0, 0), this.randomTrunkColor());

        const tree: ThematicObject = new ThematicObject();
        tree.addGeometry(cylinder);

        return tree;
    }

    private static cutted(): ThematicObject {
        const { h, w } = this.scale();
        const cubeRadius: number = w * halfFactor;
        const cylinderRadius: number = w / widthDivider;
        const cylinderHeight: number = h - h / heightDivider;

        const cubeYOffset: number = (h * halfFactor) - (cubeRadius * halfFactor);
        const cylinderYOffset: number = (h * halfFactor) - (cylinderHeight * halfFactor);

        const cube: ThematicGeometry = ThematicGeometry.cube(cubeRadius, new Point3D(0, cubeYOffset, 0), this.randomLeafColor());
        const cylinder: ThematicGeometry = ThematicGeometry.cylinder(
            cylinderRadius, cylinderHeight, new Point3D(0, cylinderYOffset, 0), this.randomTrunkColor());

        const tree: ThematicObject = new ThematicObject();
        tree.addGeometry(cube);
        tree.addGeometry(cylinder);

        return tree;
    }

    private static bush(): ThematicObject {
        const { h, w } = this.scale();
        const cubeRadius: number = w * halfFactor;

        const cubeYOffset: number = (h * halfFactor) - (cubeRadius * halfFactor);

        const cube: ThematicGeometry = ThematicGeometry.cube(cubeRadius, new Point3D(0, cubeYOffset, 0), this.randomLeafColor());

        const tree: ThematicObject = new ThematicObject();
        tree.addGeometry(cube);

        return tree;
    }

    private static scale(): {w: number, h: number} {
        const min: number = 50;
        const max: number = 150;
        const divider: number = 100;
        const scaleFactor: number = Utils.rand(min, max) / divider;

        return {w: TREE_WIDTH * scaleFactor, h: TREE_HEIGHT * scaleFactor};
    }

    private static randomLeafColor(): number {
        return LEAF_COLORS[Utils.rand(0, LEAF_COLORS.length - 1)];
    }

    private static randomTrunkColor(): number {
        return TRUNK_COLORS[Utils.rand(0, TRUNK_COLORS.length - 1)];
    }
}
