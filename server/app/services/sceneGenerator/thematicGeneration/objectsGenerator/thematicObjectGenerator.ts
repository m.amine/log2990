import { FloorType } from "../floor";
import { ThematicObject } from "../thematicObject";
import { TreeGenerator } from "./treeGenerator";

export class ThematicObjectGenerator {
    public static randomObject(floorType: FloorType): ThematicObject {
        switch (floorType) {
            case FloorType.FOREST:
                return TreeGenerator.randomTree();

            default:
                throw new Error("Invalid floor type");
        }
    }
}
