import { SceneModification } from "../../../../../common/models/iScene";
import { Utils } from "../../../utils/utils";
import { GeometricObjectGenerator } from "../geometricGeneration/geometricObjectGenerator";
import { Floor } from "./floor";
import { ThematicObject } from "./thematicObject";
import { ThematicScene } from "./thematicScene";

export class ThematicSceneModifier {
    public readonly DIFFERENCES_COUNT: number = 7;
    private allowedModifications: SceneModification[];
    private modifiedScene: ThematicScene;
    private nonModifiedObjectsChecksum: string[];
    private modificationCount: number;
    public differencesId: string[];

    public constructor(allowedModifications: SceneModification[]) {
        this.allowedModifications = [];
        this.allowedModifications = allowedModifications;
        this.nonModifiedObjectsChecksum = [];
        this.modificationCount = 0;
        this.differencesId = [];
    }

    public modify(originalScene: ThematicScene): ThematicScene {
        this.copy(originalScene);

        const REMOVE_ALLOWED: boolean = this.allowedModifications.findIndex((modification: SceneModification) =>
            modification === SceneModification.REMOVE) !== -1;
        const CHANGE_ALLOWED: boolean = this.allowedModifications.findIndex((modification: SceneModification) =>
            modification === SceneModification.CHANGE) !== -1;

        this.modificationCount = 0;

        if ((REMOVE_ALLOWED || CHANGE_ALLOWED) && this.DIFFERENCES_COUNT > originalScene.objects.length) {
            throw new Error("There is more differences than original objects");
        } else {
            while (this.modificationCount < this.DIFFERENCES_COUNT) {
                this.addModification();
            }
        }

        this.modificationCount = 0;

        return this.modifiedScene;
    }

    private copy(originalScene: ThematicScene): void {
        this.modifiedScene = ThematicScene.copy(originalScene);
        originalScene.objects.forEach((object: ThematicObject) => {
            this.nonModifiedObjectsChecksum.push(object.checksum());
        });
    }

    private addModification(): void {
        const CURRENT_MODIFICATION: SceneModification = this.allowedModifications[Utils.rand(0, this.allowedModifications.length - 1)];

        switch (CURRENT_MODIFICATION) {
            case SceneModification.ADD:
                this.add();
                break;
            case SceneModification.REMOVE:
                this.remove();
                break;
            case SceneModification.CHANGE:
                this.change();
                break;
            default:
                throw new Error("Invalid SceneModification input");
        }

        this.modificationCount++;
    }

    private add(): void {
        const floor: Floor = this.modifiedScene.floors[Utils.rand(0, this.modifiedScene.floors.length - 1)];
        const SCENE_OBJECT: ThematicObject = this.modifiedScene.randomObject(floor);
        this.modifiedScene.objects.push(SCENE_OBJECT);
        this.differencesId.push(SCENE_OBJECT.id);
    }

    private remove(): void {
        const REMOVE_CHECKSUM_IDX: number = Utils.rand(0, this.nonModifiedObjectsChecksum.length - 1);
        const REMOVE_CHECKSUM: string = this.nonModifiedObjectsChecksum[REMOVE_CHECKSUM_IDX];

        this.modifiedScene.objects.forEach((object: ThematicObject, index: number) => {
            if (object.checksum() === REMOVE_CHECKSUM) {
                this.differencesId.push(this.modifiedScene.objects[index].id);
                this.modifiedScene.objects.splice(index, 1);
            }
        });

        this.nonModifiedObjectsChecksum.splice(REMOVE_CHECKSUM_IDX, 1);
    }

    private change(): void {
        const CHANGE_CHECKSUM_IDX: number = Utils.rand(0, this.nonModifiedObjectsChecksum.length - 1);
        const CHANGE_CHECKSUM: string = this.nonModifiedObjectsChecksum[CHANGE_CHECKSUM_IDX];
        let randomColor: number = GeometricObjectGenerator.COLORS[Utils.rand(0, GeometricObjectGenerator.COLORS.length - 1)];

        const CHANGE_IDX: number = this.modifiedScene.objects.findIndex((object: ThematicObject) => object.checksum() === CHANGE_CHECKSUM);
        const CHANGE_GEOMETRY_IDX: number = Utils.rand(0, this.modifiedScene.objects[CHANGE_IDX].geometries.length - 1);
        const CURRENT_COLOR: number = this.modifiedScene.objects[CHANGE_IDX].geometries[CHANGE_GEOMETRY_IDX].hexColor;

        while (CURRENT_COLOR === randomColor) {
            randomColor = GeometricObjectGenerator.COLORS[Utils.rand(0, GeometricObjectGenerator.COLORS.length - 1)];
        }

        this.modifiedScene.objects[CHANGE_IDX].geometries[CHANGE_GEOMETRY_IDX].hexColor = randomColor;
        this.differencesId.push(this.modifiedScene.objects[CHANGE_IDX].id);
        this.nonModifiedObjectsChecksum.splice(CHANGE_CHECKSUM_IDX, 1);
    }
}
