import { Point3D } from "./point3D";

export class BoundingBox {
    public position: Point3D;
    public dimension: Point3D;

    public constructor (position: Point3D, dimension: Point3D) {
        this.position = position;
        this.dimension = dimension;
    }

    public isColliding(other: BoundingBox): boolean {
        // Collision in x axis
        let colliding: boolean = this.areCollidingOnAxis(this.position.x, other.position.x, this.dimension.x, other.dimension.x);

        // Collision in y axis
        colliding = colliding && this.areCollidingOnAxis(this.position.y, other.position.y, this.dimension.y, other.dimension.y);

        // Collision in z axis
        colliding = colliding && this.areCollidingOnAxis(this.position.z, other.position.z, this.dimension.z, other.dimension.z);

        return colliding;
    }

    private areCollidingOnAxis(pos1: number, pos2: number, measure1: number, measure2: number): boolean {
        const posL: number = (pos1 > pos2) ? pos2 : pos1;
        const posR: number = (pos1 > pos2) ? pos1 : pos2;
        const measureL: number = (pos1 > pos2) ? measure2 : measure1;
        const measureR: number = (pos1 > pos2) ? measure1 : measure2;

        return (posL + measureL) >= (posR - measureR);
    }
}
