import * as fs from "fs";
import { Utils } from "./utils";

export class PNG {
    private readonly EXT: string = ".png";
    private readonly BASE_IMAGE_URL: string = "http://localhost:3000/image/";
    private buffer: ArrayBuffer;

    public constructor (buffer: ArrayBuffer) {
        this.buffer = buffer;
    }

    public save(): string {
        const subPath: string = "res/images/";
        let path: string;
        let uid: string;

        do {
            uid = "img_" + Utils.uniqueId();
            path = Utils.rootPath() + subPath + uid + this.EXT;
        } while (fs.existsSync(path));

        fs.writeFileSync(path, Buffer.from(this.buffer));

        return this.BASE_IMAGE_URL + uid + this.EXT;
    }
}
