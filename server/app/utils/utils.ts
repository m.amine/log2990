import * as fs from "fs";
import { Record } from "../../../common/models/record";

export class Utils {

    // tslint:disable-next-line:no-any
    public static parseBufferObject(obj: any): ArrayBuffer {
        const oKeys: string[] = Object.keys(obj);
        const buf: ArrayBuffer = new ArrayBuffer(oKeys.length);
        const odvw: DataView = new DataView(buf);
        oKeys.forEach((key: string, i: number) => odvw.setUint8(i, obj[key]));

        return buf;
    }

    public static swapEndian16(val: number): number {
        const swapEndianesConst: number = 0xFF;
        const octetSize: number = 8;

        return ((val & swapEndianesConst) << octetSize) | ((val >> octetSize) & swapEndianesConst);
    }

    public static swapEndian32(val: number): number {
        const swapEndianesConst: number = 0xFF;
        const octetSize: number = 8;
        const swapEndianesConstSecond: number = 0xFF00;
        const octetSizeSecond: number = 24;

        return ((val & swapEndianesConst) << octetSizeSecond) | ((val & swapEndianesConstSecond) << octetSize) |
             ((val >> octetSize) & swapEndianesConstSecond) | ((val >> octetSizeSecond) & swapEndianesConst);
    }

    public static rootPath(): string {
        const rootEnding: string = "server";
        const root: string = __dirname;
        const removeIdx: number = root.search(rootEnding) + rootEnding.length;

        return root.slice(0, removeIdx) + "/";
    }

    public static rand(min: number, max: number): number {
        let negativeFactor: number = 1;
        let adjustedMax: number = max;
        let adjustedMin: number = min;

        if (max < 0 && min < 0) {
            negativeFactor = -1;
            adjustedMin--;
        } else {
            adjustedMax++;
        }

        adjustedMax *= negativeFactor;
        adjustedMin *= negativeFactor;

        return negativeFactor * Math.floor((Math.random() * (adjustedMax - adjustedMin)) + adjustedMin);
    }

    public static generateRandomRecords(numberOfRecords: number): Record[] {
        const records: Record[] = [];
        const times: number[] = [];

        const seconds: number = 60;
        const minutesMin: number = 5;
        const minutesMax: number = 10;
        const min: number = minutesMin * seconds;
        const max: number = minutesMax * seconds;

        for (let i: number = 0; i < numberOfRecords; i++) {
            times.push(Utils.rand(min, max));
        }

        times.sort();
        times.forEach((time: number) => records.push({time: time, holder: "System"}));

        return records;
    }

    public static uniqueId(): string {
        const fullStringSize: number = 36;
        const startOffset: number = 2;
        const newStringSize: number = 16;

        return Math.random().toString(fullStringSize).substr(startOffset, newStringSize) +
        Math.random().toString(fullStringSize).substr(startOffset, newStringSize);
    }

    public static removeImageUrl(url: string): void {
        const filename: string = url.split("/").pop() as string;
        fs.unlinkSync(Utils.rootPath() + "res/images/" + filename);
    }

    public static getRandomFactors(integer: number): [number, number] {
        const factors: number[] = [];
        let quotient: number = 0;

        for (let i: number = 1; i <= integer; i++) {
          quotient = integer / i;

          if (quotient === Math.floor(quotient)) {
            factors.push(i);
          }
        }

        const minFactorSize: number = 2;
        const min: number = (factors.length > minFactorSize) ? 1 : 0;
        const max: number = (factors.length > minFactorSize) ? factors.length - minFactorSize : factors.length - 1;
        const firstIndex: number = this.rand(min, max);
        const secondIndex: number = factors.length - 1 - firstIndex;

        return [factors[firstIndex], factors[secondIndex]];
      }
}
