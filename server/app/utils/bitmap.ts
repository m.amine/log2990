import * as fs from "fs";
import { ImageProcessingStatus as IPStatus } from "../../../common/communication/errors";
import { Pixel } from "../../../common/models/pixel";
import { Point } from "../../../common/models/point";
import { Utils } from "./utils";

class Header {
    public bfType: number;
    public bfSize: number;
    public bfReserved1: number;
    public bfReserved2: number;
    public bfOffBits: number;
    public biSize: number;
    public biWidth: number;
    public biHeight: number;
    public biPlanes: number;
    public biBitCount: number;
    public biCompression: number;
    public biSizeImage: number;
    public biXPelsPerMeter: number;
    public biYPelsPerMeter: number;
    public biClrUsed: number;
    public biClrImportant: number;

    public readonly posbfType: number = 0;
    public readonly posbfSize: number = 2;
    public readonly posbfReserved1: number = 6;
    public readonly posbfReserved2: number = 8;
    public readonly posbfOffBits: number = 10;
    public readonly posbiSize: number = 14;
    public readonly posbiWidth: number = 18;
    public readonly posbiHeight: number = 22;
    public readonly posbiPlanes: number = 26;
    public readonly posbiBitCount: number = 28;
    public readonly posbiCompression: number = 30;
    public readonly posbiSizeImage: number = 34;
    public readonly posbiXPelsPerMeter: number = 38;
    public readonly posbiYPelsPerMeter: number = 42;
    public readonly posbiClrUsed: number = 46;
    public readonly posbiClrImportant: number = 50;
}

export class Bitmap {

    private header: Header = new Header();
    private stride: number;
    private pixels: Uint8Array;
    private buffer: ArrayBuffer;
    private readonly HEADER_LENGTH: number = 54;
    private readonly EXT: string = ".bmp";
    private readonly TYPE: number = 0x4D42;
    private readonly PIXEL_SIZE: number = 3;
    private readonly PIXEL_G_POS: number = 1;
    private readonly PIXEL_R_POS: number = 2;
    private readonly BASE_IMAGE_URL: string = "http://localhost:3000/image/";

    public fromPath(path: string): IPStatus {
        const buf: Buffer = fs.readFileSync(path);
        const aBuf: ArrayBuffer = buf.buffer.slice(buf.byteOffset, buf.byteOffset + buf.byteLength);

        return this.read(aBuf);
    }

    public read(buffer: ArrayBuffer): IPStatus {
        const dataView: DataView = new DataView(buffer);
        this.readFileHeader(dataView);

        if (!this.isBmp()) {
            return IPStatus.E_INVALID_IMAGE_TYPE;
        }

        this.readInfoHeader(dataView);
        const alphaBits: number = 31;
        const dwordBits: number = 32;
        const  multipleFourBytes: number = 4;
        this.stride = Math.floor((this.header.biBitCount * this.header.biWidth + alphaBits ) / dwordBits) * multipleFourBytes;
        this.pixels = new Uint8Array(buffer, this.header.bfOffBits);

        return IPStatus.S_OK;
    }

    public shallowCopy(bmp: Bitmap): void {
        this.header = bmp.header;
        this.stride = bmp.stride;
        this.pixels = new Uint8Array(bmp.pixels.length);
    }

    public toBuffer(): ArrayBuffer {
        this.buffer = new ArrayBuffer(this.HEADER_LENGTH + this.pixels.length);
        this.writeFileHeader();
        this.writeInfoHeader();
        this.writePixels();

        return this.buffer;
    }

    public toDataURL(): string {
        let uri: string = "data:image/bmp;base64,";
        uri += Buffer.from(this.toBuffer()).toString("base64");

        return uri;
    }

    public save(): string {
        const subPath: string = "res/images/";
        let path: string;
        let uid: string;

        do {
            uid = "img_" + Utils.uniqueId();
            path = Utils.rootPath() + subPath + uid + this.EXT;
        } while (fs.existsSync(path));

        fs.writeFileSync(path, Buffer.from(this.toBuffer()));

        return this.BASE_IMAGE_URL + uid + this.EXT;
    }

    public isBmp(): boolean {
        return this.header.bfType === this.TYPE;
    }

    public width(): number {
        return this.header.biWidth;
    }

    public height(): number {
        return this.header.biHeight;
    }

    public pixel(x: number, y: number): Pixel {
        const idx: number = x * this.PIXEL_SIZE + this.stride * (this.height() - 1 - y);
        const b: number = this.pixels[idx];
        const g: number = this.pixels[idx + this.PIXEL_G_POS];
        const r: number = this.pixels[idx + this.PIXEL_R_POS];

        return new Pixel(r, g, b);
    }

    public setPixel(x: number, y: number, pix: Pixel): void {
        const idx: number = x * this.PIXEL_SIZE + this.stride * (this.height() - 1 - y);
        this.pixels[idx] = pix.b;
        this.pixels[idx + this.PIXEL_G_POS] = pix.g;
        this.pixels[idx + this.PIXEL_R_POS] = pix.r;
    }

    public getDifferentPixels(other: Bitmap): Point[] {
        const diffPoints: Point[] = [];

        for (let i: number = 0; i < this.width(); i++) {
            for (let j: number = 0; j < this.height(); j++) {
                if (!this.pixel(i, j).equals(other.pixel(i, j))) {
                    diffPoints.push(new Point(i, j));
                }
            }
        }

        return diffPoints;
    }

    private readFileHeader(dataView: DataView): void {
        this.header.bfType = dataView.getUint16(this.header.posbfType, true);
        this.header.bfSize = dataView.getUint32(this.header.posbfSize, true);
        this.header.bfReserved1 = dataView.getUint16(this.header.posbfReserved1, true);
        this.header.bfReserved2 = dataView.getUint16(this.header.posbfReserved2, true);
        this.header.bfOffBits = dataView.getUint32(this.header.posbfOffBits, true);
    }

    private readInfoHeader(dataView: DataView): void {
        this.header.biSize = dataView.getUint32(this.header.posbiSize, true);
        this.header.biWidth = dataView.getUint32(this.header.posbiWidth, true);
        this.header.biHeight = dataView.getUint32(this.header.posbiHeight, true);
        this.header.biPlanes = dataView.getUint16(this.header.posbiPlanes, true);
        this.header.biBitCount = dataView.getUint16(this.header.posbiBitCount, true);
        this.header.biCompression = dataView.getUint32(this.header.posbiCompression, true);
        this.header.biSizeImage = dataView.getUint32(this.header.posbiSizeImage, true);
        this.header.biXPelsPerMeter = dataView.getUint32(this.header.posbiXPelsPerMeter, true);
        this.header.biYPelsPerMeter = dataView.getUint32(this.header.posbiYPelsPerMeter, true);
        this.header.biClrUsed = dataView.getUint32(this.header.posbiClrUsed, true);
        this.header.biClrImportant = dataView.getUint32(this.header.posbiClrImportant, true);
    }

    private writeFileHeader(): void {
        const dataView: DataView = new DataView(this.buffer);
        dataView.setUint16(this.header.posbfType, Utils.swapEndian16(this.header.bfType));
        dataView.setUint32(this.header.posbfSize, Utils.swapEndian32(this.header.bfSize));
        dataView.setUint16(this.header.posbfReserved1, Utils.swapEndian16(this.header.bfReserved1));
        dataView.setUint16(this.header.posbfReserved2, Utils.swapEndian16(this.header.bfReserved2));
        dataView.setUint32(this.header.posbfOffBits, Utils.swapEndian32(this.header.bfOffBits));
    }

    private writeInfoHeader(): void {
        const dataView: DataView = new DataView(this.buffer);
        dataView.setUint32(this.header.posbiSize, Utils.swapEndian32(this.header.biSize));
        dataView.setUint32(this.header.posbiWidth, Utils.swapEndian32(this.header.biWidth));
        dataView.setUint32(this.header.posbiHeight, Utils.swapEndian32(this.header.biHeight));
        dataView.setUint16(this.header.posbiPlanes, Utils.swapEndian16(this.header.biPlanes));
        dataView.setUint16(this.header.posbiBitCount, Utils.swapEndian16(this.header.biBitCount));
        dataView.setUint32(this.header.posbiCompression, Utils.swapEndian32(this.header.biCompression));
        dataView.setUint32(this.header.posbiSizeImage, Utils.swapEndian32(this.header.biSizeImage));
        dataView.setUint32(this.header.posbiXPelsPerMeter, Utils.swapEndian32(this.header.biXPelsPerMeter));
        dataView.setUint32(this.header.posbiYPelsPerMeter, Utils.swapEndian32(this.header.biYPelsPerMeter));
        dataView.setUint32(this.header.posbiClrUsed, Utils.swapEndian32(this.header.biClrUsed));
        dataView.setUint32(this.header.posbiClrImportant, Utils.swapEndian32(this.header.biClrImportant));
    }

    private writePixels(): void {
        const dataView: DataView = new DataView(this.buffer);
        this.pixels.forEach((pix: number, i: number) => dataView.setUint8(this.HEADER_LENGTH + i, pix));
    }

}
