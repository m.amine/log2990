import { IPoint3D } from "../../../common/models/iPoint3D";

export class Point3D implements IPoint3D {
    public x: number;
    public y: number;
    public z: number;

    public constructor(x: number, y: number, z: number) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

}
