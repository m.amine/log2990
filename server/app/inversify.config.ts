import { Container } from "inversify";
import { Application } from "./app";
import { Routes } from "./routes";
import { DifferenceValidator3DRoute } from "./routes/differenceValidator3D.routes";
import { GameRoute } from "./routes/game.routes";
import { ImageProcessingRoute } from "./routes/imageProcessing.routes";
import { SceneGeneratorRoute } from "./routes/sceneGenerator.routes";
import { UserRoute } from "./routes/user.routes";
import { Server } from "./server";
import Types from "./types";

const container: Container = new Container();

container.bind(Types.Server).to(Server);
container.bind(Types.Application).to(Application);
container.bind(Types.Routes).to(Routes);
container.bind(Types.Game).to(GameRoute);
container.bind(Types.ImageProcessing).to(ImageProcessingRoute);
container.bind(Types.SceneGenerator).to(SceneGeneratorRoute);
container.bind(Types.User).to(UserRoute);
container.bind(Types.DifferenceValidator3D).to(DifferenceValidator3DRoute);

export { container };
