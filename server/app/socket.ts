import { Room } from "../../common/communication/room";
import { FreeGame } from "../../common/models/freeGame";
import { Point } from "../../common/models/point";
import { SimpleGame } from "../../common/models/simpleGame";
import { LoginSocketHandler } from "./socketHandlers/login.socketHandler";

export const GET_ALL_ROOMS: string = "get-all-available-rooms";
const MAX_USERS: number = 2;
const DELETE_COUNT: number = 1;

export class Socket {

    private io: SocketIO.Server;
    private loginSocketHandler: LoginSocketHandler;
    private rooms: Room[];
    private uniqueRoomId: number;

    public constructor(public socketServer: SocketIO.Server) {
        this.io = socketServer;
        this.loginSocketHandler = new LoginSocketHandler;
        this.rooms = new Array<Room>();
        this.uniqueRoomId = 0;

    }

    private events(): void {
        this.io.on("connection", (socket: SocketIO.Socket) => {
            this.loginSocketHandler.handle(socket);
            this.differenceConfiguration(socket);
            this.bestScoreConfiguration(socket);
            this.joinSimpleRoom(socket);
            this.joinFreeRoom(socket);
            this.createRoom(socket);
            this.getSimpleGamesAction(socket);
            this.getFreeGamesAction(socket);
            this.deleteGameNotification(socket);
        });
    }

    public socketConfiguration(): void {
        this.events();
    }

    private differenceConfiguration(socket: SocketIO.Socket): void {
        /*difference identification  and error identification socket */
        socket.on("find difference in solo game", (username: string) => {
            socket.emit("find difference in solo game", username);
        });
        socket.on("find difference in multiPlayer game", (username: string, roomId: string) => {
            this.isFirstUser(username, roomId) ? this.incrementFirstCounter(roomId, username) :
            this.incrementSecondCounter(roomId, username);
            this.io.to(roomId).emit("find difference in multiPlayer game", username);
        });
        socket.on("error identification in solo game", (username: string) => {
            socket.emit("error identification in solo game", username);
        });
        socket.on("error identification in multiPlayer game", (username: string, roomId: string) => {
            this.io.to(roomId).emit("error identification in multiPlayer game", username);
        });
        socket.on("updateScene", (game: FreeGame , roomId: string ) => {
            this.io.to(roomId).emit("updateScene", game);

        });
        socket.on("updateImage", (point: [Point, Point], roomId: string) => {
            this.io.to(roomId).emit("findPoints" , point);
        });
    }

    private bestScoreConfiguration(socket: SocketIO.Socket): void {

        socket.on("found best score", (username: string, position: string,
                                       gamesName: string, totalOfPlayers: string) => {
            socket.broadcast.emit("best score", username, position, gamesName, totalOfPlayers);
        });

    }
    private createRoom(socket: SocketIO.Socket): void {
        /* Socket for creation and delete of a room */
        socket.on("createRoom", (room: Room) => {
            room.id = this.getUniqueRoomId().toString();
            socket.leaveAll();
            socket.join(room.id);
            this.rooms.push(room);
            socket.emit("createdRoom", room.id);
        });

        socket.on("leaveRoom", (room: Room) => {
            socket.leave(room.id);
            const existingRoom: Room | undefined = this.getRoomByName(room);
            if (existingRoom) { this.roomDeleted(room); }
            this.io.emit("roomEmpty " + room.id, room);
        });
    }

    private joinSimpleRoom(socket: SocketIO.Socket): void {
        /* Socket for joining a room */
        socket.on("joinSimpleRoom", (room: Room) => {
            const availableRoom: Room | undefined = this.getAvailableRoom(room);
            if (availableRoom) {
                availableRoom.users.push(room.users[0]);
                socket.join(availableRoom.id);
                socket.emit("joined", availableRoom.id);
                this.io.to(availableRoom.id).emit("startSimpleGame");
                this.io.to(availableRoom.id).emit("second counter incremented", room.users[1]);
               }
        });
    }
    private joinFreeRoom(socket: SocketIO.Socket): void {
        /* Socket for joining a room */
        socket.on("joinFreeRoom", (room: Room) => {
            const availableRoom: Room | undefined = this.getAvailableRoom(room);
            if (availableRoom) {
                availableRoom.users.push(room.users[0]);
                socket.leaveAll();
                socket.join(availableRoom.id);
                socket.emit("joined", availableRoom.id);
                this.io.to(availableRoom.id).emit("startFreeGame");
                this.io.to(availableRoom.id).emit("second counter incremented", room.users[1]);
                }
        });
    }

    private incrementFirstCounter(roomid: string, username: string): void {
        this.io.to((this.getRoomById(roomid) as Room).id).emit("incrementFirstCounter", username);
    }

    private incrementSecondCounter(roomid: string , username: string): void {
        this.io.to((this.getRoomById(roomid) as Room).id).emit("incrementSecondCounter", username);
    }

    private getSimpleGamesAction(socket: SocketIO.Socket): void {
        /* Socket for joining a room */
        socket.on("getSimpleGamesAction", (games: SimpleGame[]) => {
            for (const game of games) {
                const availableRoom: Room | undefined = this.getGameAvailability(game.name as string);
                availableRoom ? game.action = "Joindre" : game.action = "Créer";
            }
            this.io.emit("getSimpleGamesAction", games);
        });
    }
    private getFreeGamesAction(socket: SocketIO.Socket): void {
        /* Socket for joining a room */
        socket.on("getFreeGamesAction", (games: FreeGame[]) => {
            for (const game of games) {
                const availableRoom: Room | undefined = this.getGameAvailability(game.name as string);
                availableRoom ? game.action = "Joindre" : game.action = "Créer";
            }
            this.io.emit("getFreeGamesAction", games);
        });
    }
    private deleteGameNotification(socket: SocketIO.Socket): void {
        socket.on("deleteGame", (id: string) => {
            this.io.emit("deleteGame", id);
        });
    }

    private getRoomByName(room: Room): Room | undefined {
        return this.rooms.find((x: Room) => x.name === room.name);
    }

    private getRoomById(roomId: string): Room | undefined {
        return this.rooms.find((x: Room) => x.id === roomId);
    }

    private getAvailableRoom(room: Room): Room | undefined {
        return this.rooms.find((x: Room) => x.name === room.name && x.users.length < MAX_USERS);
    }

    private getGameAvailability(roomName: string): Room | undefined {
        return this.rooms.find((x: Room) => x.name === roomName && x.users.length < MAX_USERS);
    }

    private roomDeleted(room: Room): void {
        this.rooms.splice(this.rooms.findIndex((x: Room) => x.id === room.id), DELETE_COUNT);
    }

    private getUniqueRoomId(): number {
        return this.uniqueRoomId++;
    }

    private isFirstUser(userName: string, roomId: string): boolean {
        return (this.getRoomById(roomId) as Room).users[0] === userName;
    }
}
