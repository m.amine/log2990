export default  {
        Server: Symbol("Server"),
        Application: Symbol("Application"),
        Routes: Symbol("Routes"),
        Index: Symbol("Index"),
        User: Symbol("User"),
        Game: Symbol("Game"),
        ImageProcessing: Symbol("ImageProcessing"),
        SceneGenerator: Symbol("SceneGenerator"),
        DifferenceValidator3D : Symbol("DifferenceValidator3D"),
};
