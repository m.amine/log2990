import { NextFunction, Request, Response, Router } from "express";
import { inject, injectable } from "inversify";

import { DifferenceValidator3DRoute } from "../app/routes/differenceValidator3D.routes";
import { GameRoute } from "./routes/game.routes";
import { ImageProcessingRoute } from "./routes/imageProcessing.routes";
import { SceneGeneratorRoute } from "./routes/sceneGenerator.routes";
import { UserRoute } from "./routes/user.routes";
import Types from "./types";

@injectable()
export class Routes {

    public constructor( @inject(Types.Game) private gameRoute: GameRoute,
                        @inject(Types.ImageProcessing) private imageProcessingRoute: ImageProcessingRoute,
                        @inject(Types.SceneGenerator) private sceneGeneratorRoute: SceneGeneratorRoute,
                        @inject(Types.User) private userRoute: UserRoute,
                        @inject(Types.DifferenceValidator3D) private differenceValidator3DRoute: DifferenceValidator3DRoute ) {}

    public get gameRoutes(): Router {
        const gameRouter: Router = Router();
        gameRouter.get(
            "/simpleGame",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.getSimpleGames(req, res, next));

        gameRouter.get(
            "/simpleGame/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.findSimpleGame(req, res, next));

        gameRouter.delete(
            "/simpleGame/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.deleteSimpleGame(req, res, next));

        gameRouter.patch(
            "/simpleGame/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.resetSimpleGame(req, res, next));

        gameRouter.put(
            "/simpleGame/updateSoloRecord/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.updateRecordInSimpleViewSoloGame(req, res, next));

        gameRouter.put(
            "/simpleGame/getPositionOfSoloRecord/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.getRecordPositionInSimpleViewSoloGame(req, res, next));

        gameRouter.put(
            "/simpleGame/updateMultiRecord/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.updateRecordInSimpleViewMultiGame(req, res, next));

        gameRouter.put(
            "/simpleGame/getPositionOfMultiRecord/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.getRecordPositionInSimpleViewMultiGame(req, res, next));

        gameRouter.put(
            "/freeGame/updateSoloRecord/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.updateRecordInFreeViewSoloGame(req, res, next));

        gameRouter.put(
            "/freeGame/getPositionOfSoloRecord/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.getRecordPositionInFreeViewSoloGame(req, res, next));

        gameRouter.put(
            "/freeGame/updateMultiRecord/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.updateRecordInFreeViewMultiGame(req, res, next));

        gameRouter.put(
            "/freeGame/getPositionOfMultiRecord/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.getRecordPositionInFreeViewMultiGame(req, res, next));

        gameRouter.get(
            "/freeGame/getAll",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.getFreeGames(req, res, next));

        gameRouter.get(
            "/freeGame/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.findFreeGame(req, res, next));

        gameRouter.post(
            "/freeGame/create",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.createFreeGame(req, res, next));

        gameRouter.delete(
            "/freeGame/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.deleteFreeGame(req, res, next));

        gameRouter.patch(
            "/freeGame/:id",
            (req: Request, res: Response, next: NextFunction) => this.gameRoute.resetFreeGame(req, res, next));

        return gameRouter;
    }

    public get imageProcessingRoutes(): Router {
        const imageProcessingRouter: Router = Router();

        imageProcessingRouter.post(
            "/imageProcessing/generateDifferences",
            (req: Request, res: Response, next: NextFunction) => this.imageProcessingRoute.generateDifferences(req, res, next));

        imageProcessingRouter.post(
            "/imageProcessing/detectDifference",
            (req: Request, res: Response, next: NextFunction) => this.imageProcessingRoute.detectDifference(req, res, next));

        imageProcessingRouter.get(
            "/image/:filename",
            (req: Request, res: Response, next: NextFunction) => this.imageProcessingRoute.getImage(req, res, next));

        return imageProcessingRouter;
    }

    public get sceneGeneratorRoutes(): Router {
        const sceneGeneratorRouter: Router = Router();

        sceneGeneratorRouter.post(
            "/sceneGenerator/generate",
            (req: Request, res: Response, next: NextFunction) => this.sceneGeneratorRoute.generateScenes(req, res, next));

        return sceneGeneratorRouter;
    }

    public get userRoutes(): Router {
        const userRouter: Router = Router();

        userRouter.get(
            "/logout/:name",
            (req: Request, res: Response, next: NextFunction) => this.userRoute.logout(req, res, next));

        userRouter.get(
            "/login/:name",
            (req: Request, res: Response, next: NextFunction) => this.userRoute.login(req, res, next));

        return userRouter;
    }

    public get differenceValidator3D(): Router {
        const differenceValidatorRouter: Router = Router();

        differenceValidatorRouter.post(
            "/differenceValidator3D/detectDifferences3D",
            (req: Request, res: Response, next: NextFunction) => this.differenceValidator3DRoute.detectDifferences3D(req, res, next));

        return differenceValidatorRouter;

    }
}
