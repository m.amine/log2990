import { model, Model, Schema } from "mongoose";
import { IFreeGame } from "./IFreeGame";

export const freeGameSchema: Schema = new Schema(
    {
        gameId: String,
        name: String,
        thumbnail: String,
        originalScene: Object,
        modifiedScene: Object,
        differencesId: [],
        soloTime: [],
        vsTime: [],
    },
    { timestamps: true });

export const FREEGAME: Model<IFreeGame> = model<IFreeGame>("FreeGame", freeGameSchema);
