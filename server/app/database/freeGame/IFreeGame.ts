import { Document } from "mongoose";
import { FreeGame } from "../../../../common/models/freeGame";
export interface IFreeGame extends FreeGame, Document {

}
