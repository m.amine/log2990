import { IUser } from "../user/IUser";
import { USER } from "../user/user.model";

export class UserDB {

    public async get(name: String): Promise<IUser | Error> {
        return new Promise<IUser | Error>((
            resolve: (value?: IUser | PromiseLike<IUser>) => void,
            reject: (reason?: Error) => void) => {
                USER.findOne({name: name}, (e: Error, games: IUser) => {
                    if (e)  { reject(e); }
                    resolve(games);
                });
        });
    }

    public async create(name: String): Promise<IUser | Error> {
        return new Promise<IUser | Error>((
            resolve: (value?: IUser | PromiseLike<IUser>) => void,
            reject: (reason?: Error) => void) => {

                const user: IUser = new USER({
                    isConnected: true,
                    name: name,
                });

                user.save((e: Error, savedUser: IUser) => {
                    if (e) { reject(e); }
                    resolve(savedUser);
                });

        });
    }

    public async update(name: String, status: boolean): Promise<IUser | Error> {
        return new Promise<IUser | Error>((
            resolve: (value?: IUser | PromiseLike<IUser>) => void,
            reject: (reason?: Error) => void) => {

                const update: Object = { $set: {
                    isConnected: status,
                }};

                USER.updateOne({name: name}, update, (e: Error) => {
                    if (e) {
                        reject(e);
                    }
                    resolve();
                });
        });
    }
}
