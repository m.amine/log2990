// tslint:disable-next-line:import-blacklist
import { Observable, Subscriber } from "rxjs";
import { Record } from "../../../../common/models/record";
import { BestGameRecord } from "../../services/bestGameRecord.service";
import { Utils } from "../../utils/utils";
import { ISimpleGame } from "../simpleGame/ISimpleGame";
import { SIMPLEGAME } from "../simpleGame/simpleGame.model";

export class SimpleGameDB {

    public async getAll(): Promise<ISimpleGame[] | Error> {
        return new Promise<ISimpleGame[] | Error>((
            resolve: (value?: ISimpleGame[] | PromiseLike<ISimpleGame[]>) => void,
            reject: (reason?: Error) => void) => {
            SIMPLEGAME.find((e: Error, games: ISimpleGame[]) => {
                if (e) { reject(e); }
                resolve(games);
            });
        });
    }

    public async create(name: String, oImg: String, mImg: String, dImg: String): Promise<ISimpleGame | Error> {
        return new Promise<ISimpleGame | Error>((
            resolve: (value?: ISimpleGame | PromiseLike<ISimpleGame>) => void,
            reject: (reason?: Error) => void) => {
            const game: ISimpleGame = this.newGame(name, oImg, mImg, dImg);
            game.save((e: Error, savedGame: ISimpleGame) => {
                if (e) { reject(e); }
                resolve(savedGame);
            });
        });
    }

    public async findGameWithId(id: String): Promise<ISimpleGame | Error> {
        return new Promise<ISimpleGame | Error>((
            resolve: (value?: ISimpleGame | PromiseLike<ISimpleGame>) => void,
            reject: (reason?: Error) => void) => {
            SIMPLEGAME.findOne({ gameId: id }, (e: Error, gameFound: ISimpleGame) => {
                if (e) {
                    reject(e);
                }
                resolve(gameFound);
            });
        });
    }

    public async delete(id: String): Promise<void | Error> {
        return new Promise<void | Error>((
            resolve: (value?: void | PromiseLike<void>) => void,
            reject: (reason?: Error) => void) => {
            SIMPLEGAME.findOneAndDelete({ gameId: id }, (e: Error, deletedGame: ISimpleGame) => {
                if (e) {
                    reject(e);
                }
                try {
                    Utils.removeImageUrl(deletedGame.originalImage.toString());
                    Utils.removeImageUrl(deletedGame.modifiedImage.toString());
                    Utils.removeImageUrl(deletedGame.differencesImage.toString());
                } catch (e) {
                    reject(e);
                }
                resolve();
            });
        });
    }

    public async reset(id: String): Promise<void | Error> {
        return new Promise<void | Error>((
            resolve: (value?: void | PromiseLike<void>) => void,
            reject: (reason?: Error) => void) => {
            const n: number = 3;
            const sTime: Record[] = Utils.generateRandomRecords(n);
            const vsTime: Record[] = Utils.generateRandomRecords(n);

            const update: Object = {
                $set: {
                    soloTime: sTime,
                    vsTime: vsTime,
                },
            };

            SIMPLEGAME.updateOne({ gameId: id }, update, (e: Error) => {
                if (e) {
                    reject(e);
                }
                resolve();
            });
        });
    }

    public updateSoloGameRecord(id: string, userName: string, newScore: number): Observable<boolean> {

        return new Observable<boolean>((observer: Subscriber<boolean>) => {
            this.findGameWithId(id).then((gamefound: ISimpleGame) => {
                        const sTime: Record[] = BestGameRecord.addRecordInGame(gamefound.soloTime, userName, newScore);
                        const update: Object = {
                            $set: {
                                soloTime: sTime,

                            },
                        };
                        SIMPLEGAME.updateOne({ gameId: id }, update, (e: Error) => {
                            if (e) {
                              observer.next(false);
                            }
                            observer.next(true);
                        });
                    });
        });
    }

    public findPostionInSoloGame(id: string, userName: string, newScore: number): Observable<number> {

        return new Observable<number>((observer: Subscriber<number>) => {
            this.findGameWithId(id).then((gamefound: ISimpleGame) => {
                        const index: number = BestGameRecord.getPositionOfRecord(gamefound.soloTime, newScore, userName);
                        observer.next(index);
                    });
        });
    }

    public updateMultiGameRecord(id: string, userName: string, newScore: number): Observable<boolean> {
        return new Observable<boolean>((observer: Subscriber<boolean>) => {
            this.findGameWithId(id).then((gamefound: ISimpleGame) => {
                        const sTime: Record[] = BestGameRecord.addRecordInGame(gamefound.vsTime, userName, newScore);
                        const update: Object = {
                            $set: {
                                vsTime: sTime,

                            },
                        };
                        SIMPLEGAME.updateOne({ gameId: id }, update, (e: Error) => {
                            if (e) {
                              observer.next(false);
                            }
                            observer.next(true);
                        });
                    });
        });
    }

    public findPostionInMultiGame(id: string, userName: string, newScore: number): Observable<number> {

        return new Observable<number>((observer: Subscriber<number>) => {
            this.findGameWithId(id).then((gamefound: ISimpleGame) => {
                        const index: number = BestGameRecord.getPositionOfRecord(gamefound.vsTime, newScore, userName);
                        observer.next(index);
                    });
        });
    }

    public newGame(name: String, oImg: String, mImg: String, dImg: String, highscoreDimensions: number = 3): ISimpleGame {

        const soloT: Record[] = Utils.generateRandomRecords(highscoreDimensions);
        const vsT: Record[] = Utils.generateRandomRecords(highscoreDimensions);

        return new SIMPLEGAME({
            gameId: Utils.uniqueId(),
            name: name,
            originalImage: oImg,
            modifiedImage: mImg,
            differencesImage: dImg,
            soloTime: soloT,
            vsTime: vsT,
        });
    }
}
