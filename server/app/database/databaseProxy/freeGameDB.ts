// tslint:disable-next-line:import-blacklist
import { Observable, Subscriber } from "rxjs";
import { FreeGameDBError } from "../../../../common/communication/freeGameDBError";
import { IScene } from "../../../../common/models/iScene";
import { Record } from "../../../../common/models/record";
import { BestGameRecord } from "../../services/bestGameRecord.service";
import { Utils } from "../../utils/utils";
import { IFreeGame } from "../freeGame/IFreeGame";
import { FREEGAME } from "../freeGame/freeGame.model";

export class FreeGameDB {

    public async getAll(): Promise<IFreeGame[] | Error> {
        return new Promise<IFreeGame[] | Error>((
            resolve: (value?: IFreeGame[] | PromiseLike<IFreeGame[]>) => void,
            reject: (reason?: FreeGameDBError) => void) => {
            FREEGAME.find((e: FreeGameDBError, games: IFreeGame[]) => {
                if (e) { reject(e); }
                resolve(games);
            });
        });
    }

    public async create(name: String, thumbnail: String, oScene: IScene, mScene: IScene, dId: string[]):
    Promise<IFreeGame | FreeGameDBError> {
        return new Promise<IFreeGame | FreeGameDBError>((
            resolve: (value?: IFreeGame | PromiseLike<IFreeGame>) => void,
            reject: (reason?: FreeGameDBError) => void) => {
            this.newGame(name, thumbnail, oScene, mScene, dId).save((e: FreeGameDBError, savedGame: IFreeGame) => {
                if (e) {
                    reject(e);
                } else {
                    resolve(savedGame);
                }
            });

        });
    }

    public async findGameWithId(id: String): Promise<IFreeGame | Error> {
        return new Promise<IFreeGame | Error>((
            resolve: (value?: IFreeGame | PromiseLike<IFreeGame>) => void,
            reject: (reason?: Error) => void) => {
                FREEGAME.findOne({gameId: id}, (e: Error, gameFound: IFreeGame) => {
                    if (e) {
                        reject(e);
                    }
                    resolve(gameFound);
                });
        });
    }

    public async delete(id: String): Promise<void | FreeGameDBError> {
        return new Promise<void | FreeGameDBError>((
            resolve: (value?: void | PromiseLike<void>) => void,

            reject: (reason?: FreeGameDBError) => void) => {
            FREEGAME.findOneAndDelete({ gameId: id }, (e: FreeGameDBError, deletedGame: IFreeGame) => {
                if (e) {
                    reject(e);
                }
                try {
                    Utils.removeImageUrl(deletedGame.thumbnail.toString());
                } catch (e) {
                    reject(e);
                }
                resolve();
            });
        });
    }

    public async reset(id: String): Promise<void | FreeGameDBError> {
        return new Promise<void | FreeGameDBError>((
            resolve: (value?: void | PromiseLike<void>) => void,
            reject: (reason?: FreeGameDBError) => void) => {
            const n: number = 3;
            const sTime: Record[] = Utils.generateRandomRecords(n);
            const vsTime: Record[] = Utils.generateRandomRecords(n);

            const update: Object = {
                $set: {
                    soloTime: sTime,
                    vsTime: vsTime,
                },
            };

            FREEGAME.updateOne({ gameId: id }, update, (e: FreeGameDBError) => {
                if (e) {
                    reject(e);
                } else {
                    resolve();

                }
            });
        });
    }

    public updateSoloGameRecord(id: string, userName: string, newScore: number): Observable<boolean> {

        return new Observable<boolean>((observer: Subscriber<boolean>) => {
            this.findGameWithId(id).then((gamefound: IFreeGame) => {
                        const sTime: Record[] = BestGameRecord.addRecordInGame(gamefound.soloTime, userName, newScore);
                        const update: Object = {
                            $set: {
                                soloTime: sTime,

                            },
                        };
                        FREEGAME.updateOne({ gameId: id }, update, (e: Error) => {
                            if (e) {
                              observer.next(false);
                            }
                            observer.next(true);
                        });
                    });
        });
    }

    public findPostionInSoloGame(id: string, userName: string, newScore: number): Observable<number> {

        return new Observable<number>((observer: Subscriber<number>) => {
            this.findGameWithId(id).then((gamefound: IFreeGame) => {
                        const index: number = BestGameRecord.getPositionOfRecord(gamefound.soloTime, newScore, userName);
                        observer.next(index);
                    });
        });
    }

    public updateMultiGameRecord(id: string, userName: string, newScore: number): Observable<boolean> {

        return new Observable<boolean>((observer: Subscriber<boolean>) => {
            this.findGameWithId(id).then((gamefound: IFreeGame) => {
                        const sTime: Record[] = BestGameRecord.addRecordInGame(gamefound.vsTime, userName, newScore);

                        const update: Object = {
                            $set: {
                                vsTime: sTime,

                            },
                        };
                        FREEGAME.updateOne({ gameId: id }, update, (e: Error) => {
                            if (e) {
                              observer.next(false);
                            }
                            observer.next(true);
                        });
                    });
        });
    }

    public findPostionInMultiGame(id: string, userName: string, newScore: number): Observable<number> {

        return new Observable<number>((observer: Subscriber<number>) => {
            this.findGameWithId(id).then((gamefound: IFreeGame) => {
                        const index: number = BestGameRecord.getPositionOfRecord(gamefound.vsTime, newScore, userName);
                        observer.next(index);
                    });
        });
    }
    public newGame(name: String, thumbnail: String, oScene: IScene,
                   mScene: IScene, dId: string[], highscoreDimensions: number = 3): IFreeGame {

        const soloT: Record[] = Utils.generateRandomRecords(highscoreDimensions);
        const vsT: Record[] = Utils.generateRandomRecords(highscoreDimensions);

        return new FREEGAME({
            gameId: Utils.uniqueId(),
            name: name,
            thumbnail: thumbnail,
            originalScene: oScene,
            modifiedScene: mScene,
            differencesId: dId,
            soloTime: soloT,
            vsTime: vsT,
        });
    }

}
