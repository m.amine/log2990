import * as mongoose from "mongoose";

export class DataBaseAccess {

    private readonly DB_USER: string = "LOG2990-09_116";
    private readonly DB_PASSWORD: string = "log2990";
    private readonly DB_DB: string = "log2990_db";
    private readonly DB_HOST: string = "ds129801.mlab.com";
    private readonly DB_PORT: string = "29801";
    private readonly DB_AUTHORITY_CONNECTION: string = "admin";
    private readonly DB_GENERAL_USER: string = "user";

    public establishConnection(): mongoose.Connection {
        mongoose.connect(this.buildUrl(), {
            useNewUrlParser: true,
            user: this.DB_GENERAL_USER,
            pass: this.DB_PASSWORD,
            auth: {
                authds: this.DB_AUTHORITY_CONNECTION,
            },
        })
            .catch((e: Error) => { throw e; });

        const database: mongoose.Connection = mongoose.connection;
        database.on("error", console.error.bind(console, "connection error:"));

        return database;
    }

    private buildUrl(): string {
        return "mongodb://" + this.DB_USER + ":" + this.DB_PASSWORD + "@" + this.DB_HOST + ":" + this.DB_PORT + "/" + this.DB_DB;
    }
}
