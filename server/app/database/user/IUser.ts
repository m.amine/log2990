import { Document } from "mongoose";
import { User } from "../../../../common/models/user";
export interface IUser extends User, Document {
}
