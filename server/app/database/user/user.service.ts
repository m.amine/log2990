import { UserLoginStatus } from "../../../../common/communication/errors";
import { UserDB } from "../databaseProxy/userDB";
import { IUser } from "./IUser";

export class UserService {
    private userDb: UserDB = new UserDB();

    public async login(name: string): Promise<UserLoginStatus> {
        return new Promise<UserLoginStatus>((
            resolve: (value?: UserLoginStatus | PromiseLike<UserLoginStatus>) => void,
            reject: (reason?: UserLoginStatus) => void) => {

                if (!this.validateAlphaNumeric(name)) {
                    reject(UserLoginStatus.E_INVALID_USER_NAME);
                } else {
                    this.userDb.get(name).then((user: IUser) => {
                        if (user) {
                            if (user.isConnected) {
                                reject(UserLoginStatus.E_USER_ALREADY_CONNECTED);
                            } else {
                                this.userDb.update(name, true)
                                .then(() => resolve(UserLoginStatus.S_OK))
                                .catch(() => reject(UserLoginStatus.E_ERROR));
                            }
                        } else {
                            this.userDb.create(name)
                            .then(() => resolve(UserLoginStatus.S_OK))
                            .catch(() => reject(UserLoginStatus.E_ERROR));
                        }
                    }).catch(() => reject(UserLoginStatus.E_ERROR));
                }
        });
    }

    public async logout(name: string): Promise<UserLoginStatus> {
        return new Promise<UserLoginStatus>((
            resolve: (value?: UserLoginStatus | PromiseLike<UserLoginStatus>) => void,
            reject: (reason?: UserLoginStatus) => void) => {
                this.userDb.update(name, false).then(() => {
                    resolve(UserLoginStatus.S_OK);
                }).catch(() => reject(UserLoginStatus.E_ERROR));
        });
    }

    private validateAlphaNumeric(username: string): boolean {
        const regexp: RegExp = new RegExp(/^[a-zA-Z0-9]{2,20}$/);
        if (regexp.test(username)) {
            return true;
        } else {
            return false;
        }
    }
}
