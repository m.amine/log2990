import { model, Model, Schema } from "mongoose";
import { IUser } from "./IUser";

const userSchema: Schema = new Schema(
    {
        name: { type: String, unique: true },
        isConnected: Boolean,
    },
    { timestamps: true },
);

const USER: Model<IUser> = model<IUser>("User", userSchema);

export { USER, userSchema };
