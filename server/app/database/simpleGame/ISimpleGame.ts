import { Document } from "mongoose";
import { SimpleGame } from "../../../../common/models/simpleGame";
export interface ISimpleGame extends SimpleGame, Document {
}
