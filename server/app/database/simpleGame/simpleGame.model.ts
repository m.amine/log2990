import { model, Model, Schema } from "mongoose";
import { ISimpleGame } from "./ISimpleGame";

export const simpleGameSchema: Schema = new Schema(
    {
        gameId: String,
        name: String,
        originalImage: String,
        modifiedImage: String,
        differencesImage: String,
        soloTime: [],
        vsTime: [],
    },
    { timestamps: true });

export const SIMPLEGAME: Model<ISimpleGame> = model<ISimpleGame>("SimpleGame", simpleGameSchema);
