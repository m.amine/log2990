import { UserDB } from "../database/databaseProxy/userDB";
import { Utils } from "../utils/utils";

const CHECK_INTERVAL: number = 1000;

interface LoginSocket {
    userName: string;
    socket: SocketIO.Socket;
    isConnected: boolean;
    intervalID: NodeJS.Timeout | null;
}

export class LoginSocketHandler {
    private userDB: UserDB;
    private sockets: Map<string, LoginSocket>;

    public constructor() {
        this.userDB = new UserDB();
        this.sockets = new Map<string, LoginSocket>();
    }

    public handle(socket: SocketIO.Socket): void {
        const loginSocket: LoginSocket = {
            socket: socket,
            userName: "",
            isConnected: false,
            intervalID: null,
        };

        const id: string = Utils.uniqueId();
        this.sockets.set(id, loginSocket);

        socket.on("login", (username: string) => {
            (this.sockets.get(id) as LoginSocket).userName = username;
            (this.sockets.get(id) as LoginSocket).intervalID = setInterval(() => this.checkConnexion(id), CHECK_INTERVAL);
            (this.sockets.get(id) as LoginSocket).isConnected = true;
            this.sendConnexionEvent(username);
        });
    }

    private checkConnexion(id: string): void {
        const loginSocket: LoginSocket = this.sockets.get(id) as LoginSocket;
        if (!loginSocket.socket.connected) {
            this.userDB.update(loginSocket.userName, false);
            clearInterval(loginSocket.intervalID as NodeJS.Timeout);
            this.sendDeconnexionEvent(loginSocket.userName);
        }
    }

    private sendConnexionEvent(playerName: string): void {
        this.sockets.forEach((player: LoginSocket, name: string) => {
            if (player.isConnected && player.userName !== playerName) {
                player.socket.emit("loggedIn", playerName);
            }
        });
    }

    private sendDeconnexionEvent(playerName: string): void {
        this.sockets.forEach((player: LoginSocket, name: string) => {
            if (player.isConnected ) {
                player.socket.emit("logout", playerName);
            }
        });
    }

}
