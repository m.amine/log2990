import { NextFunction, Request, Response } from "express";
import { injectable, } from "inversify";
import "reflect-metadata";
import { SceneGenerationStatus as SGStatus } from "../../../common/communication/errors";
import { ServerResponse } from "../../../common/communication/serverResponse";
import { SceneGenerator, SceneGeneratorResponse as SGResponse } from "../services/sceneGenerator/sceneGenerator.service";

@injectable()
export class SceneGeneratorRoute {

    public generateScenes(req: Request, res: Response, next: NextFunction): void {
        const sceneGenerator: SceneGenerator = new SceneGenerator();
        const response: ServerResponse<SGStatus, SGResponse> =
        sceneGenerator.generateScenes(req.body.objectQuantity, req.body.checkBoxes, req.body.objectType);
        res.send(response);
    }

}
