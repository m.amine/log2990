import { NextFunction, Request, Response } from "express";
import { injectable, } from "inversify";
import "reflect-metadata";
import { FreeGameDBError } from "../../../common/communication/freeGameDBError";
import { IScene } from "../../../common/models/iScene";
import { FreeGameDB } from "../database/databaseProxy/freeGameDB";
import { SimpleGameDB } from "../database/databaseProxy/simpleGameDB";
import { IFreeGame } from "../database/freeGame/IFreeGame";
import { ISimpleGame } from "../database/simpleGame/ISimpleGame";
import { PNG } from "../utils/png";
import { Utils } from "../utils/utils";

@injectable()
export class GameRoute {
    private simpleGameDB: SimpleGameDB = new SimpleGameDB();
    private freeGameDB: FreeGameDB = new FreeGameDB();

    public getSimpleGames(req: Request, res: Response, next: NextFunction): void {
        this.simpleGameDB.getAll().then((games: ISimpleGame[]) => res.send(games)).catch((e: Error) => res.json(e));
    }

    public findSimpleGame(req: Request, res: Response, next: NextFunction): void {
        this.simpleGameDB.findGameWithId(req.params.id).then((game: ISimpleGame) => res.send(game)).catch((e: Error) => res.json(e));
    }

    public updateRecordInSimpleViewSoloGame( req: Request , res: Response , next: NextFunction): void {
        this.simpleGameDB.updateSoloGameRecord(req.body.id , req.body.userName ,
                                               req.body.newScore).subscribe((status: boolean) => res.send({status}));
    }

    public getRecordPositionInSimpleViewSoloGame(req: Request , res: Response , next: NextFunction): void {
        this.simpleGameDB.findPostionInSoloGame(req.body.id, req.body.userName,
                                                req.body.newScore).subscribe((index: number) => res.send({index}));
    }

    public updateRecordInSimpleViewMultiGame( req: Request , res: Response , next: NextFunction): void {
        this.simpleGameDB.updateMultiGameRecord(req.body.id , req.body.userName ,
                                                req.body.newScore).subscribe((status: boolean) => res.send({status}));
    }

    public getRecordPositionInSimpleViewMultiGame(req: Request , res: Response , next: NextFunction): void {
        this.simpleGameDB.findPostionInMultiGame(req.body.id, req.body.userName,
                                                 req.body.newScore).subscribe((index: number) => res.send({index}));
    }

    public updateRecordInFreeViewSoloGame( req: Request , res: Response , next: NextFunction): void {
        this.freeGameDB.updateSoloGameRecord(req.body.id , req.body.userName ,
                                             req.body.newScore).subscribe((status: boolean) => res.send({status}));
    }

    public getRecordPositionInFreeViewSoloGame(req: Request , res: Response , next: NextFunction): void {
        this.freeGameDB.findPostionInSoloGame(req.body.id, req.body.userName,
                                              req.body.newScore).subscribe((index: number) => res.send({index}));
    }

    public updateRecordInFreeViewMultiGame( req: Request , res: Response , next: NextFunction): void {
        this.freeGameDB.updateMultiGameRecord(req.body.id , req.body.userName ,
                                              req.body.newScore).subscribe((status: boolean) => res.send({status}));
    }

    public getRecordPositionInFreeViewMultiGame(req: Request , res: Response , next: NextFunction): void {
        this.freeGameDB.findPostionInMultiGame(req.body.id, req.body.userName,
                                               req.body.newScore).subscribe((index: number) => res.send({index}));
    }

    public findFreeGame(req: Request, res: Response, next: NextFunction): void {
        this.freeGameDB.findGameWithId(req.params.id).then((game: IFreeGame) => res.send(game)).catch((e: Error) => res.json(e));
    }

    public deleteSimpleGame(req: Request, res: Response, next: NextFunction): void {
        const status: number = 200;
        this.simpleGameDB.delete(req.params.id).then(() => res.sendStatus(status)).catch((e: Error) => res.json(e));
    }

    public resetSimpleGame(req: Request, res: Response, next: NextFunction): void {
        const status: number = 200;
        this.simpleGameDB.reset(req.params.id).then(() => res.sendStatus(status)).catch((e: Error) => res.json(e));
    }

    public getFreeGames(req: Request, res: Response, next: NextFunction): void {
        this.freeGameDB.getAll().then((games: IFreeGame[]) => res.send(games)).catch((e: FreeGameDBError) => res.json(e));
    }

    public createFreeGame(req: Request, res: Response, next: NextFunction): void {
        const name: string = req.body.name;

        const thumbnailBuffer: ArrayBuffer = Utils.parseBufferObject(req.body.thumbnail);
        const thumbnail: PNG = new PNG(thumbnailBuffer);
        const thumbnailUrl: string = thumbnail.save();

        const oScene: IScene = req.body.oScene;
        const mScene: IScene = req.body.mScene;

        const dId: string[] = req.body.differencesId;

        this.freeGameDB.create(name, thumbnailUrl, oScene, mScene, dId)
        .then((game: IFreeGame) => res.send(game))
        .catch((e: FreeGameDBError) => res.json(e));
    }

    public deleteFreeGame(req: Request, res: Response, next: NextFunction): void {
        const status: number = 200;
        this.freeGameDB.delete(req.params.id).then(() => res.sendStatus(status)).catch((e: Error) => res.json(e));
    }

    public resetFreeGame(req: Request, res: Response, next: NextFunction): void {
        const status: number = 200;
        this.freeGameDB.reset(req.params.id).then(() => res.sendStatus(status)).catch((e: Error) => res.json(e));
    }

}
