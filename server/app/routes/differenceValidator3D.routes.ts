import { NextFunction, Request, Response } from "express";
import { injectable, } from "inversify";
import { SceneProcessingStatus as SPStatus} from "../../../common/communication/errors";
import { DifferenceDetector3D } from "../services/differenceDetector3D.service";

@injectable()
export class DifferenceValidator3DRoute {
    public detectDifferences3D(req: Request, res: Response, next: NextFunction): void {
        const differenceDetector3D: DifferenceDetector3D = new DifferenceDetector3D;
        const result: SPStatus = differenceDetector3D.validateDifference(req.body[0], req.body[1]);
        res.send([ result ]);
    }
}
