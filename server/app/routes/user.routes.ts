import { NextFunction, Request, Response } from "express";
import { injectable, } from "inversify";
import "reflect-metadata";
import { UserLoginStatus } from "../../../common/communication/errors";
import { UserService } from "../database/user/user.service";

@injectable()
export class UserRoute {
    private userService: UserService = new UserService();

    public login(req: Request, res: Response, next: NextFunction): void {
        this.userService.login(req.params.name)
        .then((status: UserLoginStatus) => res.send({"status": status}))
        .catch((status: UserLoginStatus) => res.send({"status": status}));
    }

    public logout(req: Request, res: Response, next: NextFunction): void {
        this.userService.logout(req.params.name)
        .then((status: UserLoginStatus) => res.send({"status": status}))
        .catch((status: UserLoginStatus) => res.send({"status": status}));
    }

}
