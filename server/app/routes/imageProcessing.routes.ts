import { NextFunction, Request, Response } from "express";
import { injectable, } from "inversify";
import "reflect-metadata";
import { ImageProcessingStatus as IPStatus } from "../../../common/communication/errors";
import { PixelMap } from "../../../common/models/types";
import { SimpleGameDB } from "../database/databaseProxy/simpleGameDB";
import { ISimpleGame } from "../database/simpleGame/ISimpleGame";
import { DifferenceDetector } from "../services/imageProcessing/differenceDetector.service";
import { DifferencesGenerator } from "../services/imageProcessing/generateDifferences.service";
import { ImageGetter } from "../services/imageProcessing/getImage.service";
import { Utils } from "../utils/utils";

@injectable()
export class ImageProcessingRoute {
    private simpleGameDB: SimpleGameDB = new SimpleGameDB();

    public generateDifferences(req: Request, res: Response, next: NextFunction): void {
        const differencesGenerator: DifferencesGenerator = new DifferencesGenerator();
        const result: { status: IPStatus, imgs: string[] } = differencesGenerator.start(
            Utils.parseBufferObject(req.body.originalImage),
            Utils.parseBufferObject(req.body.modifiedImage));

        if (result["status"] === IPStatus.S_OK) {
            const oIdx: number = 0;
            const mIdx: number = 1;
            const dIdx: number = 2;
            this.simpleGameDB.create(req.body.gameName, result["imgs"][oIdx], result["imgs"][mIdx], result["imgs"][dIdx])
                .then((game: ISimpleGame) => res.send({ "status": IPStatus.S_OK, "game": game }))
                .catch((e: Error) => res.json(e));
        } else {
            res.send({ "status": result["status"] });
        }
    }

    public detectDifference(req: Request, res: Response, next: NextFunction): void {
        const differenceDetector: DifferenceDetector = new DifferenceDetector(req.body.point, req.body.game);
        const result: [IPStatus, PixelMap] = differenceDetector.detect();
        res.send(result);
    }

    public getImage(req: Request, res: Response, next: NextFunction): void {
        const type: string = "image/";
        const code: number = 200;
        res.writeHead(code, {
            "Content-Type": type + ImageGetter.getImageExtension(req.params.filename),
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Referer, Range, Accept-Encoding",
        });
        res.end(ImageGetter.get(req.params.filename), "binary");
    }

}
